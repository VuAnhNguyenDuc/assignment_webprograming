﻿<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Các món ăn</title>
	    
	<link href="css/an-gi.css" rel="stylesheet" />
	<style>
		.wrap-suggestion {
			padding:0;
			display: none;
			border: 1px solid #e2e2e2;
			background: #fff;
			position: absolute;
			left: 15px;
			width: 35%;
			top: 60px;
			z-index: 9;
		}
		.wrap-suggestion li {
			display: block;
			background: #fff;
			overflow: hidden;
			list-style: none;
			border-bottom: 1px dotted #ccc;
		}
		.wrap-suggestion li a {
			display: block;
			overflow: hidden;
			padding: 6px;
			color: #999;
			font-size: 12px;
		}
		.wrap-suggestion li a img {
			float: left;
			width: 50px;
			height: auto;
			margin: 0 6px 0 0;
		}
		.wrap-suggestion li a h3 {
			display: block;
			width: auto;
			color: #333;
			font-size: 14px;
			font-weight: 700;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
		}
		.wrap-suggestion li a span.price {
			font-size: 12px;
			color: #c70100;
			float: none;
		}
		.wrap-suggestion:after, .wrap-suggestion:before {
				bottom: 100%;
				left: 80px;
				border: solid transparent;
				content: " ";
				height: 0;
				width: 0;
				position: absolute;
		}
		.wrap-suggestion:after {
			border-color: rgba(255,255,255,0);
			border-bottom-color: white;
			border-width: 8px;
			margin-left: -8px;
		}
		.wrap-suggestion:before {
			border-color: rgba(218,218,218,0);
			border-bottom-color: red;
			border-width: 9px;
			margin-left: -9px;
		}
		.wrap-suggestion li:hover, .wrapp-producthome .wrap-suggestion li.selected, .wrap-main .wrap-suggestion li.selected {
			background: #f8f8f8;
		}
	</style>
</head>

<body>
	<?php include('header.php');?>
	<!-- Begin body -->
	<div class="main">	
		<div class="container ">			
				<!-- Menu view -->
				<ul class="list-inline menuview col-centered col-md-12 col-lg-12 col-sm-12 col-xs-12" style="padding:0">	
					<li style="padding:0" class="col-md-11 col-xs-11 col-sm-11 col-lg-11">
						 <input class="form-control" onkeyup=Search(event) id="search-food-input" placeholder="Tìm kiếm" type="text" style="height:50px" />
						 <ul id="result_search" class="wrap-suggestion">
                    	</ul>
					</li>
                   <li style="padding:0;padding-left:5px" class="col-md-1 col-xs-1 col-sm-1 col-lg-1"> <input id="search-food"  type="button" class="btn btn-info" style="height:50px;    background: url(images/search-icon.png) center no-repeat ;background-color: white;width: 100%;margin-bottom: 20px" /></li>
				
				</ul>
				
				<!-- End of Menu view -->
				
				<!-- Product Grid -->
				<div class="col-md-12">
					<div class="row lazy-wrapper">
					
					</div>
            	</div>	
		</div>
	</div>
	<!-- End body -->
    <?php include('footer.php'); ?>

	
	<script src="js/imagesloaded.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.isotope.sloppy-masonry.min.js"></script>
	<script src="js/lazyloader.js"></script>
	<script>
	 $(function(){
        $('.lazy-wrapper').lazyLoader({
            ajaxLoader: 'controller/json_food_lazyload.php',
			//jsonFile: 'data.json',
            mode: 'scroll',
            limit: 8,
            records:1000,
            offset: 1,
            isIsotope: true,
            isotopeResize: 4
        });
    });
	$(document).ready(function(){
		$("#search-food").click(function(){
                var keyword=$("#search-food-input").val();
                if(keyword.length>=2){
                    keyword = formatstring(keyword);
                    location.href="mon-an-ket-qua-tim-kiem.html?tu-khoa="+keyword;
                }
            });
            $('#search-food-input').keypress(function (e) {
                var key = e.which;
                if (key == 13)  // the enter key code
                {
                    $("#search-food").click();
                }
            });
	});
	function Search(event) {
             var keyword=$("#search-food-input").val();
             keyword = formatstring(keyword);
             $("#result_search").html("");
             if (keyword!=""&&keyword.length>=2){
                 $.ajax({
                    url: 'controller/json_food_search.php',
                    type: 'get',
                    dataType: "text",
                    data: {
                        keyword:keyword,
                    },
                    success: function(output) {
                        if(output.length>0){
                            output = JSON.parse(output);
                            for(var i = 0;i<output.length;i++)
					        {
                                if(i>5) break;
                                var tr = $('<li class=""></li>')
                                var tr1 = $('<a href="mon-an-'+formatlink(output[i].name) +'-' + output[i].id +'.html"></a>');
                                var tr2 = $(' <img src="upload/products/'+output[i].avatar + '"style="hight:50px;width:50px"/>')
                                var tr3 = $('<h3>' + output[i].name + '</h3>');
                                tr1.append(tr2).append(tr3);
                                tr.append(tr1);
                                $("#result_search").append(tr);
                            }
                            $("#result_search").css("display", "block");
                        }else  $("#result_search").css("display", "none");
                    } 
                 });
             }else  $("#result_search").css("display", "none");
        }
</script>
</body>
</html>
