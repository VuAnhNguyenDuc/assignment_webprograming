﻿<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Product Detail</title>

    <!-- main css -->


	
    <!-- Latest compiled and minified CSS --> 
    <link href="css/chi-tiet-mon-an.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css">
    <style>
        
        .jssora05l, .jssora05r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 40px;
            height: 40px;
            cursor: pointer;
            background: url('img/a17.png') no-repeat;
            overflow: hidden;
        }
        .jssora05l { background-position: -10px -40px; }
        .jssora05r { background-position: -70px -40px; }
        .jssora05l:hover { background-position: -130px -40px; }
        .jssora05r:hover { background-position: -190px -40px; }
        .jssora05l.jssora05ldn { background-position: -250px -40px; }
        .jssora05r.jssora05rdn { background-position: -310px -40px; }
        .jssora05l.jssora05lds { background-position: -10px -40px; opacity: .3; pointer-events: none; }
        .jssora05r.jssora05rds { background-position: -70px -40px; opacity: .3; pointer-events: none; }
        /* jssor slider thumbnail navigator skin 01 css *//*.jssort01 .p            (normal).jssort01 .p:hover      (normal mouseover).jssort01 .p.pav        (active).jssort01 .p.pdn        (mousedown)*/.jssort01 .p {    position: absolute;    top: 0;    left: 0;    width: 72px;    height: 72px;}.jssort01 .t {    position: absolute;    top: 0;    left: 0;    width: 100%;    height: 100%;    border: none;}.jssort01 .w {    position: absolute;    top: 0px;    left: 0px;    width: 100%;    height: 100%;}.jssort01 .c {    position: absolute;    top: 0px;    left: 0px;    width: 68px;    height: 68px;    border: #000 2px solid;    box-sizing: content-box;    background: url('img/t01.png') -800px -800px no-repeat;    _background: none;}.jssort01 .pav .c {    top: 2px;    _top: 0px;    left: 2px;    _left: 0px;    width: 68px;    height: 68px;    border: #000 0px solid;    _border: #fff 2px solid;    background-position: 50% 50%;}.jssort01 .p:hover .c {    top: 0px;    left: 0px;    width: 70px;    height: 70px;    border: #fff 1px solid;    background-position: 50% 50%;}.jssort01 .p.pdn .c {    background-position: 50% 50%;    width: 68px;    height: 68px;    border: #000 2px solid;}* html .jssort01 .c, * html .jssort01 .pdn .c, * html .jssort01 .pav .c {    /* ie quirks mode adjust */    width /**/: 72px;    height /**/: 72px;}
    </style>
        <link rel='stylesheet prefetch' href="css/owl.carousel.css"/>
        <link rel='stylesheet prefetch' href="css/magnific-popup.css"/>
    <link rel="stylesheet" href="css/style_videos.css">
</head>

<body style="background: url(img/slider/slide1.jpg);">
   <?php include('header.php') ?>
	<!-- Begin body -->
	<div class="main">	
		<div class="container">			
			<div class="row">
				<div id="avatar_product" class="col-lg-6 col-md-6 col-sm-6 col-xs-12 productimage">
					
				</div>
				<div class="col-lg-6 col-md-6 col-md-6 col-xs-12 detail">
					<div class="row maintitle">
						<p id="title"></p>
						<p id="location"></p>
					</div>
				
					<ul class="list-group rating row" style="margin-left: 0; margin-right: 0;">
						<li class="list-group-item list-group-item2 col-md-2 col-lg-2 col-sm-4 col-xs-4">
							<p class="point" id="locationrate"></p>
							<p><strong>Vị trí</strong></p>
						</li>
						<li class="list-group-item list-group-item2 col-md-2 col-lg-2 col-sm-4 col-xs-4">
							<p class="point" id="qualityrate"></p>
							<p><strong>Chất lượng</strong></p>
						</li>
						<li class="list-group-item list-group-item2 col-md-2 col-lg-2 col-sm-4 col-xs-4">
							<p class="point" id="spacerate"></p>
							<p><strong>Không gian</strong></p>
						</li>
						<li class="list-group-item list-group-item2 col-md-2 col-lg-2 col-sm-4 col-xs-4">
							<p class="point" id="pricerate"></p>
							<p><strong>Giá cả</strong></p>
						</li>
						<li class="list-group-item list-group-item2 col-md-2 col-lg-2 col-sm-4 col-xs-4">
							<p class="point"id="servicerate">6.7</p>
							<p><strong>Phục vụ</strong></p>
						</li>
						<li class="list-group-item list-group-item2 col-md-2 col-lg-2 col-sm-4 col-xs-4">
							<p class="point n_comment">0</p>
							<p><strong>Bình luận</strong></p>
						</li>
					</ul>
				
					<div class="row">
						<p id="address"><img class="img-responsive" src="./images/icons/location-map-solid/16x16.png" style="float: left;">  
						</p>
						<p id="phone"><img class="img-responsive" src="./images/icons/phone-ring-solid/16x16.png" style="float: left;">    
						</p>
						<p id="openhour"><img class="img-responsive" src="./images/icons/clock-solid/16x16.png" style="float: left;">  
						</p>
						<p id="price"><img class="img-responsive" src="./images/icons/dollar-solid/16x16.png" style="float: left;">  
						</p>
                        <button id="save_products" onclick="Save_Products()" style="margin-bottom:10px;margin-top: 5px;width:25%;display:none" type="button" class="btn btn-success">Lưu lại</button>
                        <button id="delete_save_products" onclick="Delete_Save_Products()" style="margin-bottom:10px;margin-top: 5px;width:25%;display:none" type="button" class="btn btn-success">Bỏ lưu</button>
					</div>				
				</div>
			</div>
			<!--  Comments, Videos, Images... -->
			<nav class="navbar navbar-default">
				<div class="container" style="padding:0">
					  <ul class="tab nav navbar-nav" style="margin:0;">
                      
						<li id="tab_fist_show" class="tablinks" onclick="openTabs(event, 'comment')"><a>BÌNH LUẬN</a></li>
                        <li  class="tablinks" onclick="openTabs(event, 'descriptions')"><a>THÔNG TIN</a></li>
						<li class="tablinks" onclick="openTabs(event, 'image')"><a  >HÌNH ẢNH</a></li>
						<li class="tablinks"  onclick="openTabs(event, 'video')"><a >VIDEOS</a></li>
						<li class="tablinks"  onclick="openTabs(event, 'rating')"><a >ĐÁNH GIÁ</a></li>
					  </ul>
					<div style="clear:both;"></div>
				</div>
			</nav>
            
			<div id="comment" class="tabcontent">
				    <div class="row color-news" style="margin-top:5px;">

                    <div class="col-sm-12">
                        <h4 id="number_comments" style="margin-top:30px;"><span class="n_comment">0</span> Bình luận</h4>
                        <hr />
                    </div>
                    <?php if (isset($_SESSION['user']) && $_SESSION['user']){
                    ?>
                    
                    <form id="user_submit" class="form-group" >
                        <div class="col-sm-12" style="margin-top:20px;">
                            <textarea id = "text_comment" rows="2" style="width: 100%; border-radius: 5px; border-color: #ccc; " name="comment" placeholder="Đánh giá của bạn..."></textarea>
                        </div>
                        <div class="col-sm-12">
                            <button id="submit_comment" type="submit" class="btn btn-primary">GỬI</button>
                        </div>
                        <div class="clr"></div>
                    </form>
                    <?php }else{ ?>
                     
                    <form id="guest_submit" class="form-group" >
                        <div  class="col-sm-12">
                            <input type="text" name="name" required class="form-control" placeholder="Họ và tên..." id="usr">
                            <input type="email" name="email" required class="form-control" placeholder="Nhập email..." id="email">
                            <div class="clr"></div>
                        </div>
                        <div class="col-sm-12" style="margin-top:20px;">
                            <textarea id = "text_comment" rows="2" style="width: 100%; border-radius: 5px; border-color: #ccc; " name="comment" placeholder="Đánh giá của bạn..."></textarea>
                        </div>
                        <div class="col-sm-12">
                            <button id="submit_comment" type="submit" class="btn btn-primary">GỬI</button>
                        </div>
                        <div class="clr"></div>
                    </form>
                    <?php }?>
                    <div class="col-sm-12" style="padding-bottom:20px;">
                        <hr />
                    </div>
                    <div class="col-sm-12">
                        <ul class="comment-list">
                            
                        </ul>
                    </div>
                    <div  class="col-sm-12" style="padding-bottom:30px; text-align:center">
                        <a id="more_comment" onclick="ShowMoreCM(5)"  style="color: blue !important; cursor: pointer;">Xem thêm</a>
                        <hr />
                    </div>
                </div>
			</div>
            <div id="descriptions" class="tabcontent">

            </div>
			<div id="image" class="tabcontent">
                <h3 style="text-align:center">Các hình ảnh đặc sắc</h3>
                <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 800px; height: 456px; overflow: hidden; visibility: hidden; background-color: #24262e;">
                    <!-- Loading Screen -->
                    <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                        <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                        <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                    </div>
                    <div id="image_slider" data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 800px; height: 356px; overflow: hidden;">
                        
                    </div>
                    <!-- Thumbnail Navigator -->
                    <div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
                        <!-- Thumbnail Item Skin Begin -->
                        <div data-u="slides" style="cursor: default;">
                            <div data-u="prototype" class="p">
                                <div class="w">
                                    <div data-u="thumbnailtemplate" class="t"></div>
                                </div>
                                <div class="c"></div>
                            </div>
                        </div>
                        <!-- Thumbnail Item Skin End -->
                    </div>
                    <!-- Arrow Navigator -->
                    <span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
                    <span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
                </div>
                <!-- #endregion Jssor Slider End -->
                <div style="margin-bottom:20px;"></div>
			</div>
			<div id="video" class="tabcontent">
                <h3 style="text-align:center">Clips món ăn</h3>
                <div id="video_products" class="owl-carousel">

                </div>
                <div style="margin-bottom:20px"></div>
                
			</div>
            <div id="rating" class="tabcontent" style="pading-bottom:10px">
                <h3 style="text-align:center">Đánh giá của bạn</h3>
                <h4 id="number_rating"><span style="    font-weight: bold;color: red;">Đánh giá món ăn:</span></h3>
                <div id ="rating_submit">
                    <div class="form-group">
                        <label style = "    float: left;font-size: 20px;">Vị trí:</label>
                        <div id="rating_location" style="float: left;margin-top: -5px;"></div> 
                        <input id="rating_location_value" value="0" style="display:none" >
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group">
                        <label style = "    float: left;font-size: 20px;">Chất lượng:</label>
                        <div id="rating_quality" style="float: left;margin-top: -5px;"></div> 
                        <input id="rating_quality_value" value="0" style="display:none" >
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group">
                        <label style = "    float: left;font-size: 20px;">Không gian:</label>
                        <div id="rating_space" style="float: left;margin-top: -5px;"></div> 
                        <input id="rating_space_value" value="0" style="display:none" >
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group">
                        <label style = "    float: left;font-size: 20px;">Giá cả:</label>
                        <div id="rating_price" style="float: left;margin-top: -5px;"></div> 
                        <input id="rating_price_value" value="0" style="display:none" >
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group">
                        <label style = "    float: left;font-size: 20px;">Phục vụ:</label>
                        <div id="rating_service" style="float: left;margin-top: -5px;"></div> 
                        <input id="rating_service_value" value="0" style="display:none" >
                        <div style="clear:both"></div>
                    </div>
                    <button onclick="SendRating()" type="button" class="btn btn-primary" style="margin-bottom:20px;">Gửi đánh giá của bạn</button>
                </div>
                
			</div>
		</div>
    </div>
	<!-- End body -->
    <?php include('footer.php') ?>
	<!-- jQuery library -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>
    <script src="js/jssor.slider-21.1.6.mini.js" type="text/javascript"></script>
	<script>
        window.onload = document.getElementById('comment').style.display = "block";
            document.getElementById('tab_fist_show').classList.add('active');
            function openTabs(evt, tabsOpen) {
                var run = 1;
                if(tabsOpen == "rating"){
                    run = $("#islogin").val();
                    if(run == 0) alert("Bạn phải đăng nhập mới thực hiện được chức năng này!");
                }
                if(run==1){
                    var i, tabcontent, tablinks;
                    tabcontent = document.getElementsByClassName("tabcontent");
                    for (i = 0; i < tabcontent.length; i++) {
                        tabcontent[i].style.display = "none";
                    }
                    tablinks = document.getElementsByClassName("tablinks");
                    for (i = 0; i < tablinks.length; i++) {
                        tablinks[i].className = tablinks[i].className.replace(" active", "");
                    }
                    document.getElementById(tabsOpen).style.display = "block";
                    evt.currentTarget.className += " active";
                }
        }
        $(function () {
          $("#rating_location").rateYo({
                maxValue: 10,
                numStars: 10,
            onSet: function (rating, rateYoInstance) {
               $("#rating_location_value").val(rating);
            }
          });
        });
        $(function () {
          $("#rating_quality").rateYo({
            maxValue: 10,
            numStars: 10,
            onSet: function (rating, rateYoInstance) {
               $("#rating_quality_value").val(rating);
            }
          });
        });
        $(function () {
          $("#rating_space").rateYo({
            maxValue: 10,
            numStars: 10,
            onSet: function (rating, rateYoInstance) {
               $("#rating_space_value").val(rating);
            }
          });
        });
        $(function () {
          $("#rating_price").rateYo({
            maxValue: 10,
            numStars: 10,
            onSet: function (rating, rateYoInstance) {
               $("#rating_price_value").val(rating);
            }
          });
        });
        $(function () {
          $("#rating_service").rateYo({
            maxValue: 10,
            numStars: 10,
            onSet: function (rating, rateYoInstance) {
               $("#rating_service_value").val(rating);
            }
          });
        });

        var comment = new Array();
        var numshow = 0;
        var islogin = $("#islogin").val();
		var id = <?php echo $_REQUEST["id"] ?>;
		  $(document).ready(function () {
              
            $.ajax({
                url: 'controller/json_products_comment.php',
                type: 'post',
				data:{
					id:id,
				},
                dataType: "text",
                success: function(output) {
                    output = JSON.parse(output);
                    if(output.length > 5) $("#more_comment").css("display","block");
                    else $("#more_comment").css("display","none");
                    if(output.length>=1){
                        comment = output;
                        $(".n_comment").html(output.length);
                        if(output.length>5) numshow = 5;
                        else numshow = output.length;
                        for(var i = 0;i<output.length;i++)
					    {
                             if(i >= 5) break;
                             var postdate = Date.parse(output[i].commentdate);
                             var newdate = Date();
                             var current = Date.parse(newdate);
                             var timeline = current - postdate;
                             var year =  timeline/(1000*60*60*24*365);
                             var time ="";
                             if(year>=1)
                                time = year +" năm trước";
                             else if((year*365/30)>=1) 
                                 time = parseInt(year*365/30 )+ " tháng trước";
                             else if ((year*365)>=1)
                                time = parseInt(year*365) + " ngày trước";
                             else if((year*365*24)>=1)
                                time = parseInt(year*365*24) + " giờ trước";
                             else if((year*365*24*60)>=1)
                                time = parseInt(year*365*24*60) + " phút trước";
                             else if ((year*365*24*60*60)>=1)
                                time = parseInt(year*365*24*60*60) + " giây trước";
                             var li = $('<li></li>');
                             var name = $('<div class="comment-meta"><span class="user-name">'+output[i].author+'</span><span  class="comment-text"> '+output[i].content+'</span><br /><span class="comment-info">'+time+'</span></div>');
                             //var context = $('<div class="comment-text"><p>'+output[i].content+'</p></div>');
                             li.append(name);
                             $('.comment-list').append(li);
                        }
                        
                    }else{
                         $(".n_comment").html("0");
                            var li = $('<li id="no_comment" style="text-align:center">Chưa có bình luận !</li>');
                            $('.comment-list').append(li);
                    }

               }
            });
             
             $.ajax({
                 url:"controller/json_product_detail.php",
                 type:"POST",
                 data:{
                     id:id,
                 },
                 dataType:"text",
                 success:function(output){
                     if(output!=0){
                         output=JSON.parse(output);
                         document.title = output[0].Name;
                         $("#title").html('<strong>'+output[0].Name+'</strong>');
                         $("#location").html(output[0].Restaurant);
                         $("#locationrate").html(output[0].Rating["Location"]);
                         $("#qualityrate").html(output[0].Rating["Quality"]);
                         $("#spacerate").html(output[0].Rating["Space"]);
                         $("#pricerate").html(output[0].Rating["Price"]);
                         $("#servicerate").html(output[0].Rating["Service"]);
                         $("#address").append('<strong style="margin-left: 10px; font-size: 18px;">'+output[0].Address+'</strong>');
                         if(output[0].Hotline!="") $("#phone").append('<strong style="margin-left: 10px; font-size: 18px;">'+output[0].Hotline+'</strong>');
                         else $("#phone").append('<strong style="margin-left: 10px; font-size: 18px;">Chưa cập nhật</strong>');
                         $("#openhour").append('<strong style="margin-left: 10px; color: red; font-size: 18px;">'+output[0].OpenTime+'</strong>');
                         $("#price").append('<strong style="margin-left: 10px; font-size: 16px;">'+output[0].Price.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")+'đ</strong>');
                         $("#avatar_product").append('<img style="width:555px;height:346.88px" class="img-responsive" src="upload/products/'+output[0].Avatar+'" alt="'+output[0].Name+'">');
                         $("#descriptions").append('<p>'+output[0].Descriptions+'</p>');
                         $("#number_rating").append('<span> '+output[0].Rating["Avg"]+'đ / '+output[0].Rating["Number"]+' lượt</span>');
                          for(var j = 0; j<output[0].Videos.length;j++){
                             $("#video_products").append('<div class="item"><a class="popup-youtube" href="'+output[0].Videos[j]["Link"]+'?autoplay=1&rel=0&showinfo=0&wmode=transparent"><img src="http://i3.ytimg.com/vi/'+output[0].Videos[j]["Image"]+'/hqdefault.jpg"></a></div>');
                          }
                        
                            for(var i = 0; i<(output[0].Image).length;i++){
                             if(i==0){
                                 var image = $('<div data-p="144.50"><img data-u="image" src="upload/products/'+output[0].Image[i]["Name"]+'" /> <img data-u="thumb" src="upload/products/'+output[0].Image[0]["Name"]+'"  /></div>')
                              $("#image_slider").append(image);
                             }else {
                                 var image = $('<div data-p="144.50" style="display:none;"><img data-u="image" src="upload/products/'+output[0].Image[i]["Name"]+'" /> <img data-u="thumb" src="upload/products/'+output[0].Image[0]["Name"]+'"  /></div>')
                                $("#image_slider").append(image);
                             }
                         }
                         loadSlider();
                        $("#str").attr("src","js/index.js");
                     }
                 }
             });
            $("#guest_submit").submit(function(e){
                e.preventDefault();
                var usr= $("#usr").val();
                var email = $("#email").val();
                var text = $("#text_comment").val();
                if(text!=""){
                $.ajax({
                        url: 'controller/add_comment_product_guest.php',
                        type: 'post',
                        data:{
                        user:usr,
                        email:email,
                        comment:text,
                        id:id,
                        },
                        dataType: "text",
                        success: function(output) {
                            output = JSON.parse(output);
                            if(output[0].success=="1"){
                                var text = $(".comment-list").html();
                                var postdate = Date.parse(output[0].commentdate);
                                var newdate = Date();
                                var current = Date.parse(newdate);
                                var timeline = current - postdate;
                                var year =  timeline/(1000*60*60*24*365);
                                var time ="";
                                if(year>=1)
                                    time = year +" năm trước";
                                else if((year*365/30)>=1) 
                                    time = parseInt(year*365/30 )+ " tháng trước";
                                else if ((year*365)>=1)
                                    time = parseInt(year*365) + " ngày trước";
                                else if((year*365*24)>=1)
                                    time = parseInt(year*365*24) + " giờ trước";
                                else if((year*365*24*60)>=1)
                                    time = parseInt(year*365*24*60) + " phút trước";
                                else if ((year*365*24*60*60)>=0)
                                    time = parseInt(year*365*24*60*60) + " giây trước";
                                var li = $('<li></li>');
                                var name = $('<div class="comment-meta"><span class="user-name">'+output[0].author+'</span><span  class="comment-text"> '+output[0].content+'</span><br /><span class="comment-info">'+time+'</span></div>');
                                //var context = $('<div class="comment-text"><p>'+output[0].content+'</p></div>');
                                li.append(name);
                                $(".comment-list").prepend(li);
                                $("#no_comment").css("display","none");
                                $("#text_comment").val("");
                                $(".n_comment").html(parseInt($(".n_comment").html())+1);
                            }else alert("Lỗi!");
                        }
                    });
                }
            });
            
             $("#user_submit").submit(function(e){
                e.preventDefault();
                var text = $("#text_comment").val();
                var userid = $("#userid").val();
                var name = $("#usernamefull").val();
                if(text!=""){
                    $.ajax({
                            url: 'controller/add_comment_product_user.php',
                            type: 'post',
                            data:{
                            userid:userid,
                            name:name,
                            comment:text,
                            id:id,
                            },
                            dataType: "text",
                            success: function(output) {
                                output = JSON.parse(output);
                                if(output[0].success=="1"){
                                    var text = $(".comment-list").html();
                                    var postdate = Date.parse(output[0].commentdate);
                                    var newdate = Date();
                                    var current = Date.parse(newdate);
                                    var timeline = current - postdate;
                                    var year =  timeline/(1000*60*60*24*365);
                                    var time ="";
                                    if(year>=1)
                                        time = year +" năm trước";
                                    else if((year*365/30)>=1) 
                                        time = parseInt(year*365/30 )+ " tháng trước";
                                    else if ((year*365)>=1)
                                        time = parseInt(year*365) + " ngày trước";
                                    else if((year*365*24)>=1)
                                        time = parseInt(year*365*24) + " giờ trước";
                                    else if((year*365*24*60)>=1)
                                        time = parseInt(year*365*24*60) + " phút trước";
                                    else if ((year*365*24*60*60)>=0)
                                        time = parseInt(year*365*24*60*60) + " giây trước";
                                    var li = $('<li></li>');
                                    var name = $('<div class="comment-meta"><span class="user-name">'+output[0].author+'</span><span  class="comment-text"> '+output[0].content+'</span><br /><span class="comment-info">'+time+'</span></div>');
                                    //var context = $('<div class="comment-text"><p>'+output[0].content+'</p></div>');
                                    li.append(name);
                                    $(".comment-list").prepend(li);
                                    $("#no_comment").css("display","none");
                                    $("#text_comment").val("");
                                    $(".n_comment").html(parseInt($(".n_comment").html())+1);
                                }else alert("Lỗi!");
                            }
                        });
                }
            });
            
            if(islogin=="1"){
                
                var userid = $("#userid").val();
                $.ajax({
                    url:"controller/get_rating.php",
                    type:"GET",
                    data:{
                        userid:userid,
                        productid:id,
                    },
                    dataType:"text",
                    success:function(output){
                        
                        if(output!="0")
                        {
                             output = JSON.parse(output);
                             $("#rating_location").rateYo("option", "rating", output["Address_Rate"])
                             $("#rating_quality").rateYo("option", "rating", output["Quanlity_Rate"])
                             $("#rating_price").rateYo("option", "rating", output["Price_Rate"])
                             $("#rating_space").rateYo("option", "rating", output["Space_Rate"])
                             $("#rating_service").rateYo("option", "rating", output["Service_Rate"])
                        }
                    }
                });
                
                $.ajax({
                    url:"controller/get_save_product.php",
                    type:"POST",
                    data:{
                        userid:userid,
                        productid:id
                    },
                    dataType:"text",
                    success:function(output){
                        if(output=="1"){
                           
                            $("#save_products").css("display","none");
                            $("#delete_save_products").css("display","block");
                        }else{
                            $("#save_products").css("display","block");
                            $("#delete_save_products").css("display","none");
                        }
                    }
                });
            }
            
        });
       
        function ShowMoreCM(number){
             var i =0;
             for(i = numshow;i<comment.length;i++)
			 {
                    if(i >= (numshow+number)) break;
                    var postdate = Date.parse(comment[i].commentdate);
                    var newdate = Date();
                    var current = Date.parse(newdate);
                    var timeline = current - postdate;
                    var year =  timeline/(1000*60*60*24*365);
                    var time ="";
                    if(year >= 1)
                    time = year +" năm trước";
                    else if((year*365/30)>=1) 
                        time = parseInt(year*365/30 )+ " tháng trước";
                    else if ((year*365)>=1)
                    time = parseInt(year*365) + " ngày trước";
                    else if((year*365*24)>=1)
                    time = parseInt(year*365*24) + " giờ trước";
                    else if((year*365*24*60)>=1)
                    time = parseInt(year*365*24*60) + " phút trước";
                    else if ((year*365*24*60*60)>=1)
                    time = parseInt(year*365*24*60*60) + " giây trước";
                    var li = $('<li></li>');
                    var name = $('<div class="comment-meta"><span class="user-name">'+comment[i].author+'</span><span  class="comment-text"> '+comment[i].content+'</span><br /><span class="comment-info">'+time+'</span></div>');
                    //var context = $('<div class="comment-text"><p>'+output[i].content+'</p></div>');
                    li.append(name);
                    $('.comment-list').append(li);
            }
           
            numshow +=number;
            if (i>=comment.length) $("#more_comment").css("display","none");
        }
        function SendRating(){
            var rating_location = $("#rating_location_value").val();
            var rating_quality = $("#rating_quality_value").val();
            var rating_space = $("#rating_space_value").val();
            var rating_price = $("#rating_price_value").val();
            var rating_service = $("#rating_service_value").val();
            var userid = $("#userid").val();
            $.ajax({
                url:"controller/rating_products.php",
                type:"POST",
                data:{
                    location:rating_location,
                    quality:rating_quality,
                    space:rating_space,
                    price:rating_price,
                    service:rating_service,
                    productid:id,
                    userid:userid,
                },
                dataType:"text",
                success:function(output){
                    if(output=="1") alert("Cảm ơn bạn đã đánh giá món ăn!");
                    else if(output=="0") alert("Cập nhật đánh giá thành công!");
                    else alert("Lỗi");
                    // $("#rating_service").rateYo("option", "rating", 5)
                }
            });
        }
        function Save_Products(){
            var userid = $("#userid").val();
            $.ajax({
                url:"controller/save_products.php",
                type:"POST",
                data:{
                    userid:userid,
                    productid:id
                },
                dataType:"text",
                success:function(output){
                    if(output=="1"){ 
                        alert("Lưu lại thành công!");
                        $("#save_products").css("display","none");
                        $("#delete_save_products").css("display","block");
                    }
                    else if(output=="0") alert("Lỗi");
                    else alert("Món ăn này bạn đã lưu lại rồi!");
                }
            });
        }
        function Delete_Save_Products(){
            var userid = $("#userid").val();
            $.ajax({
                url:"controller/detele_save_products.php",
                type:"POST",
                data:{
                    userid:userid,
                    productid:id
                },
                dataType:"text",
                success:function(output){
                    if(output=="1"){ 
                        alert("Bỏ lưu thành công!");
                        $("#save_products").css("display","block");
                        $("#delete_save_products").css("display","none");
                    }
                    else alert("Lỗi! Vui lòng thử lại sao");
                }
            });
        }
	</script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js'></script>
   
    <script id="str"></script>
    <script type="text/javascript">
        function loadSlider() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: true,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 10,
                $SpacingX: 8,
                $SpacingY: 8,
                $Align: 360
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 800);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*responsive code end*/
        };
    </script>
    
</body>
</html>
