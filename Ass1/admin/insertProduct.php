<!DOCTYPE html>
<html lang="en">
<head>
	

    <link href="css/image-picker.css" rel="stylesheet">

</head>

<body>
	
	<?php require "header.php"; ?>
			<!-- start: Content -->
			<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.html">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Tables</a></li>
			</ul>

			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Insert new Record</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form class="form-horizontal">
							<fieldset>
								<div class="control-group">
											<label class="control-label" for="insertName">Name</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertName" type="text">
											</div>
										</div>
                                        <!-- Address -->
                                        <div class="control-group">
                                            <label class="control-label" for="getAddressList">Address</label>
                                            <div class="controls">
                                                <select id="getAddressList" data-rel="chosen">
                                                    <?php
                                                        require "admin_model/addressModel.php";
                                                        $addressModel = new addressModel();
                                                        $addressList = $addressModel->getAddress();
                                                        $addressModel->close();
                                                        $temp = json_decode($addressList,true);
                                                        foreach($temp as $address){
                                                            $id = $address["id"];
                                                            $AddressFull = $address["AdressFull"];
                                                            ?>
                                                            <option value='<?php echo $id; ?>'><?php echo $AddressFull; ?></option>
                                                            <?php
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- Restaurant -->
                                        <div class="control-group">
                                            <label class="control-label" for="getRestaurantList">Restaurant</label>
                                            <div class="controls">
                                                <select id="getRestaurantList" data-rel="chosen">
                                                    <?php
                                                    require "admin_model/restaurantModel.php";
                                                    $restaurantModel = new restaurantModel();
                                                    $restaurantList = $restaurantModel->getRestaurants();
                                                    $restaurantModel->close();
                                                    $temp = json_decode($restaurantList,true);
                                                    foreach($temp as $restaurant){
                                                        $id = $restaurant["id"];
                                                        $name = $restaurant["Name"];
                                                        ?>
                                                        <option value='<?php echo $id; ?>'><?php echo $name; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
										<div class="control-group">
											<label class="control-label" for="insertCategory">Category</label>
											<div class="controls">
												<select id="insertCategory" data-rel="chosen">
													<option value="Ăn gì?">Ăn gì</option>
													<option value="Ở đâu?">Ở đâu</option>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertShortDes">Short
												Description</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertShortDes"
													   type="text">
											</div>
										</div>
										<div class="control-group hidden-phone">
											<label class="control-label" for="insertDes">Description</label>
											<div class="controls">
												<textarea class="cleditor" id="insertDes" rows="3"></textarea>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertPrice">Price</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertPrice"
													   type="number">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertHotline">Hotline</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertHotline"
													   type="text">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertOpenTime">Open Time</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertOpenTime"
													   type="text">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertStatus">Status</label>
											<div class="controls">
												<select id="insertStatus" data-rel="chosen">
													<option value="Active">Active</option>
													<option value="Disabled">Disabled</option>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertMostView">Most View</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertMostView"
													   type="number">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertAvatar">Avatar</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertAvatar"
													   type="text">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Avatar</label>
											<div class="controls">
												<input type="file" id="imageInputAvatar" style="margin-right: 20px;">
												<input type="text" id="imageNameAvatar" style="display: none" />
												<button type="button" id="uploadImageAvatar" class="btn btn-info">Upload Image</button>
												<button type="button" id="getImageAvatar" class="btn btn-info select-images">Select From Server</button>
											</div>
										</div>
										<div class="control-group" id="imageDisplayAvatar" style="text-align: center">
										</div>
										<div class="control-group">
											<label class="control-label">Images</label>
											<div class="controls">
												<input type="file" id="imageInput" style="margin-right: 20px;">
												<input type="text" id="imageName" style="display: none" />
												<button type="button" id="uploadImage" class="btn btn-info">Upload Image</button>
												<button type="button" id="getImage" class="btn btn-info select-images">Select From Server</button>
											</div>
										</div>
										<div class="control-group" id="imageDisplay" style="text-align: center">
										</div>

										<div class="form-actions">

											<button type="button" id="insertProduct" class="btn btn-primary">Insert Record</button>
											<button type="reset" class="btn btn-success">Cancel</button>
										</div>
							</fieldset>
						</form>
					</div>
				</div><!--/span-->

			</div><!--/row-->

			</div><!--/.fluid-container-->
	
			<!-- end: Content -->
    // image picker
    <div class="modal hide fade" id="product-avatar">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h3>Settings</h3>
        </div>
        <div class="modal-body">
            <select id="image-picker-avatar-product" class="image-picker show-html">
                <option value=""></option>
                <?php
                    require "admin_model/imageModel.php";
                    $imageModel = new imageModel();
                    $images = $imageModel->getImages();
                    $imageModel->close();
                    $temp = json_decode($images,true);

                    $path = "../upload/";

                    foreach($temp as $image){
                        $id = $image["id"];
                        $imageName = $image["Name"];
                        $imagePath = $path.$imageName;
                        //<?php echo $imagePath
                        ?>
                        <option data-img-src="<?php echo $imagePath ?>" value="<?php echo $id; ?>"><?php echo $imageName ?></option>
                        <?php
                    }
                    ?>
            </select>         
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">Close</a>
			<?php if($_GET['request'] == "insertNews"){ ?>
				<a href="#" id="select-image" class="btn btn-primary">Select</a>
			<?php }else{?>
				<a href="#" id="select-image-multi" class="btn btn-primary">Select</a>
			<?php }?>

        </div>
    </div>

    <div class="modal hide fade" id="product-images">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h3>Settings</h3>
        </div>
        <div class="modal-body">         
            <select id="image-picker-multi" multiple="multiple" class="image-picker show-html">
                <option value=""></option>
                <?php
                require "admin_model/imageModel.php";
                $imageModel = new imageModel();
                $images = $imageModel->getImages();
                $imageModel->close();
                $temp = json_decode($images,true);

                $path = "../upload/";

                foreach($temp as $image){
                    $id = $image["id"];
                    $imageName = $image["Name"];
                    $imagePath = $path.$imageName;
                    //<?php echo $imagePath
                    ?>
                    <option data-img-src="<?php echo $imagePath ?>" value="<?php echo $id; ?>"><?php echo $imageName ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">Close</a>
			<?php if($_GET['request'] == "insertNews"){ ?>
				<a href="#" id="select-image" class="btn btn-primary">Select</a>
			<?php }else{?>
				<a href="#" id="select-image-multi" class="btn btn-primary">Select</a>
			<?php }?>

        </div>
    
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Select</a>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2013 <a href="http://jiji262.github.io/Bootstrap_Metro_Dashboard/" alt="Bootstrap_Metro_Dashboard">Bootstrap Metro Dashboard</a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>

		<script type="text/javascript" src="ajax/ajax_product_insert.js"></script>

        <script src="js/image-picker.js"></script>

        <script src="js/image-picker.min.js"></script>


	<!-- end: JavaScript-->
	
</body>
</html>
