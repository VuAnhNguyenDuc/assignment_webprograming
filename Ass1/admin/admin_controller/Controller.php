<?php
    header('Content-type: application/json');
    include 'ChromePhp.php';
    require "../admin_model/productModel.php";
    require "../admin_model/userModel.php";
    require "../admin_model/addressModel.php";
    require "../admin_model/videoModel.php";
    require "../admin_model/commentModel.php";
    require "../admin_model/rateModel.php";
    require "../admin_model/restaurantModel.php";
    require "../admin_model/newsModel.php";
    require "../admin_model/imageModel.php";

    $request = (isset($_POST['request']) && gettype($_POST['request']) == "string")? $_POST['request']:json_decode(json_encode($_POST['myData']))->request;

    ChromePhp::log($request);

    // <-- PRODUCT -->
    // get Address List
    if($request == "getAddressList"){
        $addressModel = new addressModel();
        $result = $addressModel->getAddress();
        $addressModel->close();
        //ChromePhp::log($result);
        echo $result;
    }
    // View Products
    if($request == "viewProducts"){
        $productModel = new productModel();
        $products = $productModel->getProducts();

        $array = array();
        while($row = mysqli_fetch_assoc($products)){
            
            $id = $row['id'];

            // view vnd number format in price
            $row['Price'] = number_format($row['Price']);

            // View html tags in description
            $row['Descriptions'] = str_replace("<","&lt;",$row['Descriptions']);

            // get address
            $address = $productModel->getAddress($id);
            $HouseNumber = strchr($address[0]," Đường",true);
            $row['HouseNumber'] = $HouseNumber;
            $row['Streets'] = $address[1];
            $row['Ward'] = $address[2];
            $row['District'] = $address[3];

            // get category
            $category = $productModel->getCategory($id);
            $row['Category'] = $category;
            $array[] = $row;
        }
        $productModel->close();

        echo json_encode($array);
    }

    // Insert Products
    if($request == 'insertProduct'){
        $data = json_decode(json_encode($_POST['myData']));
        $Name = $data->Name;
        $AddressID = $data->AddressID;

        $Category = $data->Category;
        $ShortDes = $data->ShortDes;
        $Des = $data->Des;
        $Price = $data->Price;
        $Hotline = $data->Hotline;
        $OpenTime = $data->OpenTime;
        $MostView = $data->MostView;
        $Avatar = $data->Avatar;
        $Images = $data->Images;

        ChromePhp::log($Avatar);
        ChromePhp::log($Images);

        $productModel = new productModel();
        $result = $productModel->insertProduct($Name,$AddressID,$Category,$ShortDes,$Des,$Price,$Hotline,$OpenTime,$MostView,$Avatar,$Images);

        $productModel->close();
        echo $result;
    }

    // Delete Product
    if($request == "deleteProduct"){
        $productModel = new productModel();
        $id = $_POST['id'];
        $result = $productModel->deleteProduct($id);
        $productModel->close();
        echo $result;
    }

     // Edit Product
    if($request == 'updateProduct'){
        $id = $_POST['id'];
        $Name = $_POST['Name'];
        $AddressID = $_POST['AddressID'];
        $Category = $_POST['Category'];
        $ShortDes = $_POST['ShortDes'];
        $Des = $_POST['Des'];
        $Price = preg_replace('/[^0-9\-]/', '', $_POST['Price']); // Removes special chars.
        $Hotline = $_POST['Hotline'];
        $OpenTime = $_POST['OpenTime'];
        $PostDate = $_POST['PostDate'];
        $MostView = $_POST['MostView'];
        $avatarID = $_POST['avatarID'];

        // Check if image already existed, if do then copy it from upload folder to product
        $image = dirname(dirname(dirname(__FILE__))) . "/upload/image_".$avatarID.".jpg";
        if(file_exists($image)){
            $des = dirname(dirname(dirname(__FILE__))) . "/upload/products/image_".$avatarID.".jpg";
            copy($image,$des);
        }

        $productModel = new productModel();
        $result = $productModel->editProduct($id,$Name,$AddressID,$Category,$ShortDes,$Des,$Price,$Hotline,$OpenTime,$PostDate,$MostView,$avatarID);
        $productModel->close();
        echo $result;
    }

    // <-- END PRODUCT -->

    // <-- USER -->
    // View User
    if($request == "viewUsers"){
        $userModel = new userModel();
        $result = $userModel->getUsers();
        $userModel->close();
        echo $result;
    }

    // Disable User
    if($request == "disableUser"){
        $userModel = new userModel();
        $id = $_POST["id"];
        $result = $userModel->disableUser($id);
        ChromePhp::log($result);
        echo $result;
    }

    // Enable User
    if($request == "enableUser"){
        $userModel = new userModel();
        $id = $_POST["id"];
        $result = $userModel->enableUser($id);
        echo $result;
    }

    // <-- END USER -->

    // <-- ADMIN -->
    if($request == "viewAdmins"){
        $userModel = new userModel();
        $admins = $userModel->getAdmins();
        $userModel->close();
        echo $admins;
    }
    // <-- END ADMIN -->

    // <-- ADDRESS -->
    // view Address
    if($request == "viewAddress"){
        $addressModel = new addressModel();
        $address = $addressModel->getAddress();
        $addressModel->close();
        echo $address;
    }

    // insert Address
    if($request == "insertAddress"){
        $addressModel = new addressModel();
        $data = json_decode(json_encode($_POST['myData']));

        $HouseNumber = $data->HouseNumber;
        $Street = $data->Street;
        $Ward = $data->Ward;
        $District = $data->District;

        $result = $addressModel->insertAddress($HouseNumber,$Street,$Ward,$District);
        $addressModel->close();
        echo $result;
    }

    //disable Address
    if($request == "disableAddress"){
        $addressModel = new addressModel();
        $id = $_POST["id"];
        $result = $addressModel->disableAddress($id);
        $addressModel->close();
        echo $result;
    }

    //enable Address
    if($request == "enableAddress"){
        $addressModel = new addressModel();
        $id = $_POST["id"];
        $result = $addressModel->enableAddress($id);
        $addressModel->close();
        echo $result;
    }

    //update Address
    if($request == "updateAddress"){
        $addressModel = new addressModel();
        $data = json_decode(json_encode($_POST["myData"]));

        $id = $data->id;
        $HouseNumber = $data->HouseNumber;
        $Street = $data->Street;
        $Ward = $data->Ward;
        $District = $data->District;

        $result = $addressModel->editAddress($id,$HouseNumber,$Street,$Ward,$District);
        $addressModel->close();
        echo $result;

    }

    // <-- END ADDRESS -->

    // <-- VIDEOS -->
    // view videos
    if($request == "viewVideos"){
        $videoModel = new videoModel();
        $videos = $videoModel->getVideos();
        $videoModel->close();
        echo $videos;
    }

    if($request == "getProductList"){
        $productModel = new productModel();
        $productList = $productModel->getProductList();
        $productModel->close();
        echo $productList;
    }

    if($request == "insertVideo"){
        $data = json_decode(json_encode($_POST["myData"]));
        $videoModel = new videoModel();
        $result = $videoModel->insertVideo($data->link,$data->name,$data->productID);
        $videoModel->close();
        echo $result;
    }

    if($request == "updateVideo"){
        $data = json_decode(json_encode($_POST["myData"]));
        $id = $data->id;
        $name = $data->name;
        $link = $data->link;
        $productID = $data->productID;
        $videoModel = new videoModel();
        $result = $videoModel->updateVideo($id,$link,$name,$productID);
        $videoModel->close();
        echo $result;
    }

    // <-- END VIDEOS -->

    // <-- COMMENTS -->

    if($request == "viewComments"){
        $commentModel = new commentModel();
        $comments = $commentModel->getComments();
        $commentModel->close();
        echo $comments;
    }

    if($request == "enableComment"){
        $id = $_POST["id"];
        $commentModel = new commentModel();
        $result = $commentModel->enableComment($id);
        $commentModel->close();
        echo $result;
    }

    if($request == "disableComment"){
        $id = $_POST["id"];
        $commentModel = new commentModel();
        $result = $commentModel->disableComment($id);
        $commentModel->close();
        echo $result;
    }

    // <-- END COMMENTS -->

    // <-- RATE -->
    if($request == "viewRates"){
        $rateModel = new rateModel();
        $rates  = $rateModel->viewRates();
        $rateModel->close();
        echo $rates;
    }
    // <-- END RATE -->

    // <-- RESTAURANT -->

    if($request == "viewRestaurants"){
        $restaurantModel = new restaurantModel();
        $restaurants = $restaurantModel->getRestaurants();
        $restaurantModel->close();
        echo $restaurants;
    }

    if($request == "disableRestaurant"){
        $id = $_POST["id"];
        $restaurantModel = new restaurantModel();
        $result = $restaurantModel->disableRestaurant($id);
        $restaurantModel->close();
        echo $result;
    }

    if($request == "enableRestaurant"){
        $id = $_POST["id"];
        $restaurantModel = new restaurantModel();
        $result = $restaurantModel->enableRestaurant($id);
        $restaurantModel->close();
        echo $result;
    }

    if($request == "updateRestaurant"){
        $data = json_decode(json_encode($_POST["myData"]));
        $restaurantModel = new restaurantModel();
        $result = $restaurantModel->updateRestaurant($data->id,$data->Name,$data->Service);
        $restaurantModel->close();
        echo $result;
    }

    if($request == "insertRestaurant"){
        $data = json_decode(json_encode($_POST["myData"]));
        $restaurantModel = new restaurantModel();
        $result = $restaurantModel->insertRestaurant($data->Name,$data->Service);
        $restaurantModel->close();
        echo $result;
    }

    // <-- END RESTAURANT -->

    // <-- NEWS -->

    if($request == "viewNews"){
        $newsModel = new newsModel();
        $news = $newsModel->getNews();
        $newsModel->close();
        echo $news;
    }

    if($request == "disableNews"){
        $id = $_POST["id"];
        $newsModel = new newsModel();
        $result = $newsModel->disableNews($id);
        $newsModel->close();
        echo $result;
    }

    if($request == "enableNews"){
        $id = $_POST["id"];
        $newsModel = new newsModel();
        $result = $newsModel->enableNews($id);
        $newsModel->close();
        echo $result;
    }

    if($request == "updateNews"){
        $data = json_decode(json_encode($_POST["myData"]));

        // Check if image already existed, if do then copy it from upload folder to product
        $image = dirname(dirname(dirname(__FILE__))) . "/upload/image_".$data->ImageID.".jpg";
        if(file_exists($image)){
            $des = dirname(dirname(dirname(__FILE__))) . "/upload/news/image_".$data->ImageID.".jpg";
            copy($image,$des);
        }

        $newsModel = new newsModel();
        $result = $newsModel->updateNews($data->id,$data->Name,$data->ShortDes,$data->Des,$data->UserID,$data->ImageID,$data->Postdate,$data->Author,$data->CommentID,$data->MostView);
        $newsModel->close();
        echo $result;
    }

    if($request == "insertNews"){
        $data = json_decode(json_encode($_POST["myData"]));
        $newsModel = new newsModel();
        $result = $newsModel->insertNews($data->Name,$data->ShortDes,$data->Des,$data->UserID,$data->ImageID,$data->Postdate,$data->Author,$data->CommentID,$data->MostView);
        $newsModel->close();
        echo $result;
    }

    // UPLOAD IMAGE
    if($request == "uploadNewsImage"){
        try {
            $myData = json_decode(json_encode($_POST['myData']));
            $data = $myData->data;
            /*
            $data = str_replace('data:image/png;base64,', '', $data);
            $data = str_replace('data:image/jpeg;base64,', '', $data);
            $data = str_replace('data:image/gif;base64,', '', $data);
            $data = str_replace('data:image/jpg;base64,', '', $data);
            */
            $data = base64_decode($data);
            $path = dirname(dirname(dirname(__FILE__))) . "/upload/news/";
            $rootPath = dirname(dirname(dirname(__FILE__))) . "/upload/";
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            // Add the image to database
            $imageModel = new imageModel();
            $filename = $imageModel->insertImage($myData->name);
            $imageModel->close();
            file_put_contents($rootPath . $filename. ".jpg", $data);
            file_put_contents($path . $filename. ".jpg", $data);
            return str_replace("image_","",$filename);
        } catch (Exception $e) {
            echo "Failed : " . $e;
        }
        echo "successful";
    }

    if($request == "uploadProductImage"){
    try {
        $myData = json_decode(json_encode($_POST['myData']));
        $data = $myData->data;
        /*
        $data = str_replace('data:image/png;base64,', '', $data);
        $data = str_replace('data:image/jpeg;base64,', '', $data);
        $data = str_replace('data:image/gif;base64,', '', $data);
        $data = str_replace('data:image/jpg;base64,', '', $data);
        */
        $data = base64_decode($data);
        $path = dirname(dirname(dirname(__FILE__))) . "/upload/products/";
        $rootPath = dirname(dirname(dirname(__FILE__))) . "/upload/";
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        // Add the image to database
        $imageModel = new imageModel();
        $filename = $imageModel->insertImage($myData->name);
        $imageModel->close();
        file_put_contents($rootPath . $filename. ".jpg", $data);
        file_put_contents($path . $filename. ".jpg", $data);
        return $filename;
    } catch (Exception $e) {
        echo "Failed : " . $e;
    }
    echo "successful";
}

    // select one image from server (add to news)
    if($request == "selectImage"){
        $id = $_POST["id"];
        $imageModel = new imageModel();

    }

    // <-- END NEWS -->
	
	// LOGIN
    if($request == "login"){
        $username = $_POST["username"];
        $password = $_POST["password"];
		ChromePhp::log($username);
		ChromePhp::log(sha1($password));
        $userModel  = new userModel();
        $result = $userModel->getAdmin($username,sha1($password));
        if($result != null){
            session_start();
            $_SESSION["admin"] = $result;
            echo "successful";
        } else{
            echo "Failed";
        }

    }
    if($request == "logout"){
        session_start();
        if (isset($_SESSION['admin'])){
            unset($_SESSION['admin']); // xóa session login
             echo "successful";
        }else echo "Failed";
       

    }
     // UPLOAD IMAGE TO SERVER
    if($request == "uploadImage"){
        try {
            $myData = json_decode(json_encode($_POST['myData']));
            $data = $myData->data;

            $data = base64_decode($data);

            $rootPath = dirname(dirname(dirname(__FILE__))) . "/upload/";

            // Add the image to database
            $imageModel = new imageModel();
            $filename = $imageModel->insertImage($myData->name);
            $imageModel->close();
            file_put_contents($rootPath . $filename. ".jpg", $data);

            echo str_replace("image_","",$filename);
        } catch (Exception $e) {
            echo "Failed : " . $e;
        }
    }
?>