
    <!-- start: Meta -->
    <meta charset="utf-8">
    <title>Admin</title>
    <meta name="description" content="Bootstrap Metro Dashboard">
    <meta name="author" content="Dennis Ji">
    <meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <!-- end: Meta -->

    <!-- start: Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- end: Mobile Specific -->

    <!-- start: CSS -->
    <link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link id="base-style" href="css/style.css" rel="stylesheet">
    <link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
    <link href="css/style-custom.css" rel="stylesheet">
    <!-- end: CSS -->


    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link id="ie-style" href="css/ie.css" rel="stylesheet">
    <![endif]-->

    <!--[if IE 9]>
    <link id="ie9style" href="css/ie9.css" rel="stylesheet">
    <![endif]-->

    <!-- start: Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- end: Favicon -->

 

</head>

<body>
<!-- start: Header -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="index.html"><span>Metro</span></a>

            <!-- start: Header Menu -->
            <div class="nav-no-collapse header-nav">
                <ul class="nav pull-right">
                    <!-- start: User Dropdown -->
                    <li class="dropdown">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">

                            <i class="halflings-icon white user"></i><?php

                            if(isset($_SESSION["admin"])){
                                $user = $_SESSION["admin"]["UserName"];
                                echo $user;
                            }
                            ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-menu-title">
                                <span>Account Settings</span>
                            </li>
                            <li><a href="#"><i class="halflings-icon user"></i> Profile</a></li>
                            <li><a href="#" id="LogoutAdmin"><i class="halflings-icon off"></i> Logout</a></li>
                        </ul>
                    </li>
                    <!-- end: User Dropdown -->
                </ul>
            </div>
            <!-- end: Header Menu -->

        </div>
    </div>
</div>
<!-- start: Header -->

<div class="container-fluid-full">
    <div class="row-fluid">

        <!-- start: Main Menu -->
        <div id="sidebar-left" class="span2">
            <div class="nav-collapse sidebar-nav">
                <ul class="nav nav-tabs nav-stacked main-menu">
                    <li><a href="admin.html"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>
                    <li><a href="adminPage.php"><i class="icon-align-justify"></i><span class="hidden-tablet"> Admin</span></a></li>
                    <li><a href="userPage.php"><i class="icon-align-justify"></i><span class="hidden-tablet"> Users</span></a></li>
                    <li><a href="productPage.php"><i class="icon-align-justify"></i><span class="hidden-tablet"> Products</span></a></li>
                    <li><a href="newsPage.php"><i class="icon-align-justify"></i><span class="hidden-tablet"> News</span></a></li>
                    <li><a href="videoPage.php"><i class="icon-align-justify"></i><span class="hidden-tablet"> Videos</span></a></li>
                    <li><a href="restaurantPage.php"><i class="icon-align-justify"></i><span class="hidden-tablet"> Restaurant</span></a></li>
                    <li><a href="addressPage.php"><i class="icon-align-justify"></i><span class="hidden-tablet"> Address</span></a></li>
                    <li><a href="ratePage.php"><i class="icon-star"></i><span class="hidden-tablet"> Rate</span></a></li>
                    <li><a href="commentPage.php"><i class="icon-star"></i><span class="hidden-tablet"> Comment</span></a></li>
                    <li><a href="imagePage.php"><i class="icon-star"></i><span class="hidden-tablet"> Image</span></a></li>
                    
                </ul>
            </div>
        </div>
        <!-- end: Main Menu -->
        