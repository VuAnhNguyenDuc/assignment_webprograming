<?php
    class addressModel{
        public function __construct(){
            require 'connect.php';
        }
        public function getAddress(){
            global $dbc;
            $query = "SELECT * FROM adress";
            if($result = mysqli_query($dbc,$query)){
                $address = array();
                while($row = mysqli_fetch_assoc($result)){
                    $HouseNumber = strchr($row["AdressFull"]," Đường",true);
                    $row["HouseNumber"] = $HouseNumber;
                    $address[] = $row;
                }
                return json_encode($address);
            } else{
                $error = mysqli_error($dbc);
                echo str_replace("'","",$error);
            }
        }
        public function disableAddress($id){
            global $dbc;
            $query = "UPDATE adress SET Status = '-1' WHERE id='".$id."'";
            if($result = mysqli_query($dbc,$query)){
                echo "successful";
            } else{
                $error = mysqli_error($dbc);
                echo str_replace("'","",$error);
            }
        }
        public function enableAddress($id){
            global $dbc;
            $query = "UPDATE adress SET Status = '1' WHERE id='".$id."'";
            if($result = mysqli_query($dbc,$query)){
                echo "successful";
            } else{
                $error = mysqli_error($dbc);
                echo str_replace("'","",$error);
            }
        }
        public function editAddress($id,$HouseNumber,$Street,$Ward,$District){
            global $dbc;
            $AddressFull = $HouseNumber." Đường ".$Street.", Phường ".$Ward.", Quận ".$District.", TP.HCM";
            $dbc->begin_transaction();
            try{
                $query = "UPDATE adress SET Streets='".$Street."',Ward='".$Ward."',District='".$District."',AdressFull = '".$AddressFull."' WHERE id='".$id."' ";
                if($result = mysqli_query($dbc,$query)){
                    $dbc->commit();
                    echo "successful";
                } else{
                    $error = mysqli_error($dbc);
                    throw new Exception(str_replace("'","",$error));
                }
            } catch(Exception $ex){
                $dbc->rollback();
                echo $ex;
            }
        }
        public function insertAddress($HouseNumber,$Street,$Ward,$District){
            global $dbc;
            $AddressFull = $HouseNumber." Đường ".$Street.", Phường ".$Ward.", Quận ".$District.", TP.HCM";
            $dbc->begin_transaction();
            try{
                $query = "INSERT INTO adress(Streets, Ward, District, City, AdressFull, Status) VALUE('".$Street."','".$Ward."','".$District."','TP.HCM','".$AddressFull."','1')";
                if($result = mysqli_query($dbc,$query)){
                    $dbc->commit();
                    echo "successful";
                } else{
                    $error = mysqli_error($dbc);
                    throw new Exception(str_replace("'","",$error));
                }
            } catch(Exception $ex){
                $dbc->rollback();
                echo $ex;
            }
        }
        public function close(){
            global  $dbc;
            $dbc->close();
        }
    }
?>