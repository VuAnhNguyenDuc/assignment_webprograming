<?php
    class imageModel{
        public function __construct(){
            require "connect.php";
        }
        public function insertImage($name){
            global $dbc;
            $dbc->begin_transaction();
            try {
                $query = "INSERT INTO imagetbx(id,Name, Numbers) VALUE (NULL,'" . $name . "', 1)";
                if ($result = mysqli_query($dbc, $query)) {
                    $id = mysqli_insert_id($dbc);
                    $query = "UPDATE imagetbx SET Name='" ."image_". $id . ".jpg' WHERE id='" . $id . "'";
                    mysqli_query($dbc, $query);
                    $dbc->commit();
                    return "image_".$id;
                }
            } catch (Exception $e) {
                $dbc->rollback();
                return 0;
            }
        }
        public function getImages(){
            global $dbc;
            $query = "SELECT * FROM imagetbx";
            $images = array();
            if($result = mysqli_query($dbc,$query)){
                while($row = mysqli_fetch_assoc($result)){
                    $images[] = $row;
                }
            }
            return json_encode($images);
        }
        public function close(){
            global $dbc;
            $dbc->close();
        }
    }