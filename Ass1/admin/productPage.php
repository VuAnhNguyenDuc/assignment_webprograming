<!DOCTYPE html>
<html lang="en">
<head>
			
</head>

<body>
		 <?php require "header.php"; ?>
			
			<!-- start: Content -->
			<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.html">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="productPage.php">Product</a></li>
			</ul>

			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Products</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">

						<a role="button" href="insert.php?request=insertProduct" id="insertBtn" class="btn btn-primary insert-btn">Insert product</a>

						<table class="table table-striped table-bordered bootstrap-datatable datatable">
							<thead>
							 	 <tr>
									  <th>ID</th>
									  <th colspan='3'>Name</th>
									  <th colspan='3'>Address</th>
									  <th>Category</th>
									  <th>Short Description</th>
									  <th colspan='3'>Description</th>
									  <th>Price</th>
									  <th>Hotline</th>
									  <th>Open Time</th>
									  <th>Post Date</th>
									  <th>Status</th>
									  <th>MostView</th>
									  <th>Cover Images</th>
									  <th colspan='2'>Actions</th>
								  </tr>
						    </thead>
						    <tbody id="products">
						    </tbody>
					  	</table>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->	
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	<div class="modal hide fade" id="images-product">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">

			<select id="image-picker-avatar-product" class="image-picker show-html">
				<option value=""></option>
				<?php
				require "admin_model/imageModel.php";
				$imageModel = new imageModel();
				$images = $imageModel->getImages();
				$imageModel->close();
				$temp = json_decode($images,true);

				$path = "../upload/";

				foreach($temp as $image){
					$id = $image["id"];
					$imageName = $image["Name"];
					$imagePath = $path.$imageName;
					//<?php echo $imagePath
					?>
					<option data-img-src="<?php echo $imagePath ?>" value="<?php echo $id; ?>"><?php echo $imageName ?></option>
					<?php
				}
				?>
			</select>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" id="select-image" class="btn btn-primary">Select</a>
		</div>
	</div>
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2013 <a href="http://jiji262.github.io/Bootstrap_Metro_Dashboard/" alt="Bootstrap_Metro_Dashboard">Bootstrap Metro Dashboard</a></span>
			
		</p>

	</footer>
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery-migrate-1.0.0.min.js"></script>

<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

<script src="js/jquery.ui.touch-punch.js"></script>

<script src="js/modernizr.js"></script>

<script src="js/bootstrap.min.js"></script>

<script src="js/jquery.cookie.js"></script>

<script src='js/fullcalendar.min.js'></script>

<script src='js/jquery.dataTables.min.js'></script>

<script src="js/excanvas.js"></script>
<script src="js/jquery.flot.js"></script>
<script src="js/jquery.flot.pie.js"></script>
<script src="js/jquery.flot.stack.js"></script>
<script src="js/jquery.flot.resize.min.js"></script>

<script src="js/jquery.chosen.min.js"></script>

<script src="js/jquery.uniform.min.js"></script>

<script src="js/jquery.cleditor.min.js"></script>

<script src="js/jquery.noty.js"></script>

<script src="js/jquery.elfinder.min.js"></script>

<script src="js/jquery.raty.min.js"></script>

<script src="js/jquery.iphone.toggle.js"></script>

<script src="js/jquery.uploadify-3.1.min.js"></script>

<script src="js/jquery.gritter.min.js"></script>

<script src="js/jquery.imagesloaded.js"></script>

<script src="js/jquery.masonry.min.js"></script>

<script src="js/jquery.knob.modified.js"></script>

<script src="js/jquery.sparkline.min.js"></script>

<script src="js/counter.js"></script>

<script src="js/retina.js"></script>

<script src="js/custom.js"></script>
<script src="js/image-picker.js"></script>

<script src="js/image-picker.min.js"></script>

<script type="text/javascript" src="ajax/ajax_news_handler.js"></script>

		<script type="text/javascript" src="ajax/ajax_product_handler.js"></script>

		<!--<script type="text/javascript" src="ajax/ajax_handler.js"></script>-->
	<!-- end: JavaScript-->
	
	
</body>
</html>
