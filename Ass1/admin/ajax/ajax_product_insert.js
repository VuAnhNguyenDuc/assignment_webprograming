// SELECT IMAGES FROM SERVER
    $('.select-images').click(function(e){
        e.preventDefault();
        $('#server-images').modal('show');
    });

    $('.select-images-avatar').click(function(e){
        e.preventDefault();
        $('#server-images').modal('show');
    });

    // handle preview upload image
    $("#imageInput").change(function(event){
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = reader.result;
            var newImage = "<div class='masonry-gallery'><div class='masonry-thumb'>" +
                "<img id='newsImage' class='grayscale' src='"+dataURL+"' alt='images'>" +
                "</div></div>";
            $("#imageDisplay").html("");
            $("#imageDisplay").append(newImage);
        };
        reader.readAsDataURL(input.files[0]);
    });

    $("#imageInputAvatar").change(function(event){
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = reader.result;
            var newImage = "<div class='masonry-gallery'><div class='masonry-thumb'>" +
                "<img id='avatarImage' class='grayscale' src='"+dataURL+"' alt='images'>" +
                "</div></div>";
            $("#imageDisplayAvatar").html("");
            $("#imageDisplayAvatar").append(newImage);
        };
        reader.readAsDataURL(input.files[0]);
    });

    // upload image to server
    $('#uploadImage').click(function(){
        var src = $("#newsImage").attr("src");

        var base64 = "";
        if(src.indexOf("base64")!=-1){
            base64 = src.replace(/data:image\/(jpeg|png|gif|jpg);base64,/, '');
        }

        var data = {
            request : (globalRequest != "insertProduct")?"uploadNewsImage":"uploadProductImage",
            data : base64,
            name : "demo.jpg"
        };

        $.ajax({
            type: "POST",
            url: phpController,
            cache: false,
            data:{ myData : data},
            dataType: "text",
            success: function(result){
                if(result.indexOf("Failed") < 0){
                    alert("Uploaded successful");
                    $("#imageName").val(result);
                } else{
                    console.log(result);
                }
            },
            error : function(xhr){
                var err = eval("(" + xhr.responseText +  ")");
                console.log(err.Message);
            }
        });
    });

    $('#uploadImageAvatar').click(function(){
        var src = $("#avatarImage").attr("src");

        var base64 = "";
        if(src.indexOf("base64")!=-1){
            base64 = src.replace(/data:image\/(jpeg|png|gif|jpg);base64,/, '');
        }
        var data = {
            request : "uploadProductImage",
            data : base64,
            name : "demo.jpg"
        };
        $.ajax({
            type: "POST",
            url: phpController,
            cache: false,
            data:{ myData : data},
            dataType: "text",
            success: function(result){
                if(result.indexOf("Failed") < 0){
                    alert("Uploaded successful");
                    $("#imageNameAvatar").val(result);
                } else{
                    console.log(result);
                }
            },
            error : function(xhr){
                var err = eval("(" + xhr.responseText +  ")");
                console.log(err.Message);
            }
        });
    });

    // call image picker to use with select images from server
    if($("#image-picker").length){
        $("#image-picker").imagepicker();
    }

    if($("#image-picker-avatar").length){
        $("#image-picker-avatar").imagepicker();
    }

    if($("#image-picker-multi").length){
        $("#image-picker-multi").imagepicker();
    }

    // select ONE image and set the value to the select tag
    $("#select-image").click(function(){
        var imageID = $("#image-picker").val();
        $("#select-image").val(imageID);
        alert("Image set!");
        $('#server-images').modal('hide');

        var path = "../upload/image_"+imageID+".jpg";

        var newImage = "<div class='masonry-gallery'><div class='masonry-thumb'>" +
            "<img id='newsImage' class='grayscale' src='"+path+"' alt='images'>" +
            "</div></div>";
        if(globalRequest == "insertProduct"){
            $("#imageDisplayAvatar").html("");
            $("#imageDisplayAvatar").append(newImage);
        }
        $("#imageDisplay").html("");
        $("#imageDisplay").append(newImage);
    });

    // select MULTIPLE image and set the values to the select tag
    $("#select-image-multi").click(function(){
        var imageIDs = $('select#image-picker-multi').val();
        console.log(imageIDs);
        $("#select-image").val(imageIDs);
        alert("Images set!");
        $('#server-images').modal('hide');
        $("#imageDisplayAvatar").html("");
        $.each(imageIDs,function(key,value){
            var path = "../upload/image_"+value+".jpg";

            var newImage = "<div class='masonry-gallery'><div class='masonry-thumb'>" +
                "<img id='newsImage' class='grayscale' src='"+path+"' alt='images'>" +
                "</div></div>";

            $("#imageDisplayAvatar").append(newImage);
        });
    });