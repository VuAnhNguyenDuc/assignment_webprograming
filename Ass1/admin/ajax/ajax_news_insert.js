$(window).load(function(){
    console.log("running");
    /*var phpController = "../admin/admin_controller/Controller.php";
    var pathname = document.location.pathname.match(/[^\/]+$/)[0];
    console.log("path = "+pathname);
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),//decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var globalRequest = getUrlParameter("request");

    // SELECT IMAGES FROM SERVER
    $('.select-images').click(function(e){
        e.preventDefault();
        $('#server-images').modal('show');
    });

    // handle preview upload image
    $("#imageInput").change(function(event){
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = reader.result;
            var newImage = "<div class='masonry-gallery'><div class='masonry-thumb'>" +
                "<img id='newsImage' class='grayscale' src='"+dataURL+"' alt='images'>" +
                "</div></div>";
            $("#imageDisplay").html("");
            $("#imageDisplay").append(newImage);
        };
        reader.readAsDataURL(input.files[0]);
    });

    // upload image to server
    $('#uploadImage').click(function(){
        var src = $("#newsImage").attr("src");

        var base64 = "";
        if(src.indexOf("base64")!=-1){
            base64 = src.replace(/data:image\/(jpeg|png|gif|jpg);base64,/, '');
        }

        var data = {
            request : (globalRequest != "insertProduct")?"uploadNewsImage":"uploadProductImage",
            data : base64,
            name : "demo.jpg"
        };

        $.ajax({
            type: "POST",
            url: phpController,
            cache: false,
            data:{ myData : data},
            dataType: "text",
            success: function(result){
                if(result.indexOf("Failed") < 0){
                    alert("Uploaded successful");
                    $("#imageName").val(result);
                } else{
                    console.log(result);
                }
            },
            error : function(xhr){
                var err = eval("(" + xhr.responseText +  ")");
                console.log(err.Message);
            }
        });
    });

    // call image picker to use with select images from server
    if($("#image-picker").length){
        $("#image-picker").imagepicker();
    }

    // select ONE image and set the value to the select tag
    $("#select-image").click(function(){
        var imageID = $("#image-picker").val();
        $("#select-image").val(imageID);
        alert("Image set!");
        $('#server-images').modal('hide');

        var path = "../upload/image_"+imageID+".jpg";

        var newImage = "<div class='masonry-gallery'><div class='masonry-thumb'>" +
            "<img id='newsImage' class='grayscale' src='"+path+"' alt='images'>" +
            "</div></div>";
        if(globalRequest == "insertProduct"){
            $("#imageDisplayAvatar").html("");
            $("#imageDisplayAvatar").append(newImage);
        }
        $("#imageDisplay").html("");
        $("#imageDisplay").append(newImage);
    });

    // insert news
    $('#insertNews').click(function () {
        alert("looix");
        var Name = $('#insertName').val();
        var ShortDes = $('#insertShortDes').val();
        var Des = $('#insertDes').val();
        var UserID = $('#insertUserID').val();
        var ImageID = ($("#image-picker").val() != "")?$('#imageName').val():$("#image-picker").val();
        console.log(ImageID);
        if(ImageID == ""){
            alert("Please select an image");
            return false;
        }
        var Postdate = $('#insertPostdate').val();
        var Author = $('#insertAuthor').val();
        var CommentID = $('#insertCommentID').val();
        var MostView = $('#insertMostView').val();

        var data = {
            request: "insertNews",
            Name: Name,
            ShortDes : ShortDes,
            Des : Des,
            UserID : UserID,
            ImageID : ImageID,
            Postdate : Postdate,
            Author : Author,
            CommentID : CommentID,
            MostView : MostView
        };

        $.ajax({
            type: "POST",
            url: phpController,
            dataType: "text",
            data: {myData: data},
            cache: false,
            success:function(result){
                console.log(result);
                if(result == "successful"){
                    alert("Record successfully inserted!");
                    window.location = "newsPage.php";
                }else{
                    alert("Failed:" + result);
                }
            },
            error:function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    });*/
});