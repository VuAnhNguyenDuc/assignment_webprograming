$(window).load(function(){
    var phpController = "../admin/admin_controller/Controller.php";
    var pathname = "";
    if (document.location.pathname.match(/[^\/]+$/)!=null){
        pathname = document.location.pathname.match(/[^\/]+$/)[0];
    }

    // handle preview upload image
    $("#imageInput").change(function(event){
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = reader.result;
            var newImage = "<img id='inputImage' src='"+dataURL+"' alt='images'>";
            $("#imageDisplay").html("");
            $("#imageDisplay").append(newImage);
        };
        reader.readAsDataURL(input.files[0]);
    });

    // upload image to server
    $('#uploadImage').click(function(){
        var src = $("#inputImage").attr("src");

        var base64 = "";
        if(src.indexOf("base64")!=-1){
            base64 = src.replace(/data:image\/(jpeg|png|gif|jpg);base64,/, '');
        }

        var data = {
            request : "uploadImage",
            data : base64,
            name : "demo.jpg"
        };

        $.ajax({
            type: "POST",
            url: phpController,
            cache: false,
            data:{ myData : data},
            dataType: "text",
            success: function(result){
                if(result.indexOf("Failed") < 0){
                    alert("Uploaded successful");
                    window.location = "imagePage.php";
                } else{
                    console.log(result);
                }
            },
            error : function(xhr){
                var err = eval("(" + xhr.responseText +  ")");
                console.log(err.Message);
            }
        });
    });
});