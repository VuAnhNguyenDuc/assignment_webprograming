$(window).load(function(){
    var phpController = "../admin/admin_controller/Controller.php";
    var pathname = "";
    if (document.location.pathname.match(/[^\/]+$/)!=null){
        pathname = document.location.pathname.match(/[^\/]+$/)[0];
    }
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),//decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    var globalRequest = getUrlParameter("request");

    var globalID = 0;
    var globalTr = "";

    // call image picker to use with select images from server
    if($("#images-product").length){
        $("#image-picker-avatar-product").imagepicker();
    }

    // SELECT IMAGES FROM SERVER
    $("#products").on("click",".selectAvatar",function(e){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");
        globalID = id;
        globalTr = tr;

        console.log(id);

        e.preventDefault();
        $('#images-product').modal('show');
    });

    // select ONE image and set the value to the select tag
    $("#select-image").click(function(){
        var imageID = $("#image-picker-avatar-product").val();

        globalTr.find("input.avatarID").val(imageID);
        $('#images-product').modal('hide');

        alert("Image set!");

        var path = "../upload/image_"+imageID+".jpg";

        var newImage = "<img style='width: 100px; height: 100px;' src='"+path+"' alt='images'>";

        globalTr.find("div.avatarDisplay").html("");
        globalTr.find("div.avatarDisplay").append(newImage);

        globalID = 0;
        globalTr = "";
    });

    // handle preview upload image
    $("#products").on("change",".avatarInput",function(event){
        globalTr = $(this).closest("tr");
        globalID = globalTr.attr("id");

        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = reader.result;
            var newImage = "<img class='newsAvatar' style='width: 100px; height: 100px;' src='"+dataURL+"' alt='images'>";
            $('select#image-picker-avatar-product option').removeAttr("selected");
            globalTr.find("div.avatarDisplay").html("");
            globalTr.find("div.avatarDisplay").append(newImage);
        };
        reader.readAsDataURL(input.files[0]);
    });

    // upload image to server
    $("#products").on("click",".uploadAvatar",function(){
        var src = globalTr.find(".newsAvatar").attr("src");

        var base64 = "";
        if(src.indexOf("base64")!=-1){
            base64 = src.replace(/data:image\/(jpeg|png|gif|jpg);base64,/, '');
        }

        var data = {
            request : "uploadProductImage",
            data : base64,
            name : "demo.jpg"
        };

        console.log(data);

        $.ajax({
            type: "POST",
            url: phpController,
            cache: false,
            data:{ myData : data},
            dataType: "text",
            success: function(result){
                if(result.indexOf("Failed") < 0){
                    alert("Uploaded successful");
                    globalTr.find("input.avatarID").val(result);
                    globalID = 0;
                    globalTr = "";
                } else{
                    console.log(result);
                }
            },
            error : function(xhr){
               
            }
        });
    });

    // PRODUCT
    // get product list
    if(pathname == "productPage.php"){
         
        $.ajax({
            type: 'POST',
            url: phpController,
            data: "request=getAddressList",
            dataType: "json",
            success: function(result){
                 showProducts(result);
            },
            error: function(xhr){
                
            }
        });

        function showProducts(result){
            var addressList = result;
            //console.log(addressList);
           
            $.ajax({
                type: 'POST',
                url: phpController,
                data: 'request=viewProducts',
                cache: false,
                success: function(products){
                    $('#products').html("");

                    $.each(products,function(key,value){
                        var addressOptions = "";
                        var selectId = "select"+value.id;

                        // DOI LINK CHO NAY NE THAT
                        var imgSrc = "../upload/products/image_"+value.avatar+".jpg";

                        $.each(addressList,function(key,addressvalue){
                            addressOptions += "<option value='"+addressvalue.id+"'>"+addressvalue.AdressFull+"</option>";
                        });

                        var status = (value.Status == "1")?"Active":"Disabled";
                        $('#products').append('<tr id=' + value.id + '>' +
                            "<td class='center ID'>" + value.id + "</td>" +
                            "<td class='center Name' contenteditable='true' colspan='3'>" + value.Name + "</td>" +

                            "<td class='center' colspan='3'>" +
                            "<select id='"+selectId+"' class='getAddress' data-rel='chosen'>" +
                            addressOptions +
                            "</select>" +
                            "</td>" +

                            "<td class='center Category' contenteditable='true'>" + value.Category + "</td>" +
                            "<td class='center ShortDes' contenteditable='true'>" + value.ShortDes + "</td>" +
                            "<td class='center Des' contenteditable='true' colspan='3'>" + value.Descriptions + "</td>" +
                            "<td class='center Price' contenteditable='true'>" + value.Price + "</td>" +
                            "<td class='center Hotline' contenteditable='true'>" + value.Hotline + "</td>" +
                            "<td class='center OpenTime' contenteditable='true'>" + value.OpentTime + "</td>" +
                            "<td class='center PostDate' contenteditable='true'>" + value.PostDate + "</td>" +
                            "<td class='center Status'>" + status + "</td>" +
                            "<td class='center MostView' contenteditable='true'>" + value.MostView + "</td>" +
                            "<td>" +
                            "<div style='margin-bottom: 20px;' class='avatarDisplay'>" +
                            "<img style='width: 100px; height: 100px;' src='"+imgSrc+"' alt='alt'>" +
                            "</div>" +
                            "<input type='file' class='avatarInput'>"+
                            "<button type='button' class='uploadAvatar btn btn-info'>Upload</button> " +
                            "<button type='button' class='selectAvatar btn btn-info'>Select</button>" +
                                "<input type='text' value='"+value.avatar+"' class='avatarID' style='display: none;'> " +
                            "</td>" +
                            "<td class='center' colspan='2'><button class='updateProduct btn btn-success' style='margin-right: 20px;'>Save</button><button class='deleteProduct btn btn-danger'>Disable</button></td>" +
                            '</tr>');
                        var addressID = value.Address_ID;
                        $("#"+selectId).val(addressID);
                    });
                    //$("#select2").val("");
                },
                error:function(xhr, status, error){
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        }

    }

    // delete product
    $("#products").on("click",".deleteProduct",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        if(confirm("Do you wish to delete this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: "request=deleteProduct&id="+id,
                dataType: "text",
                cache: false,
                error: function(xhr, status, error){
                    console.log(status);
                    console.log(xhr.responseText);
                },
                success: function(result){
                    if(result == "successful"){
                        alert('Delete Successful');
                        tr.find(".Status").html("Disabled");
                    } else {
                        alert("Delete Failed");
                    }
                }
            });
        }
    });

    // update product
    $('#products').on("click",".updateProduct",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        // Update values
        var Name = tr.find(".Name").html();
        var AddressID = tr.find(".getAddress").val();
        var Category = tr.find(".Category").html();
        var ShortDes = tr.find(".ShortDes").html();
        var Des = tr.find(".Des").html();
        var Price = tr.find(".Price").html();
        var Hotline = tr.find(".Hotline").html();
        var OpenTime = tr.find(".OpenTime").html();
        var PostDate = tr.find(".PostDate").html();
        var MostView = tr.find(".MostView").html();
        var avatarID = tr.find(".avatarID").val();

        var strData = "&Name="+Name+"&AddressID="+AddressID+"&Category="+Category+"&ShortDes="+ShortDes+"&Des="+Des+"&Price="+Price+"&Hotline="+Hotline+"&OpenTime="+OpenTime+"&PostDate="+PostDate+"&MostView="+MostView+"&avatarID="+avatarID ;

        console.log(strData);

        var path = "../upload/image_"+avatarID+".jpg";

        if(confirm("Do you wish to update this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: "request=updateProduct&id="+id+strData,
                dataType: "text",
                success: function(result){
                    if(result == "successful"){
                        alert("Update successful");
                        location.reload();
                    } else{
                        alert("Failed: "+result);
                    }
                }
            });
        }
    });

    // insert product
    $('#insertProduct').click(function () {
        var Name = $('#insertName').val();
        var AddressID = $("#getAddressList").val();
        var RestaurantID = $("#getRestaurantList").val();
        var Category = $('#insertCategory').val();
        var ShortDes = $('#insertShortDes').val();
        var Des = $('#insertDes').val();
        var Price = $('#insertPrice').val();
        var Hotline = $('#insertHotline').val();
        var OpenTime = $('#insertOpenTime').val();
        var MostView = $('#insertMostView').val();
        var Avatar = $("#imageNameAvatar").val();

        var data = {
            request: "insertProduct",
            AddressID : AddressID,
            RestaurantID : RestaurantID,
            Category : Category,
            Name: Name,
            ShortDes: ShortDes,
            Des: Des,
            Price: Price,
            Hotline: Hotline,
            OpenTime: OpenTime,
            MostView: MostView,
            Avatar: Avatar,
            Images : imageArr
        };

        $.ajax({
            type: "POST",
            url: phpController,
            dataType: "text",
            data: {myData: data},
            cache: false,
            success:function(result){
                console.log(result);
                if(result == "successful"){
                    alert("Record successfully inserted!");
                    window.location = "productPage.php";
                }else{
                    alert("Failed:" + result);
                }
            },
            error:function(xhr,status,error){
                console.log(status);
                console.log(xhr.responseText);
            }
        });
    });
});