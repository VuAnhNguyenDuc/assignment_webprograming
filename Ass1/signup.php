﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Đăng ký tài khoản</title>
    

    <link href="css/style_contact.css" rel="stylesheet">
    
    <!-- account styel css -->
    <link href="css/style_account.css" rel="stylesheet">
    <!-- Latest compiled and minified JavaScript -->

  </head>

  <body style="background: #f6f6f6">
    <?php require_once('header.php');?>

    <div id="wrap">
        <div class="container" id="main-content"><!-- Use wrapper for wide or boxed mode -->
            <div class="row">
                <div class="col-md-2">
                    
                </div>
                <div class="col-md-8">
                     <div class="signup_form">
                        <div class="title" style="text-align: center;">
                            Đăng ký tài khoản</div>
                            <ul id="error">
                            <li>Xin vui lòng cung cấp chính xác thông tin</li>
                            <li>Các trường bắt buộc được đánh dấu *</li>
                            
                            </ul>
                            
                            <hr>
                        <form id="sign_up" method="post" >
                            <div class="form-group col-md-6" id="fname">
                                <input id="first_name" type="text" name="su_fname" placeholder="Họ*" class="form-control form-item" required="required"></div>
                            <div class="form-group col-md-6" id="lname">
                                <input id="last_name" type="text" name="su_lname" placeholder="Tên*" class="form-control form-item" required="required"></div>
                            <div class="form-group">
                                <input id="username" type="text" name="su_uname" placeholder="Tên tài khoản*" class="form-control form-item" required="required"></div>
                            <div class="form-group">
                                <input id="password" type="password" name="su_pass" placeholder="Mật khẩu*" class="form-control form-item" required="required"></div>
                            <div class="form-group">
                                <input id="repassword" type="password" name="su_confirmpass" placeholder="Xác nhận mật khẩu*" class="form-control form-item" required="required"></div>
                            <div class="form-group">
                                <input id="email" type="email" title="su_email" placeholder="example@gmail.com*" class="form-control form-item" required="required"></div>
                            <div class="form-group">
                                <input id="phone" type="text" title="su_phone" placeholder="Số điện thoại*" class="form-control form-item" required="required"></div>
                            <div class="form-group">
                                <input id="address" type="text" title="su_addr" placeholder="Địa chỉ*" class="form-control form-item" required="required"></div>
                            <div class="form-group">
                               <label style="float:left;">Ngày sinh:</label>
                                <input id="birthday" type="date" title="su_dob" class="form-control form-item" required="required"></div>
                            <div class="form-group">
                                <label style="float:left;">Ảnh đại diện:</label>
                                <input  name="file" type="file" id="file" required="required"/>
                            </div>
                            <div class="col-md-12 text-center" style="margin-bottom:10px;">
                                <img id="avatar" width="70%" src="http://toiladangovap.com/styles/default/images/no_thumbnails.gif">
                            </div>
                            <div class="form-group">
                                <label style="float:left;line-height:30px">Nhập mã xác nhận:</label>
                                <div style="float:left">
                                    <input  type="text" name="captcha" id="captcha" maxlength="6" size="6" required="required" />
                                    <img  id="image_captcha" src="captcha/captcha_code.php"/>
                                    <input  type="button" onclick="Reset_Captcha()" class="btn btn-primary" style="background:url(images/change_captcha.png);    background-size: 30px;width: 30px;height: 30px;background-color: lightyellow;"/>
                                </div>
                                <span id="error_captcha" style ="color:red;float:left;line-height:35px;margin-left:5px;display:none">*Mã nhập không đúng!</span>
                                <div style="clear:both"></div>
                            </div>
                            <div class="form-group">
                               
                                <input style="float:left" type="checkbox" required="required" />
                                <label style="margin-top:3px;float:left">Chấp nhận các điều khoản và chính sách của chúng tôi.</label>
                                <div style="clear:both"></div>
                            </div>
                            <button type="submit" class="btn btn-1">Đăng ký</button>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div id="wait_bg" style="opacity:0; pointer-events:none ;position:fixed; height:100%; width:100%; display:table; background: rgba(0, 0, 0, 0.75); top: 0; left: 0;">
            <div style=" width:100%; height:100px;display:table-cell; vertical-align:middle; text-align:center">
                <img src="images/Preloader_10.gif" style="border-radius:50%; width:100px" />
                <p style="color:#fff">Đang xử lý...</p>
            </div>
    </div>
	<?php require_once('footer.php');?>
    <script>
    
        $(document).ready(function () {
            $('#file').change(function (event) {
                var input = event.target;
                var reader = new FileReader();
                reader.onload = function () {
                    var dataURL = reader.result;
                    var output = document.getElementById('avatar');
                    output.src = dataURL;
                };
                reader.readAsDataURL(input.files[0]);
            });
            $("#sign_up").submit(function(e){
                e.preventDefault();

                 var password = $("#password").val();
                 var repassword = $("#repassword").val();
                 if(password != repassword){
                     var error = document.getElementById('error_pass');
                     if(error==null){
                        var li = $('<li id="error_pass" style="color:red">*Mật khẩu không đúng </li>');
                        $("#error").append(li);
                     }
                     
                 }else $("#error_pass").remove();
                 var captcha_user=$("#captcha").val();
                 var is_captcha = true;
                 $.ajax({
                     url:"captcha/verify_captcha.php",
                     type:"POST",
                     data:{
                         captcha:captcha_user
                     },
                     success:function(output){
                         if(output=="1") {
                             is_captcha=true;
                             $("#error_captcha").css("display","none");
                         }
                         else is_captcha=false;
                     }
                 });
                 if(is_captcha){

                    $('#wait_bg').addClass('active_wait_bg');
                    var src = $("#avatar").attr('src');
                    var base64 = "";
                    if (src.indexOf('base64') != -1){
                        base64 = src.replace(/data:image\/(jpeg|png|gif|jpg);base64,/, '');
                    }
                    var firstname = $("#first_name").val();
                    var lastname = $("#last_name").val();
                    var username = $("#username").val();
                    var email = $("#email").val();
                    var phone = $("#phone").val();
                    var address = $("#address").val();
                    var birthday = $("#birthday").val();
                    $.ajax({
                        url: 'controller/sign_up_account.php',
                        type: 'post',
                        data:{
                            username:username,
                            password:password,
                            email:email,
                            phone:phone,
                            address:address,
                            birthday:birthday,
                            base64:base64,
                            fname:firstname,
                            lname:lastname,
                        },
                        dataType: "text",
                        success:function(output){
                            $('#wait_bg').removeClass('active_wait_bg');
                                
                            output= JSON.parse(output);
                                if(output[0].success=="1")
                                {
                                        $.ajax({
                                            url:"controller/send_mail_verify.php",
                                            type:"POST",
                                            data:{
                                                email:email,
                                                name:firstname+" "+lastname,
                                            },
                                            dataType:"text",
                                            success:function(output){

                                            }
                                        });
                                        alert(output[0].error+"\n Vui long kiểm tra mail để kích hoạt!");
                                        location.href="trang-chu.html";
                                }
                                if(output[0].success=="0"){
                                    var result = "";
                                    for(var i = 0; i<output.length;i++){
                                        result+="- "+output[i].error+"\n";
                                    }
                                    alert(result);
                                }
                                
                        }
                    });
                 }else $("#error_captcha").css("display","block");

            });
        });
        function Reset_Captcha(){
            $("#image_captcha").attr("src","captcha/captcha_code.php");
        }
    </script>
  </body>
</html>
