<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Trang chủ</title>
    

     <!-- bootstrap cdn -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    
    

    <!-- contact style css -->
    <link href="css/style_home.css" rel="stylesheet">

    <!-- jQuery library -->

    <!-- Latest compiled and minified JavaScript -->


        
    <!-- Development Google Fonts -->

  </head>
    <body style="background: #f6f6f6">
    <?php include ('header.php');?>
    <!-- Body begin -->
    <div id="wrap">
    <div class="container" id="main-content"><!-- Use wrapper for wide or boxed mode -->
        <div class="top_dish">
	        <div class="row">
		        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title-container">
		        	<div class="title">
	        			<h1>Các món nổi bật</h1>
	        			<a class="btn btn-title btn-warning" href="an-gi.html">More<i class="fa fa-arrow-right"></i></a>
	        		</div>
	        		<br><hr>
	        	</div>
				<div id="food_highlights">
					
				<div>
	        </div>
        </div>

        <div class="popular_location">
        	<div class="row">
        		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title-container">
		        	<div class="title">
	        			<h1>Địa điểm phổ biến</h1>
	        			<a class="btn btn-title btn-warning" href="o-dau.html">More<i class="fa fa-arrow-right"></i></a>
	        		</div>
	        		<div id="clear"></div>
	        		<br><hr>
	        	</div>
				<div id="p_location">
					
					</div>
        	</div>

        </div>

        <div class="daily_news">
        	<div class="row">
        		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title-container">
		        	<div class="title">
	        			<a class="btn btn-title btn-warning" href="tin-tuc.html">More<i class="fa fa-arrow-right"></i></a>
	        			<h1>Tin tức</h1>
	        		</div>
	        		<div id="clear"></div>
	        		<br><hr>
	        	</div>
				<div id="news_index">
					
	        	</div>
	        	

        	</div>
        </div>     
    </div>
    </div>
	</div>
<?php include ('footer.php');?>

    <!-- Body end -->
	<script>
		$(document).ready(function () {
			$.ajax({
				url:"controller/json_index.php",
				type:"GET",
				data:{
					type:"food_highlights",
				},
				dataType: "text",
				success:function(output){
					 output = JSON.parse(output);
					 
					 for(var i = 0; i < output.length; i++){
						 var div = $('<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"></div>');
						 var icon = "";
						 if(i % 3 == 0) icon="fa-coffee";
						 if (i % 3 == 1 ) icon="fa-fire";
						 if (i % 3 == 2 ) icon="fa-cutlery";
						 var div_child = $('<div class="dishes"><i class="fa '+icon+'"></i>'
						 				+'<a href="mon-an-'+formatlink(output[i].name) +'-' + output[i].id +'.html"><img src="upload/products/'+output[i].avatar+'" class="img-responsive">'
										+'</a><h3><a href="mon-an-'+formatlink(output[i].name) +'-' + output[i].id +'.html">'+output[i].name+'</a></h3>'
										+'<h4>'+output[i].address+'</h4><p>'+output[i].shortdes+'</p></div>');
						 div.append(div_child);
						 $("#food_highlights").append(div);
					 }
				}
			});
			$.ajax({
				url:"controller/json_index.php",
				type:"GET",
				data:{
					type:"p_location",
				},
				dataType: "text",
				success:function(output){
					 output = JSON.parse(output);
					 
					 for(var i = 0; i < output.length; i++){
						 var div = $('<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"></div>');
						 var icon = "";
						 if(i % 3 == 0) icon="fa-coffee";
						 if (i % 3 == 1 ) icon="fa-fire";
						 if (i % 3 == 2 ) icon="fa-cutlery";
						 var div_child = $('<div class="dishes"><i class="fa '+icon+'"></i>'
						 				+'<a href="mon-an-'+formatlink(output[i].name) +'-' + output[i].id +'.html"><img src="upload/products/'+output[i].avatar+'" class="img-responsive">'
										+'</a><h3><a href="mon-an-'+formatlink(output[i].name) +'-' + output[i].id +'.html">'+output[i].name+'</a></h3>'
										+'<h4>'+output[i].address+'</h4><p>'+output[i].shortdes+'</p></div>');
						 div.append(div_child);
						 $("#p_location").append(div);
					 }
				}
			});
			$.ajax({
				url:"controller/json_news.php",
				type:"POST",
				dataType:"text",
				success:function(output){
					output= JSON.parse(output);
					if(output.length>0){
						for(var i = 0; i<output.length;i++){
							if(i>=5) break;
						var div = $('<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 news"></div>');
						var contents=$('<a href="tin-tuc-'+formatlink(output[i].name) +'-' + output[i].id +'.html"><h3>'+output[i].name+'</h3></a><div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">'
							 +'<a href="tin-tuc-'+formatlink(output[i].name) +'-' + output[i].id +'.html"><img src="upload/news/'+output[i].avatar+'" class="img-responsive"></a></div>'
	        				+'<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><p>'+output[i].shortdes+'</p></div>');
							div.append(contents);
							$("#news_index").append(div);
						}
					}
				}
			})
		});
	</script>
  </body>
</html>
