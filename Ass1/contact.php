﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Liên hệ</title>
    
    
    <!-- bootstrap cdn -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link href="css/style_contact.css" rel="stylesheet">
    <!-- contact style css -->        
    <!-- Development Google Fonts -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Lobster|Open Sans|Cantata One|Merriweather">  
    
  </head>

  <body style="background: #f6f6f6">
  
  
    <?php include('header.php'); ?>
    <div id="wrap">
    <!--<div class="sub-header">

    </div>-->

        <div class="container" id="main-content"><!-- Use wrapper for wide or boxed mode -->
            <div class="row">
                <div class="col-md-5">
                    <div class="contact_info">
                        <div class="title">
                            Thông tin<h3>Liên hệ</h3></div>
                        <div class="ct_content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <h4>Địa chỉ</h4>
                            <ul class="list_ul">
                                <li><i class="fa fa-home"></i> 268 Lý Thường Kiệt</li>
                                <li><i class="fa fa-location-arrow"></i> Phường 14, Quận 10, Hồ Chí Minh</li>
                            </ul>
                            <hr>
                            <h4>Số điện thoại</h4>
                            <ul class="list_ul">
                                <li><i class="fa fa-mobile"></i> 0123 456 789</li>
                                <li><i class="fa fa-phone"></i> (08)38888888</li>
                                <li><i class="fa fa-fax"></i> 1(800) 999-6969</li>
                            </ul>
                            <hr>
                            <h4>Email</h4>
                            <ul class="list_ul">
                                <li><i class="fa fa-envelope"></i> nhdtndvannt@hcmut.edu.vn</li>
                            </ul>
                            <hr>
                            <h4>Let's stay in touch</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                     <div class="contact_form">
                        <div class="title">
                            Mẫu<h3>Liên hệ</h3></div>
                            <ul>
                                
                                <li>Địa chỉ email của bạn sẽ được bảo mật tuyệt đối.</li>
                            <li>Các trường bắt buộc được đánh dấu *</li>
                            </ul>
                            
                            <hr>
                        <form method="post" action="controller/send_mail_contact.php" >
                            <div class="form-group">
                                <input type="text" name="ct_name" placeholder="Họ tên*" class="form-control form-item" required="required"></div>
                            <div class="form-group">
                                <input type="text" name="ct_phone" placeholder="Số diện thoại*" class="form-control form-item" required="required"></div>
                            <div class="form-group">
                                <input type="email" name="ct_email" placeholder="email@gmail.com*" class="form-control form-item" required="required"></div>
                            <div class="form-group">
                                <input type="text" title="ct_title" name = "ct_title" placeholder="Tiêu đề*" class="form-control form-item" required="required"></div>
                            <div class="form-group">
                                <textarea class="form-control form-area" name = "contents" placeholder="Nội dung*" rows="8" required="required"></textarea></div>
                            <button type="submit" class="btn btn-1">Send</button>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>


	<?php include('footer.php'); ?>
  </body>
</html>
