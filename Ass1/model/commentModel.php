<?php
    
    $dbc;

    class Comment_model{

    function __construct() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "ass1";
        global $dbc;
        // Create connection
        $dbc = new mysqli($servername, $username, $password, $dbname);
        $dbc->query("SET NAMES utf8");  
        //mysqli_query($dbc,'set character set "utf8"');
        // Check the Connection
        if (!$dbc) {
            trigger_error('Can not connect to MySQL: ' . mysqli_connect_error());
        }
    }
    public function get_list_comment_news($newsid){
        global $dbc;
        $sql="SELECT * FROM comment WHERE status=1 and News_ID=$newsid ORDER BY CommentTime DESC ";

        return $dbc->query($sql);
    }
    public function get_list_comment_products($productid){
        global $dbc;
        $sql="SELECT * FROM comment WHERE status=1 and Products=$productid ORDER BY CommentTime DESC ";

        return $dbc->query($sql);
    }
    public function insert_comment_news_guest($usr,$email,$text,$newsid){
        global $dbc;
        $sql="INSERT INTO comment (id, Contents, User_ID, Products, News_ID, Name, Email, Status, CommentTime) VALUES (NULL, '$text', NULL, NULL, '$newsid', '$usr', '$email', '1', now())";
        if(($dbc->query($sql))===TRUE)
            return $dbc->insert_id;
        else return false;

    }
    public function insert_comment_news_user($usrid,$name,$email,$text,$newsid){
        global $dbc;
        $sql="INSERT INTO comment (id, Contents, User_ID, Products, News_ID, Name, Email, Status, CommentTime) VALUES (NULL, '$text', '$usrid', NULL, '$newsid', '$name', '$email', '1', now())";
        if(($dbc->query($sql))===TRUE)
            return $dbc->insert_id;
        else return false;

    }
    public function insert_comment_product_guest($usr,$email,$text,$productid){
        global $dbc;
        $sql="INSERT INTO comment (id, Contents, User_ID, Products, News_ID, Name, Email, Status, CommentTime) VALUES (NULL, '$text', NULL, $productid, NULL, '$usr', '$email', '1', now())";
        if(($dbc->query($sql))===TRUE)
            return $dbc->insert_id;
        else return false;

    }
    public function insert_comment_product_user($usrid,$name,$email,$text,$productid){
        global $dbc;
        $sql="INSERT INTO comment (id, Contents, User_ID, Products, News_ID, Name, Email, Status, CommentTime) VALUES (NULL, '$text', '$usrid', $productid, NULL, '$name', '$email', '1', now())";
        if(($dbc->query($sql))===TRUE)
            return $dbc->insert_id;
        else return false;

    }
    
    public function get_comment_by_id($comment_id){
         global $dbc;
        $sql="SELECT * FROM comment WHERE status=1 and id=$comment_id";

        return $dbc->query($sql);
    }
    function close_connect() {
        global $dbc;
        $dbc->close();

    }
}
?>