<?php
    
    $dbc;

    class Post_model{

    function __construct() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "ass1";
        global $dbc;
        // Create connection
        $dbc = new mysqli($servername, $username, $password, $dbname);
       $dbc->query("SET NAMES utf8");  
        // Check the Connection
        if (!$dbc) {
            trigger_error('Can not connect to MySQL: ' . mysqli_connect_error());
        }
    }
    public function get_news_by_id ($id){
        global $dbc;
        $sql="SELECT * FROM news WHERE id=$id ";
        return $dbc->query($sql);
    }
    public function get_list_news(){
        global $dbc;
        $sql="SELECT * FROM news WHERE status=1 ";
        return $dbc->query($sql);
    }
    public function get_list_news_current(){
        global $dbc;
        $sql = "SELECT * FROM news WHERE STATUS=1 ORDER BY news.Postdate DESC LIMIT 3";
        return $dbc->query($sql);
    }
    public function set_view_news_byid($id,$views){
        global $dbc;
        $sql = "UPDATE news SET MostView= $views WHERE news.id=$id";
        return $dbc->query($sql);
    }
    public function get_list_Most_View(){
        global $dbc;
         $sql = "SELECT * FROM news WHERE STATUS=1 ORDER BY news.MostView DESC LIMIT 4";
        return $dbc->query($sql);
    }
    function close_connect() {
        global $dbc;
        $dbc->close();

     }
}
?>