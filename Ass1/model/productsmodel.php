<?php
    
    $dbc;

    class Products_model{

    function __construct() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "ass1";
        global $dbc;
        // Create connection
        $dbc = new mysqli($servername, $username, $password, $dbname);
        $dbc->query("SET NAMES utf8");  
        // Check the Connection
        if (!$dbc) {
            trigger_error('Can not connect to MySQL: ' . mysqli_connect_error());
        }
    }
    public function get_product_by_id ($id){
        global $dbc;
        $sql="SELECT * FROM products WHERE id=$id and Status = 1 ";
        return $dbc->query($sql);
    }
    public function get_list_video_by_productsid($productid){
        global $dbc;
        $sql="SELECT * FROM videos WHERE Product_ID = $productid and Status = 1";
        return $dbc->query($sql);
    }
    public function get_address_by_id ($id){
        global $dbc;
        $sql="SELECT * FROM products WHERE id=$id and Status = 1 ";
        return $dbc->query($sql);
    }
    public function get_list_products_by_category_id($cateId){
        global $dbc;
        $sql = "SELECT t.id as productID,t.Name, adress.District,adress.City,t.ShortDes,t.avatarID FROM adress JOIN (SELECT* FROM address_product JOIN (SELECT products.id as id , products.Name as Name,products.ShortDes as ShortDes,products.avatar as avatarID FROM products JOIN product_cate ON products.id=product_cate.Product_ID WHERE product_cate.Category_ID= $cateId and products.Status=1 ORDER BY products.MostView DESC LIMIT 6) p on p.id=address_product.Product_ID) t on t.Address_ID=adress.id";

        return $dbc->query($sql);
    }
    public function get_address_by_productsid($productid){
        global $dbc;
        $sql = "SELECT t.id as Product_ID,adress.AdressFull FROM adress JOIN (SELECT * FROM products JOIN address_product on products.id=address_product.Product_ID WHERE products.id=$productid AND products.Status=1) t on adress.id= t.Address_ID";
        return $dbc->query($sql);
    }
    public function get_restaurant_by_productsid($productid){
        global $dbc;
        $sql = "SELECT restaurant.Name,restaurant.id as Res_ID,t.productID FROM restaurant JOIN (SELECT product_restaurant.Res_ID,products.id as productID FROM products JOIN product_restaurant on products.id=product_restaurant.Product_ID WHERE products.id=$productid and products.Status=1 ) t on t.Res_ID=restaurant.id";

        return $dbc->query($sql);
    }
    public function get_image_by_productsid($productid){
        global $dbc;
        $sql = "SELECT t.Image_ID, t.Product_ID,imagetbx.Name FROM imagetbx JOIN (SELECT product_image.Image_ID,product_image.Product_ID FROM products JOIN product_image on products.id=product_image.Product_ID WHERE products.id=$productid and products.Status=1) t on t.Image_ID=imagetbx.id";


        return $dbc->query($sql);
    }
    public function save_products_by_user($userid,$productid){
        global $dbc;
        $sql="INSERT INTO save_products (Product_ID, User_ID) VALUES ($productid,$userid)";
        return $dbc->query($sql);
    }
    public function detele_save_products_by_user($userid,$productid){
    global $dbc;
    $sql = "DELETE FROM save_products WHERE save_products.Product_ID=$productid and save_products.User_ID=$userid";
    return $dbc->query($sql);
    }
    public function get_save_products_by_userid_productid($userid,$productid){
        global $dbc;
        $sql = "SELECT * FROM save_products WHERE save_products.Product_ID=$productid AND save_products.User_ID=$userid";
        return $dbc->query($sql);
    }
    public function  set_product_most_views($productid,$views){
        global $dbc;
        $sql = "UPDATE products SET MostView = $views WHERE id=$productid";
        return $dbc->query($sql);
    }
    public function  get_list_products_new(){
        global $dbc;
        $sql = "SELECT * FROM products WHERE products.Status = 1 and products.STATUS = 1 ORDER BY products.PostDate DESC LIMIT 3";
        return $dbc->query($sql);
    }
    public function get_list_products_by_category_menu_id($cateId){
        global $dbc;
        $sql = "SELECT t.id as productID,t.Name,adress.AdressFull, adress.District,adress.City,t.ShortDes,t.avatarID FROM adress JOIN (SELECT* FROM address_product JOIN (SELECT products.id as id , products.Name as Name,products.ShortDes as ShortDes,products.avatar as avatarID FROM products JOIN product_cate ON products.id=product_cate.Product_ID WHERE product_cate.Category_ID= $cateId and products.Status=1 ORDER BY products.MostView DESC) p on p.id=address_product.Product_ID) t on t.Address_ID=adress.id";

        return $dbc->query($sql);
    }
    public function get_list_products(){
        global $dbc;
        $sql="SELECT * FROM products WHERE Status = 1 ";
        return $dbc->query($sql);
    }
    public function close_connect() {
         global $dbc;
          $dbc->close();

     }

}
?>