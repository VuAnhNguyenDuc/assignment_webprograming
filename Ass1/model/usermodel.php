<?php
    
    $dbc;

    class User_model{

    function __construct() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "ass1";
        global $dbc;
        // Create connection
        $dbc = new mysqli($servername, $username, $password, $dbname);
        $dbc->query("SET NAMES utf8");  
        // Check the Connection
        if (!$dbc) {
            trigger_error('Can not connect to MySQL: ' . mysqli_connect_error());
        }
    }
    public function get_user_by_id($id){
        global $dbc;
        $sql="SELECT * FROM user WHERE id = $id ";
        return $dbc->query($sql);
    }
    public function active_account($id){
        global $dbc;
        $sql = "UPDATE user SET Status=1 WHERE user.id=$id";
        return $dbc->query($sql);
    }
    public function get_user_by_username($username){
        global $dbc;
        $sql="SELECT * FROM user WHERE UserName = '$username' ";
        
       return $dbc->query($sql);
    }
    public function check_login($username,$password){
        global $dbc;
        $sql="SELECT * FROM user WHERE UserName = '$username' and Password = '$password' and Status = 1 ";
        
       return $dbc->query($sql);
    }
    public function update_password($username,$password){
        global $dbc;
        $sql = "UPDATE user SET Password='$password' WHERE user.UserName='$username'";
        return $dbc->query($sql);
    }
    public function update_info($userid,$fname,$lname,$birthday,$address,$phone,$email){
        global $dbc;
        $sql = "UPDATE user SET FirstName ='$fname',LastName='$lname',Email='$email',Phone='$phone',Adress='$address',BirthDay='$birthday' WHERE user.id=$userid";
        return $dbc->query($sql);
    }
    public function get_user_by_email($email){
        global $dbc;
        $sql="SELECT * FROM user WHERE Email = '$email' ";
        return $dbc->query($sql);
    }
    public function get_products_save_by_user_id($userid){
    global $dbc;
    $sql="SELECT t.Product_ID,t.Name,t.ShortDes,imagetbx.Name AS Avatar FROM imagetbx JOIN (SELECT s.Product_ID,products.Name,products.ShortDes,products.avatar FROM products JOIN (SELECT * FROM save_products WHERE save_products.User_ID=$userid) s on s.Product_ID = products.id WHERE products.Status=1) t on imagetbx.id= t.avatar";
    return $dbc->query($sql);
    }
    public function add_role($userid,$roleid){
        global $dbc;
        $sql="INSERT INTO user_admin(User_ID,Admin_ID) value ('$userid','$roleid') ";
        return $dbc->query($sql);
    }
    public function add_user($firstname,$lastname,$username,$email,$phone,$password,$avatarID,$address,$birthday,$status){
        global $dbc;
        $sql="INSERT INTO user(id, FirstName , LastName, UserName, Email, Phone, Password, Avatar_ID, Adress, RegisterDate, BirthDay, Status) VALUES (NULL,'$firstname','$lastname','$username','$email','$phone','$password','$avatarID','$address',now(),'$birthday','$status')";
        if(($dbc->query($sql))===TRUE)
            return $dbc->insert_id;
        else return false;
    }
     function close_connect() {
         global $dbc;
        $dbc->close();

     }
}
?>