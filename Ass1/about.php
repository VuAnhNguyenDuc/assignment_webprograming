﻿<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Giới thiệu</title>



    <!-- main css -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link href="css/style_about.css" rel="stylesheet">

    <!-- Development Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Cantata+One%7COpen+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- Development Google Fonts -->
</head>

<body >
	<?php include('header.php'); ?>
    <div id="about">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1 style="text-align:center;">Giới thiệu</h1>
                    <hr style="border: 0; border-top: 3px solid #eee;" />
                    <div class="row">
                        <p>Ngonday.Com là trang đánh giá địa điểm ăn uống, nhà hàng, quán cafe ở Hà nội.
                         Chúng tôi hướng tới đánh giá chuyên nghiệp, khách quan, chính xác và minh bạch.
                         Ngonday tuyệt đối không nhận viết bài quảng cáo,
                         PR trái với sự thật nhằm câu kéo khách cho bất kỳ nhà hàng hay quán cafe nào.
                        </p>
                    </div>
                    <div class="row">
                        <p>Bằng sự nhiệt huyết, mong muốn xây dựng chuyên trang đánh giá chất lượng và uy tín, 
                        Ngonday luôn giữ vững phương trâm "Chính xác, chân thực và khách quan".
                         Ngonday thường xuyên cập nhật các nhà hàng, quán cafe hay quán ăn vặt quanh Hà nội và trong tương lai sẽ là khắp các tỉnh thành trong cả nước.</p>
                    </div>
                    <div class="row">
                        <p>Ngonday.Com cung cấp ảnh chụp 360 hoàn toàn miễn phí, bạn có thể xem ảnh 360 toàn cảnh tại bài viết giúp bạn dễ hình dung
                         và bạn sẽ có cảm giác như đang đứng ngay tại vị trí cửa hàng, quán cafe mà Ngonday đang đánh giá. Thậm chí, ngonday còn hỗ trợ nhà hàng,
                         quán cafe submit ảnh địa điểm 360 miễn phí lên Google StreetView, từ đó người dùng dễ dàng xem được hình ảnh 360 qua Google StreetView. 
                        Hãy xem thử bài viết 360 về quán cafe của Ngonday qua bài Đánh giá Cabine Cafe 50 Hàng Bài hoặc xem tại Google StreetView.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h4><b>Nhân viên</b></h4>
                    <hr style="border: 0; border-top: 2px solid #eee;" />
                    <div class="row" style="margin:10px 0;">
                        <div class="col-sm-6 col-xs-12">
                            <a href="#">
                                <img src="images/foody-mobile.jpg" class="img-thumbnail img-responsive" />
                            </a>

                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <a href="#"><b>Nguyễn Ngọc Thật</b></a><br />
                            <span style="color: #888">CEO</span>
                        </div>
                    </div>
                    <div class="row" style="margin:10px 0;">
                        <div class="col-sm-6 col-xs-12">
                            <a href="#">
                                <img src="images/foody-mobile.jpg" class="img-thumbnail img-responsive" />
                            </a>

                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <a href="#"><b>Nguyễn Đức Vũ Anh</b></a><br />
                            <span style="color: #888">CFO</span>
                        </div>
                    </div>
                    <div class="row" style="margin:10px 0;">
                        <div class="col-sm-6 col-xs-12">
                            <a href="#">
                                <img src="images/foody-mobile.jpg" class="img-thumbnail img-responsive" />
                            </a>

                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <a href="#"><b>Tâm</b></a><br />
                            <span style="color: #888">CPO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
</body>
</html>
