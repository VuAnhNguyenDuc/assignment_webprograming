<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    
    <title>News Detail</title>
    <link href="/Ass1/css/style_news.css" rel="stylesheet">

</head>

<body>
   <?php include('header.php')?>
    <section class="news-background">
        <div class="container">
            <div class="col-sm-9 no-padding">
                <div class="row color-news" style="margin-right: -10px;">
                    <h3 style="text-align:center;" id="news_title"></h3>
                    <hr style="margin-left:30px;margin-right:30px;" />
                    <div id="details_news" style="padding:20px">
					</div>
                </div>
                <div class="row color-news" style="margin-top:5px; margin-right: -10px;">

                    <div class="col-sm-12">
                        <h4 id="number_comments" style="margin-top:30px;"><span id="number_comment">0</span> Bình luận</h4>
                        <hr />
                    </div>
                     <?php 
                            
                        if (isset($_SESSION['user']) && $_SESSION['user']){
                    ?>
                    <form id="user_comment" class="form-group" >
                        <div class="col-sm-12" style="margin-top:20px;">
                            <textarea id = "text_comment" rows="2" style="width: 100%; border-radius: 5px; border-color: #ccc; " name="comment" placeholder="Đánh giá của bạn..."></textarea>
                        </div>
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">GỬI</button>
                        </div>
                        <div class="clr"></div>
                    </form>
                        <?php } else {?>
                    <form id="guest_submit" class="form-group" >
                        <div  class="col-sm-12">
                            <input type="text" name="name" required class="form-control" placeholder="Họ và tên..." id="usr">
                            <input type="email" name="email" required class="form-control" placeholder="Nhập email..." id="email">
                            <div class="clr"></div>
                        </div>
                        <div class="col-sm-12" style="margin-top:20px;">
                            <textarea id = "text_comment" rows="2" style="width: 100%; border-radius: 5px; border-color: #ccc; " name="comment" placeholder="Đánh giá của bạn..."></textarea>
                        </div>
                        <div class="col-sm-12">
                            <button id="submit_comment" type="submit" class="btn btn-primary">GỬI</button>
                        </div>
                        <div class="clr"></div>
                    </form>
                        <?php }?>
                    <div class="col-sm-12" style="padding-bottom:20px;">
                        <hr />
                    </div>
                    <div class="col-sm-12">
                        <ul class="comment-list">
                            
                        </ul>
                    </div>
                    <div  class="col-sm-12" style="padding-bottom:30px; text-align:center">
                        <a id="more_comment" onclick="ShowMoreCM(5)"  style="color: blue !important; cursor: pointer;">Xem thêm</a>
                        <hr />
                    </div>
                </div>
            </div>

            <div class="col-sm-3 no-padding">
                <div class="row color-news">
                    <input class="form-control" onkeyup=Search(event) id="search-news-input" placeholder="Search" type="text" />
                    <input id="search-news"  type="button" class="btn btn-info" />
                    <ul id="result_search" class="wrap-suggestion">
                    </ul>
                    <div class="clr"></div>
                </div>
                <div class="row color-news" style="margin-top:5px;">
                    <h3 class="recent-post">Bài viết hot nhất</h3>
                    <ul id="list_news_hot" class="border-top-news" style="margin-top:10px;margin-bottom:10px;margin-left:5px;padding:0;">
                        
                    </ul>

                </div>
            </div>

        </div>
    </section>
    <?php include('footer.php')?>
	<script>
        var comment = new Array();
        var numshow = 0;
        var islogin = $("#islogin").val();
        var newsid = <?php echo $_REQUEST["id"] ?>;
		  $(document).ready(function () {
            $.ajax({
                url: 'controller/json_news_comment.php',
                type: 'post',
                data:{
                    newsid:newsid,
                },
                dataType: "text",
                success: function(output) {
                    output = JSON.parse(output);
                    if(output.length > 5) $("#more_comment").css("display","block");
                    else $("#more_comment").css("display","none");
                    if(output.length>=1){
                        comment = output;
                        $("#number_comment").html(output.length);
                        if(output.length>5) numshow = 5;
                        else numshow = output.length;
                        for(var i = 0;i<output.length;i++)
					    {
                             if(i >= 5) break;
                             var postdate = Date.parse(output[i].commentdate);
                             var newdate = Date();
                             var current = Date.parse(newdate);
                             var timeline = current - postdate;
                             var year =  timeline/(1000*60*60*24*365);
                             var time ="";
                             if(year>=1)
                                time = year +" năm trước";
                             else if((year*365/30)>=1) 
                                 time = parseInt(year*365/30 )+ " tháng trước";
                             else if ((year*365)>=1)
                                time = parseInt(year*365) + " ngày trước";
                             else if((year*365*24)>=1)
                                time = parseInt(year*365*24) + " giờ trước";
                             else if((year*365*24*60)>=1)
                                time = parseInt(year*365*24*60) + " phút trước";
                             else if ((year*365*24*60*60)>=1)
                                time = parseInt(year*365*24*60*60) + " giây trước";
                             var li = $('<li></li>');
                             var name = $('<div class="comment-meta"><span class="user-name">'+output[i].author+'</span><span  class="comment-text"> '+output[i].content+'</span><br /><span class="comment-info">'+time+'</span></div>');
                             //var context = $('<div class="comment-text"><p>'+output[i].content+'</p></div>');
                             li.append(name);
                             $('.comment-list').append(li);
                        }
                        
                    }else{
                         $("#number_comment").html("0");
                            var li = $('<li id="no_comment" style="text-align:center">Chưa có bình luận !</li>');
                            $('.comment-list').append(li);
                    }

               }
            });
            $.ajax({
                url:"controller/json_news_detail.php",
                type:"POST",
                data:{
                    newsid:newsid
                },
                dataType:"text",
                success:function(output){
                    output = JSON.parse(output);
                    if(output.length>0){
                            document.title=output[0].name; 
                            $("#news_title").html(output[0].name);
                            $("#details_news").append(output[0].desc)
                    }

                }
            });
            $.ajax({
                url:"controller/json_news_hot.php",
                type:"POST",
                dataType:"text",
                success:function(output){
                    if(output!="0"){
                        output = JSON.parse(output);
                        for(var i = 0; i<output.length;i++){
                            if(i==0){
                                var li = $('<li style="border:0;"></li>');
                                var image = $('<div class="col-sm-5 col-md-5 col-xs-3"><img class="img-responsive img-newspost" style="width:70px;" src="upload/news/'+output[i].avatar+'" /> </div>');
                                var name = $('<div class="col-sm-7 col-md-7 col-xs-9"><a class="title-newspost" href="tin-tuc-'+formatlink(output[i].name) +'-' + output[i].id +'.html">'+output[i].name+'</a> </div> <div class="clr"></div>');
                                li.append(image).append(name);
                                $('#list_news_hot').append(li);
                            }else{
                                var li = $('<li></li>');
                                var image = $('<div class="col-sm-5 col-md-5 col-xs-3"><img class="img-responsive img-newspost" style="width:70px;" src="upload/news/'+output[i].avatar+'" /> </div>');
                                var name = $('<div class="col-sm-7 col-md-7 col-xs-9"><a class="title-newspost" href="tin-tuc-'+formatlink(output[i].name) +'-' + output[i].id +'.html">'+output[i].name+'</a> </div> <div class="clr"></div>');
                                li.append(image).append(name);
                                $('#list_news_hot').append(li);
                            }
                        }
                    }
                }
            });
             
            $("#text_comment").keyup(function (event) {
                if (event.keyCode == 13) {
                    if (islogin==0)
                    $("#guest_submit").submit();
                    else $("#user_submit").submit();
                }
            });
            $("#guest_submit").submit(function(e){
                e.preventDefault();
                var usr= $("#usr").val();
                var email = $("#email").val();
                var text = $("#text_comment").val();
                if(text!=""){
                $.ajax({
                        url: 'controller/add_comment_news_guest.php',
                        type: 'post',
                        data:{
                        user:usr,
                        email:email,
                        comment:text,
                        newsid:newsid,
                        },
                        dataType: "text",
                        success: function(output) {
                            output = JSON.parse(output);
                            if(output[0].success=="1"){
                                var text = $(".comment-list").html();
                                var postdate = Date.parse(output[0].commentdate);
                                var newdate = Date();
                                var current = Date.parse(newdate);
                                var timeline = current - postdate;
                                var year =  timeline/(1000*60*60*24*365);
                                var time ="";
                                if(year>=1)
                                    time = year +" năm trước";
                                else if((year*365/30)>=1) 
                                    time = parseInt(year*365/30 )+ " tháng trước";
                                else if ((year*365)>=1)
                                    time = parseInt(year*365) + " ngày trước";
                                else if((year*365*24)>=1)
                                    time = parseInt(year*365*24) + " giờ trước";
                                else if((year*365*24*60)>=1)
                                    time = parseInt(year*365*24*60) + " phút trước";
                                else if ((year*365*24*60*60)>=0)
                                    time = parseInt(year*365*24*60*60) + " giây trước";
                                var li = $('<li></li>');
                                var name = $('<div class="comment-meta"><span class="user-name">'+output[0].author+'</span><span  class="comment-text"> '+output[0].content+'</span><br /><span class="comment-info">'+time+'</span></div>');
                                //var context = $('<div class="comment-text"><p>'+output[0].content+'</p></div>');
                                li.append(name);
                                $(".comment-list").prepend(li);
                                $("#no_comment").css("display","none");
                                $("#text_comment").val("");
                                $("#number_comment").html(parseInt($("#number_comment").html())+1);
                            }else alert("Lỗi!");
                        }
                    });
                }
            });
             $("#user_comment").submit(function(e){
                e.preventDefault();
                var text = $("#text_comment").val();
                var userid = $("#userid").val();
                var name = $("#usernamefull").val();
                if(text!=""){
                $.ajax({
                        url: 'controller/add_comment_news_user.php',
                        type: 'post',
                        data:{
                        comment:text,
                        newsid:newsid,
                        userid:userid,
                        name:name,
                        },
                        dataType: "text",
                        success: function(output) {
                            output = JSON.parse(output);
                            if(output[0].success=="1"){
                                var text = $(".comment-list").html();
                                var postdate = Date.parse(output[0].commentdate);
                                var newdate = Date();
                                var current = Date.parse(newdate);
                                var timeline = current - postdate;
                                var year =  timeline/(1000*60*60*24*365);
                                var time ="";
                                if(year>=1)
                                    time = year +" năm trước";
                                else if((year*365/30)>=1) 
                                    time = parseInt(year*365/30 )+ " tháng trước";
                                else if ((year*365)>=1)
                                    time = parseInt(year*365) + " ngày trước";
                                else if((year*365*24)>=1)
                                    time = parseInt(year*365*24) + " giờ trước";
                                else if((year*365*24*60)>=1)
                                    time = parseInt(year*365*24*60) + " phút trước";
                                else if ((year*365*24*60*60)>=0)
                                    time = parseInt(year*365*24*60*60) + " giây trước";
                                var li = $('<li></li>');
                                var name = $('<div class="comment-meta"><span class="user-name">'+output[0].author+'</span><span  class="comment-text"> '+output[0].content+'</span><br /><span class="comment-info">'+time+'</span></div>');
                                //var context = $('<div class="comment-text"><p>'+output[0].content+'</p></div>');
                                li.append(name);
                                $(".comment-list").prepend(li);
                                $("#no_comment").css("display","none");
                                $("#text_comment").val("");
                                $("#number_comment").html(parseInt($("#number_comment").html())+1);
                            }else alert("Lỗi!");
                        }
                    });
                }
            });
             $("#search-news").click(function(){
                var keyword=$("#search-news-input").val();

                if(keyword.length>=2){
                    keyword = formatstring(keyword);
                    location.href="tin-tuc-ket-qua-tim-kiem.html?tu-khoa="+keyword;
                }
            });
            $('#search-news-input').keypress(function (e) {
                var key = e.which;
                if (key == 13)  // the enter key code
                {
                    $("#search-news").click();
                }
            });
        });
        function Search(event) {
             var keyword=$("#search-news-input").val();
             keyword = formatstring(keyword);
             $("#result_search").html("");
             if (keyword!=""&&keyword.length>=2){
                 $.ajax({
                    url: 'controller/json_news_search.php',
                    type: 'get',
                    dataType: "text",
                    data: {
                        keyword:keyword,
                    },
                    success: function(output) {
                        if(output.length>0){
                            output = JSON.parse(output);
                            for(var i = 0;i<output.length;i++)
					        {
                                if(i>5) break;
                                var tr = $('<li class=""></li>')
                                var tr1 = $('<a href="'+formatlink(output[i].name) +'-' + output[i].id +'.html"></a>');
                                var tr2 = $(' <img src="upload/news/'+output[i].avatar + '"style="hight:50px;width:50px"/>')
                                var tr3 = $('<h3>' + output[i].name + '</h3>');
                                tr1.append(tr2).append(tr3);
                                tr.append(tr1);
                                $("#result_search").append(tr);
                            }
                            $("#result_search").css("display", "block");
                        }else  $("#result_search").css("display", "none");
                    } 
                 });
             }else  $("#result_search").css("display", "none");
        }
        function ShowMoreCM(number){
             var i =0;
             for(i = numshow;i<comment.length;i++)
			 {
                    if(i >= (numshow+number)) break;
                    var postdate = Date.parse(comment[i].commentdate);
                    var newdate = Date();
                    var current = Date.parse(newdate);
                    var timeline = current - postdate;
                    var year =  timeline/(1000*60*60*24*365);
                    var time ="";
                    if(year >= 1)
                    time = year +" năm trước";
                    else if((year*365/30)>=1) 
                        time = parseInt(year*365/30 )+ " tháng trước";
                    else if ((year*365)>=1)
                    time = parseInt(year*365) + " ngày trước";
                    else if((year*365*24)>=1)
                    time = parseInt(year*365*24) + " giờ trước";
                    else if((year*365*24*60)>=1)
                    time = parseInt(year*365*24*60) + " phút trước";
                    else if ((year*365*24*60*60)>=1)
                    time = parseInt(year*365*24*60*60) + " giây trước";
                    var li = $('<li></li>');
                    var name = $('<div class="comment-meta"><span class="user-name">'+comment[i].author+'</span><span  class="comment-text"> '+comment[i].content+'</span><br /><span class="comment-info">'+time+'</span></div>');
                    //var context = $('<div class="comment-text"><p>'+output[i].content+'</p></div>');
                    li.append(name);
                    $('.comment-list').append(li);
            }
           
            numshow +=number;
            if (i>=comment.length) $("#more_comment").css("display","none");
        }
	</script>
</body>
</html>
