<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Tin Tức</title>
    <link href="css/style_news.css" rel="stylesheet">
</head>

<body >
<?php include('header.php')?>
    <section class="news-background">
        <div class="container">
            <div class="col-sm-9 no-padding">

                <div class="row color-news" style="margin-right:-10px;">
                    <h1 style="text-align:center">TIN TỨC</h1>
                    <hr />
					<div id="contents">
						<div class=" lazy-wrapper">
					
					    </div>
					<div class="clr"></div>
					</div>
                    
                </div>
            </div>
            <div class="col-sm-3 no-padding">
                <div class="row color-news">
                    <input class="form-control" onkeyup=Search(event) id="search-news-input" placeholder="Search" type="text" />
                    <input id="search-news"  type="button" class="btn btn-info" />
                    <ul id="result_search" class="wrap-suggestion">
                    </ul>
                    <div class="clr"></div>
                </div>
                <div class="row color-news" style="margin-top:5px;">
                    <h3 class="recent-post">Bài viết hot nhất</h3>
                    <ul id="list_news_hot_main" class="border-top-news" style="margin-top:10px;margin-bottom:10px;margin-left:5px;padding:0;">
                        
                    </ul>
                    
                </div>
            </div>

        </div>
    </section>
    
  <?php include('footer.php')?>
  <script src="js/imagesloaded.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.isotope.sloppy-masonry.min.js"></script>
	<script src="js/lazyloader.js"></script>
	<script>
	 $(function(){
        $('.lazy-wrapper').lazyLoader({
            ajaxLoader: 'controller/json_news_lazyload.php',
			//jsonFile: 'data.json',
            mode: 'scroll',
            limit: 1,
            records:1000,
            offset: 1,
            isIsotope: true,
            isotopeResize: 3
        });
    });
</script>
  <script>
        $(document).ready(function () {
             $.ajax({
                url:"controller/json_news_hot.php",
                type:"POST",
                dataType:"text",
                success:function(output){
                    if(output!="0"){
                        output = JSON.parse(output);
                        for(var i = 0; i<output.length;i++){
                            if(i==0){
                                var li = $('<li style="border:0;"></li>');
                                var image = $('<div class="col-sm-5 col-md-5 col-xs-3"><img class="img-responsive img-newspost" style="width:70px;" src="upload/news/'+output[i].avatar+'" /> </div>');
                                var name = $('<div class="col-sm-7 col-md-7 col-xs-9"><a class="title-newspost" href="tin-tuc-'+formatlink(output[i].name) +'-' + output[i].id +'.html">'+output[i].name+'</a> </div> <div class="clr"></div>');
                                li.append(image).append(name);
                                $('#list_news_hot_main').append(li);
                            }else{
                                var li = $('<li></li>');
                                var image = $('<div class="col-sm-5 col-md-5 col-xs-3"><img class="img-responsive img-newspost" style="width:70px;" src="upload/news/'+output[i].avatar+'" /> </div>');
                                var name = $('<div class="col-sm-7 col-md-7 col-xs-9"><a class="title-newspost" href="tin-tuc-'+formatlink(output[i].name) +'-' + output[i].id +'.html">'+output[i].name+'</a> </div> <div class="clr"></div>');
                                li.append(image).append(name);
                                $('#list_news_hot_main').append(li);
                            }
                        }
                    }
                }
            });  
            $("#search-news").click(function(){
                var keyword=$("#search-news-input").val();
                if(keyword.length>=2){
                    keyword = formatstring(keyword);
                    location.href="tin-tuc-ket-qua-tim-kiem.html?tu-khoa="+keyword;
                }
            });
            $('#search-news-input').keypress(function (e) {
                var key = e.which;
                if (key == 13)  // the enter key code
                {
                    $("#search-news").click();
                }
            });
        });
        function Search(event) {
             var keyword=$("#search-news-input").val();
             keyword = formatstring(keyword);
             $("#result_search").html("");
             if (keyword!=""&&keyword.length>=2){
                 $.ajax({
                    url: 'controller/json_news_search.php',
                    type: 'get',
                    dataType: "text",
                    data: {
                        keyword:keyword,
                    },
                    success: function(output) {
                        if(output.length>0){
                            output = JSON.parse(output);
                            for(var i = 0;i<output.length;i++)
					        {
                                if(i>5) break;
                                var tr = $('<li class=""></li>')
                                var tr1 = $('<a href="tin-tuc-'+formatlink(output[i].name) +'-' + output[i].id +'.html"></a>');
                                var tr2 = $(' <img src="upload/news/'+output[i].avatar + '"style="hight:50px;width:50px"/>')
                                var tr3 = $('<h3>' + output[i].name + '</h3>');
                                tr1.append(tr2).append(tr3);
                                tr.append(tr1);
                                $("#result_search").append(tr);
                            }
                            $("#result_search").css("display", "block");
                        }else  $("#result_search").css("display", "none");
                    } 
                 });
             }else  $("#result_search").css("display", "none");
        }
  </script>
</body>
</html>
