
    <!-- header style css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="css/style_header.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Lobster|Open Sans|Cantata One|Merriweather">
    <script src="js/custom.js"></script>
    <script src="js/jquery.scrollUp.js"></script>
    <link href="css/themes/image.css" rel="stylesheet" />
    <script>
        $(function () {
            $.scrollUp({
                scrollText: '', // Text for element
            });
        });
    </script>
    <!-- Development Google Fonts -->
    <?php include ('config.php');?>
    <?php session_start(); ?>
<!-- Header Begin -->
        <header>
            <nav class="navbar navbar-inverse " style="width: 100%; z-index: 999; ">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?=base_url()?>trang-chu.html"><img src="img/vienna-logo.png" class="img-responsive pm-header-logo" alt="Vienna Restaurant"></a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li class=""><a href="<?=base_url()?>trang-chu.html">TRANG CHỦ</a></li>
                            <li class="dropdown">
                                <a class="dropbtn">MENUS <span class="caret"></span></a>
                                <ul class="dropdown-content" style="background-color: rgba(0,0,0,.85);padding-left:20px;padding-right:20px;z-index:999">
                                    <li class="icon-dropdown"><a href="an-gi.html">ĂN GÌ</a></li>
                                    <li class="icon-dropdown"><a href="o-dau.html">Ở ĐÂU</a></li>
                                </ul>
                            </li>
                            <li><a href="tin-tuc.html">TIN TỨC</a></li>
                            <li><a href="gioi-thieu.html">GIỚI THIỆU</a></li>
                            <li><a href="lien-he.html">LIÊN HỆ</a></li>
                            
                             <?php 
                            
                                if (isset($_SESSION['user']) && $_SESSION['user']){
                                    
                            ?>
                            <li class="dropdown">
                            	<a href="thong-tin-tai-khoan.html" class="dropbtn"><?=$_SESSION['user']['UserName']?><span class="caret"></span></a>
                            	<ul class="dropdown-content" style="background-color: rgba(0,0,0,.85);padding-left:20px;padding-right:20px;z-index:999">
	                            	<li class="icon-dropdown"><a class="log_out" style="cursor:pointer">ĐĂNG XUẤT</a></li>
                            	</ul>
                            </li>
                            <input id="islogin" style="display:none"value="1" />
                            <input id="userid" style="display:none" value="<?=$_SESSION['user']['id']?>"/>
                            <input id="usernamefull" style="display:none" value="<?=$_SESSION['user']['FirstName']." ".$_SESSION['user']['LastName']?>"/>
                                <?php }else { ?>
                            <li id="log_in">
                                <a style="cursor:pointer" data-toggle="modal" data-target="#viModal">ĐĂNG NHẬP</a>
                            </li>
                            <input id="islogin" style="display:none"value="0" />
                                <?php }?>
                            
                        </ul>

                    </div>
                </div>
            </nav>
        </header>
    <!-- Header end -->
     <div class="clr"></div>
     <div class="modal fade" id="viModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content login-wrap">
                    <div class="login-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <br>
                        <div class="login-title">
                            <h2>Đăng nhập</h2>
                        </div>
                    </div>
                    <div class="login-body">
                        <div class="login-container" style="text-align:center">
                            <span id="fail_login" style="color:red;display:none">Tài khoản hoặc mật khẩu không đúng</span>
                            <form method="post"  id="loginForm">
                                <div class="form-group">
                                    <input type="text" value="" id="username_login" name="uname" class="form-control form-item" placeholder="Tên tài khoản"required="required">
                                </div>
                                <div class="form-group">
                                    <input type="password" id="password_login" name="pass" class="form-control form-item" placeholder="Mật khẩu"required="required">
                                </div>
                                <div class="form-group">
                                    <button type="submit" required="required" class="btn_login">Đăng nhập</button>
                                </div>
                                <h6><a href="dang-ky-tai-khoan.html">Quên mật khẩu?</a></h6><br/>
                                <h6><a href="dang-ky-tai-khoan.html">Tạo tài khoản</a></h6>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $("#loginForm").submit(function(e){
                     e.preventDefault();
                     var username = $("#username_login").val();
                     var password = $("#password_login").val();
                     $.ajax({
                         url:"controller/login_account.php",
                         type:"POST",
                         data:{
                             username:username,
                             password:password,
                         },
                         dataType:"text",
                         success:function(output){
                             if(output=="1"){
                                 //alert("Đăng nhập thành công!");
                                  $("#fail_login").css("display","none");
                                 location.reload();
                             }else{
                                 $("#fail_login").css("display","block");
                             }
                         }

                     });
                });
                $(".log_out").click(function(){
                    $.ajax({
                         url:"controller/logout_account.php",
                         type:"POST",
                         dataType:"text",
                         success:function(output){
                             location.reload();
                         }

                     });
                });
            });
        </script>
