
     <link href="css/style_footer.css" rel="stylesheet">
    <!-- Footer begin -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-12 footer-content">
                        <img src="images/vienna-logo.png" class="img-responsive" alt="Vienna Restaurant"> 
                        <p>
                        Được xây dựng từ giữa năm 2012 tại TP. HCM, Việt Nam, Vienna là cộng đồng tin cậy cho mọi người có thể tìm kiếm, đánh giá, bình luận các địa điểm ăn uống: nhà hàng, quán ăn, cafe, bar, karaoke, tiệm bánh, khu du lịch... tại Việt Nam - từ website hoặc ứng dụng di động. Tất cả thành viên từ Bắc đến Nam, Vienna kết nối những thực khách đến với các địa điểm ăn uống lớn nhỏ cả đất nước.
 
						Đến thời điểm hiện tại, Vienna với hàng trăm ngàn địa điểm và hàng trăm ngàn bình luận, hình ảnh tại Việt Nam, ở hầu hết các tỉnh thành. Vienna là cách dễ nhất để bạn có thể tìm kiếm và lựa chọn địa điểm tốt nhất cho mình và bạn bè.
                        </p>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 footer-content">
                        <h6>Tin tức gần đây</h6>
                        <ul class="list-group article" id="current_news">
							
						</ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 footer-content">
                        <h6>Các món ăn mới</h6>
						<ul class="list-group article" id="food_new">
							
						</ul>
                      
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 footer-content">
                      <h6>Liên kết</h6>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ant</p> 
                        <ul class="list-inline footer-btns">
                            <li><a href="https://www.google.com/" class="btn-link small btn-fb"><i class="fa fa-google"></i></a></li>
                            <li><a href="https://www.facebook.com/" class="btn-link small btn-gg"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/" class="btn-link small btn-tw"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://youtube.com/" class="btn-link small btn-yt"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                        
                      </div>
                                     
                    </div>
                </div>              
        </footer>
    <!-- Footer end -->
	<script>
		$(document).ready(function(){
			$.ajax({
				url:"controller/json_footer.php",
				type:"GET",
				data:{
					type:"news",
				},
				dataType:"text",
				success:function(output){
					if(output!="0"){
						output = JSON.parse(output);
						for(var i = 0; i<output.length;i++){
							var li = $('<li class="list-group-item"></li>');
							var div =  $('<div class="row"></div>');
							var image = $('<div class="col-md-2 col-lg-2 col-xs-2 col-sm-2"><a  href="tin-tuc-'+formatlink(output[i].name) +'-' + output[i].id +'.html"><img style="height:70px;width:70px;" class="image-responsive" src="upload/news/'+ output[i].avatar +'" /></a></div>');
							var name = $('<div class="col-md-9 col-lg-9 col-xs-9 col-sm-9 pull-right articletitle"><a  href="tin-tuc-'+formatlink(output[i].name) +'-' + output[i].id +'.html">'+output[i].name+'</a></div>');
							div.append(image).append(name);
							li.append(div);
							$("#current_news").append(li);

						}
					}
				}
			});
			$.ajax({
				url:"controller/json_footer.php",
				type:"GET",
				data:{
					type:"food_new",
				},
				dataType:"text",
				success:function(output){
					if(output!="0"){
						output = JSON.parse(output);
						for(var i = 0; i<output.length;i++){
							var li = $('<li class="list-group-item"></li>');
							var div =  $('<div class="row"></div>');
							var image = $('<div class="col-md-2 col-lg-2 col-xs-2 col-sm-2"><a  href="mon-an-'+formatlink(output[i].name) +'-' + output[i].id +'.html"><img style="height:70px;width:70px;" class="image-responsive" src="upload/products/'+ output[i].avatar +'" /></a></div>');
							var name = $('<div class="col-md-9 col-lg-9 col-xs-9 col-sm-9 pull-right articletitle"><a  href="mon-an-'+formatlink(output[i].name) +'-' + output[i].id +'.html">'+output[i].name+'</a></div>');
							div.append(image).append(name);
							li.append(div);
							$("#food_new").append(li);

						}
					}
				}
			});
		});
	</script>
