<?php
    include('../model/newsmodel.php');
    $newsmodel = new Post_model();
    $newsid = $_REQUEST["newsid"];
    $news= $newsmodel->get_news_by_id($newsid);
    $result = array();
    if(($news->num_rows)==1){
        $row = $news->fetch_assoc();
        $newsmodel->set_view_news_byid($newsid,$row["MostView"]+1);
        $result[] = array(
        "id"=>$row["id"],
        "name"=>$row["Name"],
        "desc"=>$row["Descriptions"],
        );
    }
   echo json_encode($result);
   $newsmodel->close_connect();
   
?>