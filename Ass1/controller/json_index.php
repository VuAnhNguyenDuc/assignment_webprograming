<?php
    include('../model/newsmodel.php');
    include('../model/imagemodel.php');
    include('../model/productsmodel.php');
    $newsmodel = new Post_model();
    $imagemodel = new Image_model();
    $productsmodel = new Products_model();
    $result = array();
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        $type = $_REQUEST["type"];
        if($type=="food_highlights"){
            $query_products = $productsmodel->get_list_products_by_category_id(3);
            while($row= $query_products->fetch_assoc()){
                $image = $imagemodel->get_image_by_id($row["avatarID"])->fetch_assoc()["Name"];
                $address= 'Q.'.$row["District"].', Tp.'. $row["City"];
                $result[] = array(
                    "id"=>$row["productID"],
                    "name"=>$row["Name"],
                    "shortdes"=>$row["ShortDes"],
                    "address"=>$address,
                    "avatar"=>$image,
                    );
            }
        }
        if($type=="p_location"){
            $query_products = $productsmodel->get_list_products_by_category_id(4);
            while($row= $query_products->fetch_assoc()){
                $image = $imagemodel->get_image_by_id($row["avatarID"])->fetch_assoc()["Name"];
                $address= 'Q.'.$row["District"].', Tp.'. $row["City"];
                $result[] = array(
                    "id"=>$row["productID"],
                    "name"=>$row["Name"],
                    "shortdes"=>$row["ShortDes"],
                    "address"=>$address,
                    "avatar"=>$image,
                    );
            }
        }
    }
   echo json_encode($result);
   $newsmodel->close_connect();
?>