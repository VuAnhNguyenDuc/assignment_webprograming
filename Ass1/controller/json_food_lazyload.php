<?php
    include('../model/newsmodel.php');
    include('../model/imagemodel.php');
    include('../model/productsmodel.php');
    include('config.php');
    $newsmodel = new Post_model();
    $imagemodel = new Image_model();
    $productsmodel = new Products_model();
    $html=array();
    $query_products = $productsmodel->get_list_products_by_category_menu_id(3);
    while($row= $query_products->fetch_assoc()){
        $image = $imagemodel->get_image_by_id($row["avatarID"])->fetch_assoc()["Name"];
        $address= 'Q.'.$row["District"].', Tp.'. $row["City"];
        $html[] =  array(
            "html"=>'<div class="col-md-3 col-sm-6 col-lg-3 col-xs-12 product "><div class="row"><a href="mon-an-'.formatlink($row["Name"]).'-'.$row["productID"].'.html"><img src="upload/products/'.$image.'" alt="text" class="img-responsive"></a></div><div class="row productdetail"><p><strong> '.$row["Name"].'</strong></p><p>'.$address.'</p></div></div>'
        );
    }
    $result = array(
        "items"=>$html
    );
    echo json_encode($result);
    $productsmodel->close_connect();      
?>