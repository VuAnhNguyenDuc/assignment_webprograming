<?php
    $message = '
    <!DOCTYPE html >

<html>
<head>
    <title></title>
    <style>
        body{
            background-color:#808080;
           
        }
        #main {
            background-color: white;
            
            margin: 20px 80px 20px 80px;
            border-radius:5px;
            position:relative;
        }
        .step {
            color: #707070;
            font-family: Helvetica,Arial,sans-serif;
            font-size: 40px;
            font-weight: 900;
            letter-spacing: -1.5px;
            text-align: left;
            line-height: 120%;
        }
        .email {
            text-decoration: none;
            color: #d90007;
            font-family: Helvetica,Arial,sans-serif;
            font-size: 18px;
            letter-spacing: -.4px;
            font-weight: 800;
            padding-bottom: 12px;
            line-height: 100%;
            max-width: 500px;
        }
    </style>
    
 </head>
<body>
    <div id="main">
        <div style="margin:auto; width:75%;">
            <p class="step" >One last step is required</p>
            <span style="color:blue;font-size:20px">Hello {name},</span> <br /><br /><br />
            <a style="text-decoration:none;" href="http://localhost/Ass1/kich-hoat-tai-khoan/{url}"><span class="email">{email}</span></a>
            <hr style="border-style: dotted; border-color: #707070;" />
            <p style=" margin-bottom: 30px; padding-top: 6px; color: #707070; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 100; line-height: 135%; letter-spacing: .2px;">
            Click the big button below to activate your Food account:</p>

            <a href="http://localhost/Ass1/kich-hoat-tai-khoan/{url}" id="" style="margin-top:20px;color: #fff; text-decoration: none; background-color: #d90007; border-radius: 6px; border-collapse:separate!important; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 100; line-height: 100%; letter-spacing: 1px; padding-right: 35px; padding-bottom: 15px; padding-left: 35px; padding-top: 15px;">
                Activate Account
            </a>
            <p style=" margin-bottom: 30px;margin-top:30px; padding-top: 6px; color: #707070; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 100; line-height: 135%; letter-spacing: .2px;">
                Or copy and paste this link into your web browser:
            </p>
            <a href="http://localhost/Ass1/kich-hoat-tai-khoan/{url}" style="text-decoration:none;color:#d90007" target="_blank">
                <span>
                    http://localhost/Ass1/kich-hoat-tai-khoan/{url}
                </span>
            </a>
            <p style=" margin-bottom: 30px; margin-top: 30px; padding-top: 6px; color: #505050; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 100; line-height: 135%; letter-spacing: .2px;">
                Best regards, 
            </p>
            <span style="color:#d90007"> Team Food </span>
        </div>
        
        <footer style="margin:auto; width:75%;">
            <div style="margin-top:40px; border-top:1px solid #808080">&nbsp;</div>
            <div style="margin-left: 150px; margin-right: 150px;">
                <div style="text-align:center;color:#808080;">
                    <p>&copy; 2016. Admin </p>
                    <p>Contact: Nguyễn Ngọc Thật</p>
                    <p>Email: nguyen.thanhtu229@gmail.com</p>
                    <p>Skype: ngocthat1204@hotmail.com</p>
                    <p>Tel: 01299643331</p>
                </div>
            </div>
        </footer>
    </div>
</body>
</html>
    ';
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $email = $_REQUEST["email"];
        $name = $_REQUEST["name"];
        $subject="Activation Acount";
        $url = base64_encode($email);
        $message = str_replace("{name}",$name,$message);
        $message = str_replace("{email}",$email,$message);
        $message = str_replace("{url}",$url,$message);
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: nguyen.thanhtu229@gmail.com' . "\r\n";
        mail($email,$subject,$message,$headers);
    }

?>