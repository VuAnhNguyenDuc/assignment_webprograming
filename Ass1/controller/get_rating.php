<?php
    include('../model/ratingmodel.php');
    $ratingmodel = new Rating_model();
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        $productid=$_REQUEST["productid"];
        $userid=$_REQUEST["userid"];
        $rating = $ratingmodel->get_rating_by_userid_productid($userid,$productid);
        $result = array();
        if(($rating->num_rows)==1){
          $result = $rating->fetch_assoc();
          echo json_encode($result);
        }else{
            echo "0";
        }
    }
    $ratingmodel->close_connect();
?>