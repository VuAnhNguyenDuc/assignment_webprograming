<?php
    include('../model/newsmodel.php');
    include('../model/imagemodel.php');
    include('../model/productsmodel.php');
    $newsmodel = new Post_model();
    $imagemodel = new Image_model();
    $productsmodel = new Products_model();
    $result = array();
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        $type = $_REQUEST["type"];
        if($type=="news"){
            $listnews= $newsmodel->get_list_news_current();
            if(($listnews->num_rows) > 0){
                while($row=$listnews->fetch_assoc()){
                    $image = $imagemodel->get_image_by_id($row["ImageID"])->fetch_assoc()["Name"];
                    $result[] = array(
                    "id"=>$row["id"],
                    "name"=>$row["Name"],
                    "avatar"=>$image,
                    );
                }  
                echo json_encode($result);
            }
            else  echo "0";
        }
        if($type=="food_new"){
            $query_products = $productsmodel->get_list_products_new();
            if(($query_products->num_rows)>0){
                while($row= $query_products->fetch_assoc()){
                    $image = $imagemodel->get_image_by_id($row["avatar"])->fetch_assoc()["Name"];
                    $result[] = array(
                        "id"=>$row["id"],
                        "name"=>$row["Name"],
                        "avatar"=>$image,
                        );
                }
             echo json_encode($result);   
             } else echo "0";
        }
    }
   $newsmodel->close_connect();
?>