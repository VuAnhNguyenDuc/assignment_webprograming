<?php
    include('../model/newsmodel.php');
    include('../model/imagemodel.php');
    include('../model/productsmodel.php');
    include('../model/ratingmodel.php');
    include('config.php');
    $newsmodel = new Post_model();
    $imagemodel = new Image_model();
    $productsmodel = new Products_model();
    $ratingmodel = new Rating_model();
    $html=array();
    $query_products = $productsmodel->get_list_products_by_category_menu_id(4);
    while($row= $query_products->fetch_assoc()){
        $image = $imagemodel->get_image_by_id($row["avatarID"])->fetch_assoc()["Name"];
        $listrating = $ratingmodel->get_list_rating_by_productid($row["productID"]);
        $total_location=0;
        $total_quality=0;
        $total_price=0;
        $total_space=0;
        $total_service=0;
        while($rating = $listrating->fetch_assoc()){
            $total_location+=$rating["Address_Rate"];
            $total_quality+=$rating["Quanlity_Rate"];
            $total_price+=$rating["Price_Rate"];
            $total_space+=$rating["Space_Rate"];
            $total_service+=$rating["Service_Rate"];
        }
        $number = $listrating->num_rows;
        if(($listrating->num_rows)>0){
            $total_location=round(($total_location/$number),1);
            $total_quality=round(($total_quality/$number),1);
            $total_price=round(($total_price/$number),1);
            $total_space=round(($total_space/$number),1);
            $total_service=round(($total_service/$number),1);
        }
        $avg = round(($total_location+$total_quality+$total_price+$total_space+$total_service)/5,1);
        $html[] =  array(
            "html"=>'<div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 product">
						<div class="col-md-12">
							<div class="row head">
								<div class="col-md-2 col-xs-2 point">
									<p style="font-size: 45px; text-align: center;"><strong>'.$avg.'</strong></p>
								</div>
								<div class ="col-md-10 col-xs-10">
									<div class="row">
										<strong>'.$row["Name"].'</strong>
									</div>
									<div class="row">
										'.$row["AdressFull"].'
									</div>
								</div>
							</div>
							<div class="row">
								<a href="mon-an-'.formatlink($row["Name"]).'-'.$row["productID"].'.html"><img style="width:555px;height:346.88px" class="img-responsive" src="upload/products/'.$image.'" alt="text"></a>
							</div>
						</div>	
					</div>'
        );
    }
    $result = array(
        "items"=>$html
    );
    echo json_encode($result);
    $productsmodel->close_connect();      
?>