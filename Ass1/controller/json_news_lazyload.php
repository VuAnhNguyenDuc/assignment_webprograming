<?php
    include('../model/newsmodel.php');
    include('../model/imagemodel.php');
    include('../model/commentModel.php');
    include('config.php');
    $newsmodel = new Post_model();
    $imagemodel = new Image_model();
    $commentmodel = new Comment_model();
    $listnews= $newsmodel->get_list_news();
    $html=array();
    while($row=$listnews->fetch_assoc()){
        $image = $imagemodel->get_image_by_id($row["ImageID"])->fetch_assoc()["Name"];
        $lstcomment = $commentmodel->get_list_comment_news($row["id"]);
        $numbercomment=$lstcomment->num_rows;
        
        
        $date = strtotime( $row["Postdate"]);
        $datenow = strtotime(date("Y-m-d h:i:s"));
        $year =  ($datenow-$date)/(60*60*24*365);
        $time ="";
        if($year>=1)
            $time = $year ." năm trước";
        else if(($year*365/30)>=1) 
            $time = round($year*365/30 ). " tháng trước";
        else if (($year*365)>=1)
            $time = round($year*365) . " ngày trước";
        else if(($year*365*24)>=1)
            $time = round(year*365*24) . " giờ trước";
        else if(($year*365*24*60)>=1)
            $time = round($year*365*24*60) . " phút trước";
        else if (($ear*365*24*60*60)>=1)
            $time = round($year*365*24*60*60) . " giây trước";
        $html[] =  array("html"=>'<div class="col-sm-12 news-list"><a href="tin-tuc-'.formatlink($row["Name"]).'-'.$row["id"].'.html"><img src="upload/news/'.$image.'" style="width:850px;" class="img-responsive"></a><div class="news-items"><a href="tin-tuc-oc-nhay-gangnam-style-1.html"><span style="font-size: 30px; color: #829f26;">'.$row["Name"].'</span></a></div><div class="col-sm-12"><span class="news-time">'.$time.'</span><span class="comment"><span class="sep"> /</span>'.$numbercomment.' Comment</span> </div><div class="col-sm-12" style="margin-top:20px; margin-bottom:20px;"><p>'.$row["ShortDescriptions"].'</p></div><a href="tin-tuc-'.formatlink($row["Name"]).'-'.$row["id"].'.html" class="btn btn-primary" style="background-color:#829f26;float:right;">Read More...</a><div class="clr"></div><hr></div>'
            );
    }  
    
    $result = array(
        "items"=>$html
    );
    echo json_encode($result);
    $newsmodel->close_connect();
?>