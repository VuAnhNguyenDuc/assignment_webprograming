<?php
    include('../model/usermodel.php');
    include('../model/imagemodel.php');
    include('../model/ratingmodel.php');
    $usermodel = new User_model();
    $imagemodel = new Image_model();
    $ratingmodel = new Rating_model();
    $user_id = $_REQUEST["userid"];
    $result = $usermodel->get_user_by_id($user_id);
    if(($result->num_rows>0)){
        
        $user =$result->fetch_assoc();
        $result = array();
        $result_products=$usermodel->get_products_save_by_user_id($user_id);
        $array_product= array();
        while($product=$result_products->fetch_assoc()){
            $listrating = $ratingmodel->get_list_rating_by_productid($product["Product_ID"]);
            $total_location=0;
            $total_quality=0;
            $total_price=0;
            $total_space=0;
            $total_service=0;
            while($rating = $listrating->fetch_assoc()){
                $total_location+=$rating["Address_Rate"];
                $total_quality+=$rating["Quanlity_Rate"];
                $total_price+=$rating["Price_Rate"];
                $total_space+=$rating["Space_Rate"];
                $total_service+=$rating["Service_Rate"];
            }
           $number = $listrating->num_rows;
            if(($listrating->num_rows)>0){
                
                $total_location=round(($total_location/$number),1);
                $total_quality=round(($total_quality/$number),1);
                $total_price=round(($total_price/$number),1);
                $total_space=round(($total_space/$number),1);
                $total_service=round(($total_service/$number),1);
            }
            $avg = round(($total_location+$total_quality+$total_price+$total_space+$total_service)/5,1);
            $array_product[]=array(
                "Product_ID"=>$product["Product_ID"],
                "Product_Name"=>$product["Name"],
                "Product_Avatar"=>$product["Avatar"],
                "Product_ShortDes"=>$product["ShortDes"],
                "Avg_Rating"=>$avg

            );
        }
        $user_avatar = $imagemodel->get_image_by_id($user["Avatar_ID"])->fetch_assoc()["Name"];
        $result[] = array(
        "id"=>$user["id"],
        "FirstName"=>$user["FirstName"],
        "LastName"=>$user["LastName"],
        "UserName"=>$user["UserName"],
        "Email"=>$user["Email"],
        "Phone"=>$user["Phone"],
        "Address"=>$user["Adress"],
        "BirthDay"=>$user["BirthDay"],
        "Avatar"=>$user_avatar,
        "Save_product"=>$array_product
        );
        
        echo json_encode($result);
    }else echo "0";
    $usermodel->close_connect();
?>