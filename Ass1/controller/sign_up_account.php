<?php

    include('../model/usermodel.php');
    include('../model/imagemodel.php');

    $usermodel = new User_model();
    $imagemodel = new Image_model();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $usr=$_REQUEST["username"];
        $email = $_REQUEST["email"];
        $result = array();
        $usercheck = $usermodel->get_user_by_username($usr);
        $emailcheck = $usermodel->get_user_by_email($email);  
        if(($usercheck->num_rows)>0){
            $result[] = array(
                "success"=>"0",
                "error"=>"Tài khoản đã có người sử dụng",
            );  
        }
        if(($emailcheck->num_rows)>0){
            $result[] = array(
                "success"=>"0",
                "error"=>"Email đã được đăng ký với tài khoản khác",
            );   
        }
        if((($usercheck->num_rows)==0)&&(($emailcheck->num_rows)==0)){
            $data= $_REQUEST["base64"];
            $firstName= $_REQUEST["fname"];
            $lastName= $_REQUEST["lname"];
            $pass = $_REQUEST["password"];
            $address = $_REQUEST["address"];
            $birthday = $_REQUEST["birthday"];
            $phone = $_REQUEST["phone"];
            $image = $imagemodel->insert_image("demo.jpg",1);
            if($image!=false){
                $imagename="image_".$image.".jpg";
                if( $imagemodel->update_image($image,$imagename)){
                    $data = base64_decode($data);
                    file_put_contents('../upload/'.$imagename, $data);
                    file_put_contents('../upload/users/'.$imagename, $data);
                    $adduser = $usermodel-> add_user($firstName,$lastName,$usr,$email,$phone,sha1($pass),$image,$address,$birthday,0);
                    $addrole = $usermodel->add_role($adduser,2);
                    $result[] = array(
                        "success"=>"1",
                        "error"=>"Đăng ký thành công!",
                    );
                }
            }else{
                $result[] = array(
                        "success"=>"0",
                        "error"=>"Lỗi vui long thử lại!",
                    );
            }
            
        }
      
        echo json_encode($result);
        $usermodel->close_connect();
    }

   
?>