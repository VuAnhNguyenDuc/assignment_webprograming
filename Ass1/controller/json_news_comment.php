<?php

    include('../model/commentModel.php');

    $commentmodel = new Comment_model();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $newsid=$_REQUEST["newsid"];
        $result = array();
        $listcomments= $commentmodel->get_list_comment_news($newsid);
        if ($listcomments!=false){
            while($row=$listcomments->fetch_assoc()){
                $result[] = array(
                "content"=>$row["Contents"],
                "commentdate"=>$row["CommentTime"],
                "author"=>$row["Name"],
                );
            }  
       }else {
            $result[] = array(
            "content"=>"Chưa có bình luận!",
            );
        }
        echo json_encode($result);
        $commentmodel->close_connect();
    }

   
?>