<?php
    include('../model/newsmodel.php');
    include('../model/imagemodel.php');
    $newsmodel = new Post_model();
    $imagemodel = new Image_model();
    $listnewshot = $newsmodel->get_list_Most_View();
    if(($listnewshot->num_rows>0)){
        $result = array();
        while($row=$listnewshot->fetch_assoc()){
                    $image = $imagemodel->get_image_by_id($row["ImageID"])->fetch_assoc()["Name"];
                    $result[] = array(
                    "id"=>$row["id"],
                    "name"=>$row["Name"],
                    "avatar"=>$image,
                    );
                }  
                echo json_encode($result);
    }else echo "0";
    $newsmodel->close_connect();
?>