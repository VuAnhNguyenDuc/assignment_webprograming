<?php
    include('../model/newsmodel.php');
    include('../model/imagemodel.php');
    include('../model/commentModel.php');
    $newsmodel = new Post_model();
    $imagemodel = new Image_model();
    $commentmodel = new Comment_model();
    $listnews= $newsmodel->get_list_news();
    $result = array();

    while($row=$listnews->fetch_assoc()){
        $image = $imagemodel->get_image_by_id($row["ImageID"])->fetch_assoc()["Name"];
        $numbercomment=0;
        $lstcomment = $commentmodel->get_list_comment_news($row["id"]);
        
        while($comment=$lstcomment->fetch_assoc() ){
            $numbercomment+=1;
        }
            
         
        $result[] = array(
        "id"=>$row["id"],
        "name"=>$row["Name"],
        "shortdes"=>$row["ShortDescriptions"],
        "postdate"=>$row["Postdate"],
        "avatar"=>$image,
        "comment"=>$numbercomment,
        );
    }  
    
   echo json_encode($result);
   $newsmodel->close_connect();
?>