<?php

    include('../model/productsmodel.php');
    include('../model/imagemodel.php');
    include('../model/ratingmodel.php');
    $ratingmodel = new Rating_model();
    $productmodel = new Products_model();
    $imagemodel = new Image_model();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $productid=$_REQUEST["id"];
        $result = array();
        $product = $productmodel->get_product_by_id($productid);
        if (($product->num_rows)==1){
            
            $listrating = $ratingmodel->get_list_rating_by_productid($productid);
            $total_location=0;
            $total_quality=0;
            $total_price=0;
            $total_space=0;
            $total_service=0;
            while($rating = $listrating->fetch_assoc()){
                $total_location+=$rating["Address_Rate"];
                $total_quality+=$rating["Quanlity_Rate"];
                $total_price+=$rating["Price_Rate"];
                $total_space+=$rating["Space_Rate"];
                $total_service+=$rating["Service_Rate"];
            }
           $number = $listrating->num_rows;
            if(($listrating->num_rows)>0){
                
                $total_location=round(($total_location/$number),1);
                $total_quality=round(($total_quality/$number),1);
                $total_price=round(($total_price/$number),1);
                $total_space=round(($total_space/$number),1);
                $total_service=round(($total_service/$number),1);
            }
            $avg = round(($total_location+$total_quality+$total_price+$total_space+$total_service)/5,1);
            $rate = array(
                "Location"=>$total_location,
                "Quality" =>$total_quality,
                "Price" => $total_price,
                "Space" =>$total_space,
                "Service"=>$total_service,
                "Number"=> $number,
                "Avg" => $avg

            ); 
            $row=$product->fetch_assoc();
            $productmodel->set_product_most_views($productid,$row["MostView"]+1);
            $avatar = $imagemodel->get_image_by_id($row["avatar"])->fetch_assoc()["Name"];
            $listaddress = $productmodel->get_address_by_productsid($productid);
            $address="";
            if(($listaddress->num_rows)>1){
                $i =1;
                while($row_address= $listaddress->fetch_assoc()){
                    $address=$address."Vị trí ".$i.": ".$row_address["AdressFull"]."<br/>";
                    $i=$i+1;
                }
            }else  $address=$listaddress->fetch_assoc()["AdressFull"];
            $listres =  $productmodel->get_restaurant_by_productsid($productid);
            $restaurant ="";
            if(($listres->num_rows)>1){
                $j =1;
                while($row_restaurant= $listres->fetch_assoc()){
                    $restaurant=$restaurant."Nhà hàng ".$j.": ".$row_restaurant["Name"]."<br/>";
                    $j=$j+1;
                }
            }else $restaurant = $listres->fetch_assoc()["Name"];
            $listimage = $productmodel->get_image_by_productsid($productid);
            $image = array();
            while($row_image= $listimage->fetch_assoc()){
                $image[]=$row_image;
            }
            $listvideo = $productmodel->get_list_video_by_productsid($productid);
            $video=array();
            while($row_video=$listvideo->fetch_assoc()){
                $image_video = substr($row_video["Link"], strpos($row_video["Link"], "=") + 1);
                $video[]=array(
                    "Link"=>$row_video["Link"],
                    "Image"=>$image_video,
                );
            }
            $result[] = array(
                "Name"=>$row["Name"],
                "Restaurant"=> $restaurant,
                "Avatar" => $avatar,
                "Rating"=>$rate,
                "Address" =>$address,
                "Hotline" => $row["Hotline"],
                "OpenTime" =>$row["OpentTime"],
                "Price" =>$row["Price"],
                "Descriptions"=>$row["Descriptions"],
                "Image"=>$image,
                "Videos"=>$video
            );
            echo json_encode($result);
            }  
         else {
           echo "0";
        }
        $productmodel->close_connect();
    }

   
?>