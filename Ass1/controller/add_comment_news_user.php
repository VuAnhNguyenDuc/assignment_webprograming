<?php

    include('../model/commentModel.php');
    include('../model/userModel.php');
    $commentmodel = new Comment_model();
    $usermodel = new User_model();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $comment = $_REQUEST["comment"];
        $newsid = $_REQUEST["newsid"];
        $userid = $_REQUEST["userid"];
        $name = $_REQUEST["name"];
        $user = $usermodel->get_user_by_id($userid);
        $result = array();
        $insert = $commentmodel->insert_comment_news_user($userid,$name,($user->fetch_assoc())['Email'],$comment,$newsid);
        if($insert!=false){
            $record=$commentmodel->get_comment_by_id($insert);
            $row=$record->fetch_assoc();
            $result[] = array(
            "content"=>$row["Contents"],
            "commentdate"=>$row["CommentTime"],
            "author"=>$row["Name"],
            "success"=>"1",
            "id"=>$insert,
            );
            
       }else {
            $result[] = array(
            "success"=>"0",
            );
        }
        echo json_encode($result);
        $commentmodel->close_connect();
    }

   
?>