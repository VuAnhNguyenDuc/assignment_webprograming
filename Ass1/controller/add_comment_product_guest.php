<?php

    include('../model/commentModel.php');

    $commentmodel = new Comment_model();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $usr=$_REQUEST["user"];
        $email = $_REQUEST["email"];
        $comment = $_REQUEST["comment"];
        $productid = $_REQUEST["id"];
        $result = array();
        $insert = $commentmodel->insert_comment_product_guest($usr,$email,$comment,$productid);
        if($insert!=false){
            $record=$commentmodel->get_comment_by_id($insert);
            $row=$record->fetch_assoc();
            $result[] = array(
            "content"=>$row["Contents"],
            "commentdate"=>$row["CommentTime"],
            "author"=>$row["Name"],
            "success"=>"1",
            "id"=>$insert,
            );
            
       }else {
            $result[] = array(
            "success"=>"0",
            );
        }
        echo json_encode($result);
        $commentmodel->close_connect();
    }

   
?>