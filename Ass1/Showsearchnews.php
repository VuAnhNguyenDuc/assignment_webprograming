<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Kết quả tìm kiếm</title>



    <!-- main css -->
    <link href="css/style_news.css" rel="stylesheet">
    
    
    <style>
        .report_text {
            display: block;
            overflow: hidden;
            padding: 10px 0 15px;
            font-size: 18px;
            color: #a9a9a9;
        }
        .noresultsuggestion h3 {
            font-size: 16px;
            font-weight: 600;
        }
        .noresultsuggestion ul {
            margin: 10px 0;
            line-height: 20px;
        }
        .noresultsuggestion ul li {
            list-style-position: inside;
            list-style-type: disc;
        }
    </style>
</head>

<body >
<?php include('header.php')?>
    <section class="news-background">
        <div class="container">
            <div class="col-sm-12 no-padding">

                <div class="row color-news" style="margin-right:-10px;">
                    <h1 style="text-align:center">Kết quả tìm kiếm</h1>
                    <hr />
                    <div id="not_results" style="display:none" class="container">
                        <div class="report_text">
                            <h1>Rất tiếc, không tìm thấy kết quả nào phù hợp với từ khóa <strong class="keyword_show"></strong></h1>
                        </div>
                        <div class="noresultsuggestion">
                            <h3>Để tìm được kết quả chính xác hơn, bạn vui lòng:</h3>
                            <ul>
                                <li>Kiểm tra lỗi chính tả của từ khóa đã nhập</li>
                                <li>Thử lại bằng từ khóa khác</li>
                                <li>Thử lại bằng những từ khóa tổng quát hơn</li>
                                <li>Thử lại bằng những từ khóa ngắn gọn hơn</li>
                            </ul>
                        </div>
                    </div>
                    <br />
                    <div id="contents" class="" style="display:none" >
                    <div class=" container report_text">
                        <h1>Tìm thấy <strong id="number_result"> kết quả</strong> phù hợp với từ khóa <strong class="keyword_show"></strong> trong đó có:/h1>
                        
                    </div>
                    <div class=" lazy-wrapper">
                
                    </div>
                    <div class="clr"></div>
                </div>

            </div>
             <div class="clr"></div>
        </div>
    </section>
    
  <?php include('footer.php')?>
  <script src="js/imagesloaded.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.isotope.sloppy-masonry.min.js"></script>
	<script src="js/lazyloader.js"></script>
	<script>
    var keyword = '<?php echo $_REQUEST["tu-khoa"] ?>';
    $(document).ready(function(){
        $(".keyword_show").html('"'+keyword+'"')
        $.ajax({
            url:"controller/check_result_searchnews.php",
            type:"POST",
            data:{
                keyword:keyword,
            },
            dataType:"text",
            success:function(output){
                if(output!="0"){
                    
                    $("#number_result").html(output+" kết quả");
                     $("#contents").css("display","block");
                     $("#not_results").css("display","none");
                     $(function(){
                        $('.lazy-wrapper').lazyLoader({
                            ajaxLoader: 'controller/json_search_news_enter.php?keyword='+keyword,
                            //jsonFile: 'data.json',
                            mode: 'scroll',
                            limit: 1,
                            records:1000,
                            offset: 1,
                            isIsotope: true,
                            isotopeResize: 3
                        });
                    });
                }else {
                    $("#not_results").css("display","block");
                    $("#contents").css("display","none");
                }
            }
            
        });
       
    });
	 
    </script>
  
</body>
</html>
