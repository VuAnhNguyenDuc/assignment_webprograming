<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Thông tin tài khoản</title>
    

    <!-- contact style css -->
    <link href="css/style_contact.css" rel="stylesheet">
    <!-- account styel css -->
    <link href="css/style_account.css" rel="stylesheet">    
    <!-- Development Google Fonts -->
  </head>
    <body style="background: #f5f5f5">
    <?php require_once('header.php');?>
    <!-- Body begin -->
    <div class="wrap">
        <div class="acc-menu">
            <div class="menu-header">
                <h1>Username</h1>
            </div>
            <div class="ava-wrap">
                <div class="img-wrap">
                    <img id="avatar_user" src=""/>
                </div>
                <div class="acc-info">
                    <h1 id="user_fullname">Fullname</h1>
                    <h4 ><i class="fa fa-envelope"></i> <span id="user_email" style=" text-transform: lowercase;"> Email</span></h4>
                    <h6><i class="fa fa-phone"></i> <span id="user_phone"> Phone Number</span></h6>
                </div>
            </div>
            
            <nav >
                <div class="separator-aside"></div>
                <div class="tab-container">
                    <a href="#profile" class="active" data-toggle="tab"><span class="menu-name">Hồ sơ người dùng</span><span class="fa fa-home"></span></a>
                    <a href="#saved-pages" data-toggle="tab"><span class="menu-name">Món ăn đã lưu</span><span class="fa fa-save"></span></a>
                    <a href="#pass-change" data-toggle="tab"><span class="menu-name">Đổi mật khẩu</span><span class="fa fa-user-secret"></span></a>
                    <a href="#" class="log_out" data-toggle="tab"><span class="menu-name">Đăng xuất</span><span class="fa fa-sign-out"></span></a>
                </div>
                <div class="separator-aside"></div>
            </nav>
            <div class="separator-aside"></div>
            <div class="menu-footer">
                <a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a>
                <a href="https://mail.google.com/"><i class="fa fa-google"></i></a>
                <a href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
                <a href="https://www.linkedin.com/"><i class="fa fa fa-linkedin"></i></a>
                <a href="https://github.com/"><i class="fa fa fa-github"></i></a>
            </div>
        </div>
        <div class="acc-content tab-content">
            
            <div class="tab-pane" id="profile">
            	<div class="content-header">
                	<h1><i class="fa fa-user"></i>Hồ sơ người dùng</h1>
            	</div>
            	<div class="row">
	            	<div class="col-lg-12">
		            	<div class="profile-content">
		            		<h3><i class="fa fa-user"></i> Thông tin trên Internet</h3>
			       			<div class="list-container">
				       			<div class="col-xs-4"><Label>Tên tài khoản</Label></div>			       						
				       			<div class="col-xs-8">
                                    <input id="user_name" type="text" name="uname" readonly value="Username">
                                </div>				
			       			</div>

		            		<h3><i class="fa fa-info"></i> Thông tin cá nhân</h3>
		       				<div class="list-container">
				       			<div class="col-xs-4"><label>Tên</label></div>			       						
				       			<div class="col-xs-8">
                                    <input id="last_name" type="text" name="fname" readonly value="First Name">
                                    <a id="edit_lastname" class="" style="cursor:pointer"><i class="fa fa-edit"></i> Edit</a>
                                </div>				
			       			</div>
			       			<div class="list-container">
				       			<div class="col-xs-4"><Label>Họ</Label></div>			       						
				       			<div class="col-xs-8">
                                    <input id="first_name" type="text" name="lname" readonly value="Last Name">
                                    <a id="edit_firstname" class="" style="cursor:pointer"><i class="fa fa-edit"></i> Edit</a>
                                </div>				
			       			</div>

			       			<div class="list-container">
				       			<div class="col-xs-4"></i><Label>Ngày sinh</Label></div>			       						
				       			<div class="col-xs-8">
                                    <input id="birth_day" type="date" name="dob" readonly value="Day of Birth">
                                    <a id="edit_birthday" class="" style="cursor:pointer"><i class="fa fa-edit"></i> Edit</a>
                                </div>				
			       			</div>
			       			<h3><i class="fa fa-phone"></i> Thông tin liên lạc</h3>
			       			<div class="list-container">
				       			<div class="col-xs-4"><Label>Địa chỉ</Label></div>			       						
				       			<div class="col-xs-8">
                                    <input id="adderss_user" type="text" name="addr" readonly value="Address">
                                    <a id="edit_address" class="" style="cursor:pointer"><i class="fa fa-edit"></i> Edit</a>
                                </div>				
			       			</div>
			       			<div class="list-container">
				       			<div class="col-xs-4"><Label>Số điện thoại</Label></div>			       						
				       			<div class="col-xs-8">
                                    <input id="phone_user" type="text" name="phoneNo" readonly value="Phone Number">
                                    <a id="edit_phone" class="" style="cursor:pointer"><i class="fa fa-edit"></i> Edit</a>
                                </div>				
			       			</div>
                            <div class="list-container">
				       			<div class="col-xs-4"><Label>Email</Label></div>			       						
				       			<div class="col-xs-8">
                                    <input id="email_user" type="text" name="email" readonly value="Email">
                                    <a id="edit_email" class="" style="cursor:pointer"><i class="fa fa-edit"></i> Edit</a>
                                </div>				
			       			</div>
                            <div class="list-container">
                                    <button id="update_info" type="button" class="btn btn-success" style="width:10%;margin-left:45%;display:none">Lưu</button>
			       			</div>			       			
		            	</div>
	            	</div>
            	</div>
            </div>
            <div class="tab-pane" id="saved-pages">
            	<div class="content-header">
                	<h1><i class="fa fa-save"></i>Món ăn đã lưu</h1>
            	</div>
                <div class="pages-wrap">
                	<div class="pages-content">
                        <div class="row">
                            <div id="products_save_left" class="col-lg-6">
                            </div>
                            <div id="products_save_right" class="col-lg-6">
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="pass-change">
            	<div class="content-header">
                	<h1><i class="fa fa-user-secret"></i>Đổi mật khẩu</h1>
            	</div>
	            <div class="row">
	                <div class="col-lg-3 col-md-2 col-sm-1"></div>
	                <div class="col-lg-6 col-md-8 col-sm-10">
	                    <div class="pass-change-form">
	                        <div class="title" style="font-size: 36px; text-align: center;">
	                            Thông tin đổi mật khẩu</div>
	                            <ul id="error"> 
	                                <li>Xin vui lòng cung cấp chính xác thông tin</li>
	                                <li>Các trường bắt buộc được đánh dấu *</li>
	                            </ul>
	                            <hr>
	                        <form id="pass_change" method="post">
	                            <div class="form-group">
	                                <input type="password" id="old_pass" name="old_pass" placeholder="Mật khẩu cũ*" class="form-control form-item" required="required"></div>
	                            <div class="form-group">
	                                <input type="password" id="new_pass" name="new_pass" placeholder="Mật khẩu mới*" class="form-control form-item" required="required"></div>
	                            <div class="form-group">
	                                <input type="password" id="comfirm_new_pass" name="comfirm_new_pass" placeholder="Xác nhận mật khẩu mới*" class="form-control form-item" required="required"></div>
	                            <button type="submit" class="btn btn-1">Thay đổi</button>
	                        </form>
	                    </div>
	                </div>
	            </div>
            </div>
        </div>
    </div>
    <div id="menu-panel" style="overflow: hidden; padding: 0; width: 50px;">
        <div class="tab-container" style="width: 49px;">

        </div>
    </div>
    <!-- Body end -->

    <?php require_once('footer.php');?>

    <script>
        $(document).ready(function(){
            var userid = $("#userid").val();
            $.ajax({
                url:"controller/json_account.php",
                type:"POST",
                data:{
                    userid:userid
                },
                success:function(output){
                    if(output!="0"){
                        output=JSON.parse(output);
                        $("#avatar_user").attr("src","upload/users/"+output[0].Avatar);
                        $("#user_fullname").html(output[0].FirstName+" "+output[0].LastName);
                        $("#user_email").html(output[0].Email);
                        $("#user_phone").html(output[0].Phone);
                        $("#user_name").val(output[0].UserName);
                        $("#last_name").val(output[0].LastName);
                        $("#first_name").val(output[0].FirstName);
                        $("#birth_day").val(output[0].BirthDay);
                        $("#adderss_user").val(output[0].Address);
                        $("#phone_user").val(output[0].Phone);
                        $("#email_user").val(output[0].Email);
                        for(var i = 0 ; i<output[0].Save_product.length;i++){
                            var div = $('<div class="saved-dishes"></div>');
                            var image = $('<a href="mon-an-'+formatlink(output[0].Save_product[i].Product_Name) +'-' + output[0].Save_product[i].Product_ID +'.html"><img src="upload/products/'+output[0].Save_product[i].Product_Avatar+'" alt class="img-responsive"></a>');
                            var info = $('<div class="dish-info"><h3>'+output[0].Save_product[i].Product_Name+'</h3><p>'+output[0].Save_product[i].Product_ShortDes+'</p></div>');
                            var score = $('<span class="score">'+output[0].Save_product[i].Avg_Rating+'</span>');
                            div.append(image).append(info).append(score);
                            if((i%2)==0){
                                
                                $("#products_save_left").append(div);
                            }else{
                                $("#products_save_right").append(div);
                            }
                        }
                    }
                }
            });
            $(".log_out").click(function(){
                    $.ajax({
                         url:"controller/logout_account.php",
                         type:"POST",
                         dataType:"text",
                         success:function(output){
                             location.href="trang-chu.html";
                         }

                     });
            });
            $("#pass_change").submit(function(e){
                e.preventDefault();

                 var old_password = $("#old_pass").val();
                 var new_password = $("#new_pass").val();
                 var comfirm_password = $("#comfirm_new_pass").val();
                 if(new_password != comfirm_password){
                     var error = document.getElementById('error_pass');
                     if(error==null){
                        var li = $('<li id="error_pass" style="color:red">*Mật khẩu không đúng </li>');
                        $("#error").append(li);
                     }
                     
                 }else $("#error_pass").remove();
                if(new_password == comfirm_password){
                    var username = $("#user_name").val();
                    $.ajax({
                        url:"controller/change_password_account.php",
                        type:"POST",
                        data:{
                            old_password:old_password,
                            new_password:new_password,
                            username:username
                        },
                        dataType:"text",
                        success:function(output){
                            if(output=="1"){
                                alert("Cập nhật mật khẩu thành công!");
                                location.reload();
                            }else if(output=="0") alert("Mật khẩu củ không đúng!");
                            else alert("Lỗi! Vui lòng thử lại sao!");
                        }
                    })
                }
            });
            $("#edit_lastname").click(function(){
                $("#last_name").attr("readonly",false);
                $("#last_name").focus();
                $("#update_info").css("display","block");
            });
            $("#edit_firstname").click(function(){
                $("#first_name").attr("readonly",false);
                $("#first_name").focus();
                $("#update_info").css("display","block");
            });
            $("#edit_birthday").click(function(){
                $("#birth_day").attr("readonly",false);
                $("#birth_day").focus();
                $("#update_info").css("display","block");
            });
            $("#edit_address").click(function(){
                $("#adderss_user").attr("readonly",false);
                $("#adderss_user").focus();
                $("#update_info").css("display","block");
            });
            $("#edit_phone").click(function(){
                $("#phone_user").attr("readonly",false);
                $("#phone_user").focus();
                $("#update_info").css("display","block");
            });
            $("#edit_email").click(function(){
                $("#email_user").attr("readonly",false);
                $("#email_user").focus();
                $("#update_info").css("display","block");
            });
            $("#update_info").click(function(){
                var fname = $("#first_name").val();
                var lname = $("#last_name").val();
                var birthday = $("#birth_day").val();
                var address = $("#adderss_user").val();
                var phone = $("#phone_user").val();
                var email = $("#email_user").val();
                $.ajax({
                    url:"controller/update_info_account.php",
                    type:"POST",
                    data:{
                        userid:userid,
                        fname:fname,
                        lname:lname,
                        birthday:birthday,
                        address:address,
                        phone:phone,
                        email:email
                    },
                    dataType:"text",
                    success:function(output){
                        if(output=="1") {
                            alert("Cập nhật thông tin thành công");
                            location.reload();
                        }
                        else alert("Lỗi! Vui lòng thử lại sau");
                    }
                })
            });
            $('#profile').show('pages');
        $('nav .tab-container a:first').addClass('active-sec');
        $('nav .tab-container a').click(function(){
            if($(this).hasClass('active-sec')) {
                /** active **/
            } else {
                $('nav .tab-container a').removeClass('active-sec');
                $(this).addClass('active-sec');
                $('#profile').hide();
                $('#saved-pages').hide();
                $('#pass-change').hide();
                var currentPage = $(this).attr('href');
                $(currentPage).show('pages');
            }
            return false;
        });
        });
    </script>
  </body>
</html>
