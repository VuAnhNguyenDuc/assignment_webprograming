<?php
    class restaurantModel{
        public function __construct(){
            require "connect.php";
        }
        public function getRestaurants(){
            global $dbc;
            $query = "SELECT * FROM restaurant";
            $restaurants = array();
            if($result = mysqli_query($dbc,$query)){
                while($row = mysqli_fetch_assoc($result)){
                    $restaurants[] = $row;
                }
            }
            return json_encode($restaurants);
        }
        public function enableRestaurant($id){
            global $dbc;
            $dbc->begin_transaction();

            try {
                $query = "UPDATE restaurant SET Status = '1' WHERE id='".$id."'";
                if ($result = mysqli_query($dbc, $query)) {
                    $dbc->commit();
                    echo "successful";
                } else {
                    $error = str_replace("'","",mysqli_error($dbc));
                    throw new Exception($error);
                }
            } catch (Exception $e) {
                $dbc->rollback();
                echo $e;
            }
        }
        public function disableRestaurant($id){
            global $dbc;
            $dbc->begin_transaction();

            try {
                $query = "UPDATE restaurant SET Status = '-1' WHERE id='".$id."'";
                if ($result = mysqli_query($dbc, $query)) {
                    $dbc->commit();
                    echo "successful";
                } else {
                    $error = str_replace("'","",mysqli_error($dbc));
                    throw new Exception($error);
                }
            } catch (Exception $e) {
                $dbc->rollback();
                echo $e;
            }
        }
        public function updateRestaurant($id,$name,$service){
            global $dbc;
            $dbc->begin_transaction();

            try {
                $query = "UPDATE restaurant SET Name = '".$name."', Service = '".$service."'  WHERE id='".$id."'";
                if ($result = mysqli_query($dbc, $query)) {
                    $dbc->commit();
                    echo "successful";
                } else {
                    $error = str_replace("'","",mysqli_error($dbc));
                    throw new Exception($error);
                }
            } catch (Exception $e) {
                $dbc->rollback();
                echo $e;
            }
        }
        public function insertRestaurant($name,$service){
            global $dbc;
            $dbc->begin_transaction();

            try {
                $query = "INSERT INTO restaurant(Name, Service, Status) VALUE ('".$name."','".$service."','1')";
                if ($result = mysqli_query($dbc, $query)) {
                    $dbc->commit();
                    echo "successful";
                } else {
                    $error = str_replace("'","",mysqli_error($dbc));
                    throw new Exception($error);
                }
            } catch (Exception $e) {
                $dbc->rollback();
                echo $e;
            }
        }

        public function close(){
            global $dbc;
            $dbc->close();
        }
    }
?>