<?php
    class rateModel{
        public function __construct(){
            require "connect.php";
        }
        public function viewRates(){
            global $dbc;
            $query = "SELECT * FROM rate";
            $rates = array();
            if($result = mysqli_query($dbc,$query)){
                while($row = mysqli_fetch_assoc($result)){
                    $rates[] = $row;
                }
            }
            return json_encode($rates);
        }
        public function close(){
            global $dbc;
            $dbc->close();
        }
    }
?>