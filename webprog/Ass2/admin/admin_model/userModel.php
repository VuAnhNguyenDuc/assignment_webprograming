<?php
    require_once '../admin_controller/ChromePhp.php';

    class userModel{
        function __construct(){
            require 'connect.php';
        }

        public function getUsers(){
            global $dbc;
            $query = "SELECT * FROM user";
            if($result = mysqli_query($dbc,$query)){
                $users = array();
                while($row = mysqli_fetch_assoc($result)){
                    $users[] = $row;
                }
                return json_encode($users);
            } else{
                return "failed";
            }
        }

        public function getAdmins(){
            global $dbc;
            $query = "SELECT user_admin.Admin_ID, user.*  FROM user,user_admin WHERE id IN (SELECT User_ID FROM user_admin)";
            $admins = array();
            if($result =  mysqli_query($dbc,$query)){
                while($row = mysqli_fetch_assoc($result)){
                    $admins[] = $row;
                }
            }
            return json_encode($admins);
        }

        public function getAdmin($username,$password){
            global $dbc;
            $query = "SELECT * FROM user_admin JOIN (SELECT * FROM user WHERE username='".$username."' AND password='".$password."')u ON u.id = user_admin.User_ID WHERE user_admin.Admin_ID=1";
            if($result = mysqli_query($dbc,$query)){
                if($result->num_rows != 0){
                    $row = mysqli_fetch_row($result);
                    return $row;
                } else{
                    return null;
                }
            }
        }

        public function disableUser($id){
            global $dbc;
            $query = "UPDATE user SET Status='-1' WHERE id='".$id."'";
            if($result = mysqli_query($dbc,$query)){
                echo "successful";
            } else{
                $error = str_replace("'","",mysqli_error($dbc));
                echo $error;
            }
        }

        public function enableUser($id){
            global $dbc;
            $query = "UPDATE user SET Status='1' WHERE id='".$id."'";
            if($result = mysqli_query($dbc,$query)){
                echo "successful";
            } else{
                $error = str_replace("'","",mysqli_error($dbc));
                echo $error;
            }
        }

        public function close(){
            global $dbc;
            $dbc->close();
        }
    }
?>