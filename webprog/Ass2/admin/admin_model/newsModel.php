<?php
    class newsModel{
        public function __construct(){
            require "connect.php";
        }
        public function getNews(){
            global $dbc;
            $query = "SELECT * FROM news";
            $news = array();
            if($result = mysqli_query($dbc,$query)){
                while($row = mysqli_fetch_assoc($result)){
                    $news[] = $row;
                }
            }
            return json_encode($news);
        }
        public function disableNews($id){
            global $dbc;
            $dbc->begin_transaction();

            try {
                $query = "UPDATE news SET Status = '-1' WHERE id='".$id."'";
                if ($result = mysqli_query($dbc, $query)) {
                    $dbc->commit();
                    echo "successful";
                } else {
                    $error = str_replace("'","",mysqli_error($dbc));
                    throw new Exception($error);
                }
            } catch (Exception $e) {
                $dbc->rollback();
                echo $e;
            }
        }
        public function enableNews($id){
            global $dbc;
            $dbc->begin_transaction();

            try {
                $query = "UPDATE news SET Status = '1' WHERE id='".$id."'";
                if ($result = mysqli_query($dbc, $query)) {
                    $dbc->commit();
                    echo "successful";
                } else {
                    $error = str_replace("'","",mysqli_error($dbc));
                    throw new Exception($error);
                }
            } catch (Exception $e) {
                $dbc->rollback();
                echo $e;
            }
        }
        public function updateNews($id,$name,$shortDes,$des,$UserID,$ImageID,$PostDate,$Author,$commentID,$mostView){
            global $dbc;
            $dbc->begin_transaction();

            try {
                $query = "UPDATE news SET Name='".$name."',ShortDescriptions='".$shortDes."',Descriptions='".$des."',User_ID='".$UserID."',ImageID='".$ImageID."',Postdate='".$PostDate."',Author='".$Author."',CommentID='".$commentID."',MostView='".$mostView."' WHERE id='".$id."'";
                if ($result = mysqli_query($dbc, $query)) {
                    $dbc->commit();
                    echo "successful";
                } else {
                    $error = str_replace("'","",mysqli_error($dbc));
                    throw new Exception($error);
                }
            } catch (Exception $e) {
                $dbc->rollback();
                echo $e;
            }
        }
        public function insertNews($name,$shortDes,$des,$UserID,$ImageID,$Author,$commentID,$mostView){
            global $dbc;
            $dbc->begin_transaction();

            try {
                $query = "INSERT INTO news(Name, ShortDescriptions, Descriptions, User_ID, ImageID, Postdate, Author, CommentID, MostView, Status) VALUE ($name,$shortDes,$des,$UserID,$ImageID,CURRENT_TIMESTAMP,$Author,$commentID,$mostView,'1')";
                if ($result = mysqli_query($dbc, $query)) {
                    $dbc->commit();
                    echo "successful";
                } else {
                    $dbc->rollback();
                    $error = str_replace("'","",mysqli_error($dbc));
                    throw new Exception($error);
                }
            } catch (Exception $e) {
                $dbc->rollback();
                echo $e;
            }
        }
        public function close(){
            global $dbc;
            $dbc->close();
        }
    }