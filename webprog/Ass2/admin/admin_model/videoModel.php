<?php
    //require_once '../admin_controller/ChromePhp.php';
    class videoModel{
        public function __construct(){
            require "connect.php";
        }
        public function getVideos(){
            global $dbc;
            $query = "SELECT * FROM videos";
            if($result = mysqli_query($dbc,$query)){
                $videos = array();
                while($row = mysqli_fetch_assoc($result)){
                    $query =  "SELECT Name FROM products WHERE id='".$row["Product_ID"]."'";
                    $ProductName = "";
                    if($temp = mysqli_query($dbc,$query)){
                        $row1 = mysqli_fetch_row($temp);
                        $ProductName = $row1[0];
                    }
                    $row["ProductName"] = $ProductName;
                    $videos[] = $row;
                }
                return json_encode($videos);
            } else{
                echo str_replace("'","",mysqli_error($dbc));
            }
            return "";
        }
        public function insertVideo($link,$name,$productID){
            global $dbc;
            $dbc->begin_transaction();
            $query = "INSERT INTO videos(Link, Name, Product_ID) VALUE('".$link."','".$name."','".$productID."')" ;

            try {
                if ($result = mysqli_query($dbc, $query)) {
                    $dbc->commit();
                    echo "successful";
                } else{
                    $error = mysqli_error($dbc);
                    throw new Exception($error);
                }
            } catch (Exception $ex) {
                $dbc->rollback();
                echo $ex;
            }
        }
        public function updateVideo($id,$link,$name,$productID){
            global $dbc;
            $dbc->begin_transaction();
            try {
                $query = "SELECT Name FROM products WHERE id='" . $productID . "'";
                if($result = mysqli_query($dbc,$query)){
                    $row = mysqli_fetch_row($result);
                    $productName = $row[0];
                    $query = "UPDATE videos SET Link='".$link."',Name='".$name."',Product_ID='".$productID."' WHERE id='".$id."'";
                    if($result = mysqli_query($dbc,$query)){
                        $data = array("result"=>"successful","productName"=>$productName);
                        $dbc->commit();
                        echo json_encode($data);
                    } else{
                        $error = str_replace("'","",mysqli_error($dbc));
                        throw new Exception($error);
                    }
                } else{
                    $error = str_replace("'","",mysqli_error($dbc));
                    throw new Exception($error);
                }
            } catch (Exception $e) {
                $dbc->rollback();
                echo $e;
            }
        }
        public function close(){
            global $dbc;
            $dbc->close();
        }
    }
?>