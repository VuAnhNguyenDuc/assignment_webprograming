<?php
    //require_once '../admin_controller/ChromePhp.php';

    class productModel{
        function __construct(){
            require_once 'connect.php';
        }

        public function getProducts(){
            global $dbc;
            $query = "SELECT products.*,address_product.Address_ID FROM products,address_product WHERE address_product.Product_ID=products.id";
            $products = mysqli_query($dbc,$query);
            return $products;
        }

        public function getAddress($id){
            global $dbc;
            $query = "SELECT Address_ID FROM address_product WHERE Product_ID='".$id."'";

            $Address_ID = "";

            if($result = mysqli_query($dbc,$query)){
                while($row = mysqli_fetch_row($result)){
                    $Address_ID = $row[0];
                }
            }

            $query = "SELECT AdressFull,Streets,Ward,District FROM adress WHERE id='".$Address_ID."'";

            if($result = mysqli_query($dbc,$query)){
                $row = mysqli_fetch_row($result);
                $array = array($row[0],$row[1],$row[2],$row[3]);
                return $array;
            } else{
                return "";
            }
        }

        public function getCategory($id){
            global $dbc;
            $query = "SELECT Category_ID FROM product_cate WHERE Product_ID='".$id."'";

            $Category_ID = "";
            $Category = "";

            if($result = mysqli_query($dbc,$query)){
                while($row = mysqli_fetch_row($result)){
                    $Category_ID = $row[0];
                }
            }

            $query = "SELECT Name FROM category WHERE id='".$Category_ID."'";

            if($result = mysqli_query($dbc,$query)){
                while($row = mysqli_fetch_row($result)){
                    $Category = $row[0];
                }
                return $Category;
            }
        }

        public function getProductList(){
            global $dbc;
            $query = "SELECT id,Name FROM products";
            if($result = mysqli_query($dbc,$query)){
                $productList = array();
                while($row = mysqli_fetch_assoc($result)){
                    $productList[] = $row;
                }
                return json_encode($productList);
            }
            return str_replace("'","",mysqli_error($dbc));
        }

        public function insertProduct($Name,$AddressID,$Category,$ShortDes,$Des,$Price,$Hotline,$OpenTime,$MostView,$Avatar,$Images){
            global $dbc;
            $dbc->begin_transaction();
            try{
                // insert into product table
                $query = "INSERT INTO products(Name, ShortDes, Descriptions, Price, Hotline, OpentTime, PostDate,Status, MostView, avatar) VALUE ('".$Name."','".$ShortDes."','".$Des."','".$Price."','".$Hotline."','".$OpenTime."',CURRENT_TIMESTAMP,'1','".$MostView."','".$Avatar."')";
                if(!$result = mysqli_query($dbc,$query)){
                    $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                    throw new Exception($error);
                } else{
                    // return the new product id
                    $nID = $dbc->insert_id;
                }

                $query = "INSERT INTO address_product(Address_ID, Product_ID) VALUE('".$AddressID."','".$nID."')";
                if(!mysqli_query($dbc,$query)){
                    $error = str_replace("'","",mysqli_error($dbc));
                    throw new Exception($error);
                }

                foreach($Images as $image){
                    $query = "INSERT INTO product_image(Product_ID, Image_ID) VALUE('".$nID."','".$image."')";
                    if(!mysqli_query($dbc,$query)){
                        $error = str_replace("'","",mysqli_error($dbc));
                        throw new Exception($error);
                    }
                }

                // Check category
                $catid = ($Category == "Ăn gì?")?3:4;
                $query = "INSERT INTO product_cate(Category_ID, Product_ID) VALUES('".$catid."','".$nID."')";
                if(!$result = mysqli_query($dbc,$query)){
                    $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                    throw new Exception($error);
                }

                $dbc->commit();
                echo "successful";
            } catch (Exception $ex){
                $dbc->rollback();
                echo $ex;
            }
        }

        public function editProduct($id,$Name,$AddressID,$Category,$ShortDes,$Des,$Price,$Hotline,$OpenTime,$PostDate,$MostView,$avatarID){
            global $dbc;
            $dbc->begin_transaction();
            try{
                // update Address
                $query = "UPDATE address_product SET Address_ID='".$AddressID."' WHERE Product_ID='".$id."'";
                if(!mysqli_query($dbc,$query)){
                    $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                    throw new Exception($error);
                }

                // Check category
                $catid = ($Category == "Ăn gì?")?3:4;
                $query = "UPDATE product_cate SET Category_ID = '$catid' WHERE Product_ID = '".$id."'";
                if(!$result = mysqli_query($dbc,$query)){
                    $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                    throw new Exception($error);
                }

                // Update the rest
                $query = "UPDATE products SET Name='".$Name."',ShortDes ='".$ShortDes."',Descriptions='".$Des."',Price='".$Price."',Hotline='".$Hotline."',OpentTime='".$OpenTime."',PostDate='".$PostDate."',MostView='".$MostView."',avatar='".$avatarID."' WHERE id='".$id."'";

                if(!$result = mysqli_query($dbc,$query)){
                    $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                    throw new Exception($error);
                }
                echo "successful";
                $dbc->commit();
            } catch (Exception $ex){
                $dbc->rollback();
                echo $ex;
                //->getMessage()
            }
        }

        public function deleteProduct($id){
            global $dbc;
            $query = "UPDATE products SET Status= '-1' WHERE id='".$id."'" ;

            $dbc->begin_transaction();
            if($result = mysqli_query($dbc,$query)){
                $dbc->commit();
                return "successful";
            } else{
                $dbc->rollback();
                return "failed";
            }
        }

        public function close(){
            global $dbc;
            mysqli_close($dbc);
        }
    }
?>