$(window).load(function(){
    var phpController = "../admin/admin_controller/Controller.php";
    var pathname = "";
    if (document.location.pathname.match(/[^\/]+$/)!=null){
        pathname = document.location.pathname.match(/[^\/]+$/)[0];
    }
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),//decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    var globalRequest = getUrlParameter("request");

    var globalID = 0;
    var globalTr = "";

    // call image picker to use with select images from server
    if($("#images-news").length){
        $("#image-picker-avatar-news").imagepicker();
    }

    // SELECT IMAGES FROM SERVER
    $("#news").on("click",".selectAvatar",function(e){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");
        globalID = id;
        globalTr = tr;

        console.log(id);

        e.preventDefault();
        $('#images-news').modal('show');
    });

    // select ONE image and set the value to the select tag
    $("#select-image").click(function(){
        var imageID = $("#image-picker-avatar-news").val();

        globalTr.find(".avatarID").val(imageID);
        $('#images-news').modal('hide');

        alert("Image set!");

        var path = "http://localhost/webprog/Ass2/upload/"+imageID+".png";

        var newImage = "<img style='width: 100px; height: 100px;' src='"+path+"' alt='images'>";

        globalTr.find("div.avatarDisplay").html("");
        globalTr.find("div.avatarDisplay").append(newImage);

        globalID = 0;
        globalTr = "";
    });

    // handle preview upload image
    $("#news").on("change",".avatarInput",function(event){
        globalTr = $(this).closest("tr");
        globalID = globalTr.attr("id");

        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = reader.result;
            var newImage = "<img class='newsAvatar' style='width: 100px; height: 100px;' src='"+dataURL+"' alt='images'>";
            $('select#image-picker-avatar-news option').removeAttr("selected");
            globalTr.find("div.avatarDisplay").html("");
            globalTr.find("div.avatarDisplay").append(newImage);
        };
        reader.readAsDataURL(input.files[0]);
    });

    // upload image to server
    $("#news").on("click",".uploadAvatar",function(){
        var src = globalTr.find(".newsAvatar").attr("src");

        var base64 = "";
        if(src.indexOf("base64")!=-1){
            base64 = src.replace(/data:image\/(jpeg|png|gif|jpg);base64,/, '');
        }

        var data = {
            request : "uploadNewsImage",
            data : base64,
            name : "demo.png"
        };

        console.log(data);

        $.ajax({
            type: "POST",
            url: phpController,
            cache: false,
            data:{ myData : data},
            dataType: "text",
            success: function(result){
                if(result.indexOf("Failed") < 0){
                    alert("Uploaded successful");
                    globalTr.find(".avatarID").val(result);
                    console.log("new id = " + result);
                    globalID = 0;
                    globalTr = "";
                } else{
                    console.log(result);
                }
            },
            error : function(xhr){
                var err = eval("(" + xhr.responseText +  ")");
                console.log(err.Message);
            }
        });
    });

    // NEWS
    // get news list
    if(pathname == "newsPage.php"){
        $.ajax({
            type: 'POST',
            url: phpController,
            data: 'request=viewNews',
            cache: false,
            success: function(news){
                $('#news').html("");

                $.each(news,function(key,value){
                    var imgSrc = "http://localhost/webprog/Ass2/upload/news/"+value.ImageID+".png";

                    var status = (value.Status == "1")?"Active":"Disabled";
                    $('#news').append('<tr id=' + value.id + '>' +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center Name' contenteditable='true'>" + value.Name + "</td>" +
                        "<td class='center ShortDes' contenteditable='true'>" + value.ShortDescriptions + "</td>" +
                        "<td class='center Des' contenteditable='true'>" + value.Descriptions + "</td>" +
                        "<td class='center UserID' contenteditable='true'>" + value.User_ID + "</td>" +

                        "<td>" +
                        "<div style='margin-bottom: 20px;' class='avatarDisplay'>" +
                        "<img style='width: 100px; height: 100px;' src='"+imgSrc+"' alt='alt'>" +
                        "</div>" +
                        "<input type='file' class='avatarInput'>"+
                        "<button type='button' class='uploadAvatar btn btn-info'>Upload</button> " +
                        "<button type='button' class='selectAvatar btn btn-info'>Select</button>" +
                        "<input type='text' class='avatarID' value='"+value.ImageID+"' style='display: none;'> " +
                        "</td>" +

                        "<td class='center Postdate' contenteditable='true'>" + value.Postdate + "</td>" +
                        "<td class='center Author' contenteditable='true'>" + value.Author + "</td>" +
                        "<td class='center CommentID' contenteditable='true'>" + value.CommentID + "</td>" +
                        "<td class='center MostView' contenteditable='true'>" + value.MostView + "</td>" +
                        "<td class='center Status'>" + status + "</td>" +
                        "<td class='center' colspan='2'><button class='updateNews btn btn-success' style='margin-right: 20px;'>Save</button><button class='enableNews btn btn-primary'>Enable</button> <button class='disableNews btn btn-danger'>Disable</button></td>" +
                        '</tr>');
                });
            },
            error:function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    }

    // disable news
    $("#news").on("click",".disableNews",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        if(confirm("Do you wish to disable this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: "request=disableNews&id="+id,
                dataType: "text",
                cache: false,
                error: function(xhr, status, error){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                },
                success: function(result){
                    if(result == "successful"){
                        alert('Record disabled!');
                        tr.find(".Status").html("Disabled");
                    } else {
                        alert("Disabled failed: "+result);
                    }
                }
            });
        }
    });

    // enable news
    $("#news").on("click",".enableNews",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        if(confirm("Do you wish to disable this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: "request=enableNews&id="+id,
                dataType: "text",
                cache: false,
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                },
                success: function(result){
                    if(result == "successful"){
                        alert('Record enabled!');
                        tr.find(".Status").html("Active");
                    } else {
                        alert("Enabled failed: "+result);
                    }
                }
            });
        }
    });

    // update news
    $('#news').on("click",".updateNews",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        // Update values
        var Name = tr.find(".Name").html();
        var ShortDes = tr.find(".ShortDes").html();
        var Des = tr.find(".Des").html();
        var UserID = tr.find(".UserID").html();
        var ImageID = tr.find(".avatarID").val();
        var Postdate = tr.find(".Postdate").html();
        var Author = tr.find(".Author").html();
        var CommentID = tr.find(".CommentID").html();
        var MostView = tr.find(".MostView").html();

        var data = {
            request: "updateNews",
            id: id,
            Name: Name,
            ShortDes : ShortDes,
            Des : Des,
            UserID : UserID,
            ImageID : ImageID,
            Postdate : Postdate,
            Author : Author,
            CommentID : CommentID,
            MostView : MostView
        };

        console.log(ImageID);

        if(confirm("Do you wish to update this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: {myData: data},
                dataType: "text",
                success: function(result){
                    if(result == "successful"){
                        alert("Update successful");
                        location.reload();
                    } else{
                        alert("Failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // insert news
    $('#insertNews').click(function () {
        var Name = $('#insertName').val();
        var ShortDes = $('#insertShortDes').val();
        var Des = $('#insertDes').val();
        var UserID = $('#insertUserID').val();
        var ImageID = ($("#image-picker").val() != "")?$('#imageName').val():$("#image-picker").val();
        console.log(ImageID);
        if(ImageID == ""){
            alert("Please select an image");
            return false;
        }
        var Postdate = $('#insertPostdate').val();
        var Author = $('#insertAuthor').val();
        var CommentID = $('#insertCommentID').val();
        var MostView = $('#insertMostView').val();

        var data = {
            request: "insertNews",
            Name: Name,
            ShortDes : ShortDes,
            Des : Des,
            UserID : UserID,
            ImageID : ImageID,
            Postdate : Postdate,
            Author : Author,
            CommentID : CommentID,
            MostView : MostView
        };

        $.ajax({
            type: "POST",
            url: phpController,
            dataType: "text",
            data: {myData: data},
            cache: false,
            success:function(result){
                console.log(result);
                if(result == "successful"){
                    alert("Record successfully inserted!");
                    window.location = "newsPage.php";
                }else{
                    alert("Failed:" + result);
                }
            },
            error:function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    });
});