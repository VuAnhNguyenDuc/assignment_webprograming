$(window).load(function(){
    var phpController = "../admin/admin_controller/Controller.php";
    var pathname = "";
    if (document.location.pathname.match(/[^\/]+$/)!=null){
        pathname = document.location.pathname.match(/[^\/]+$/)[0];
    }
    console.log("path = "+pathname);
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),//decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var globalRequest = getUrlParameter("request");

    var imageArr = [];

    var avatarArr = [];

    // SELECT IMAGES FROM SERVER
    $('.select-images').click(function(e){
        e.preventDefault();
        $('#server-images').modal('show');
    });

    $('.select-images-avatar').click(function(e){
        e.preventDefault();
        $('#server-images-avatar').modal('show');
    });

    // handle preview upload image
    $("#imageInput").change(function(event){
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = reader.result;
            var newImage = "<div class='masonry-gallery'><div class='masonry-thumb'>" +
                "<img id='newsImage' class='grayscale' src='"+dataURL+"' alt='images'>" +
                "</div></div>";
            $('select#image-picker option').removeAttr("selected");
            $("#imageDisplay").html("");
            $("#imageDisplay").append(newImage);
        };
        reader.readAsDataURL(input.files[0]);
    });

    $("#imageInputAvatar").change(function(event){
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = reader.result;
            var newImage = "<div class='masonry-gallery'><div class='masonry-thumb'>" +
                "<img id='avatarImage' class='grayscale' src='"+dataURL+"' alt='images'>" +
                "</div></div>";
            $('select#image-picker-avatar option').removeAttr("selected");
            $("#imageDisplayAvatar").html("");
            $("#imageDisplayAvatar").append(newImage);
        };
        reader.readAsDataURL(input.files[0]);
    });

    // upload image to server
    $('#uploadImage').click(function(){
        var src = $("#newsImage").attr("src");

        var base64 = "";
        if(src.indexOf("base64")!=-1){
            base64 = src.replace(/data:image\/(jpeg|png|gif|jpg);base64,/, '');
        }

        var data = {
            request : (globalRequest != "insertProduct")?"uploadNewsImage":"uploadProductImage",
            data : base64,
            name : "demo.png"
        };

        $.ajax({
            type: "POST",
            url: phpController,
            cache: false,
            data:{ myData : data},
            dataType: "text",
            success: function(result){
                if(result.indexOf("Failed") < 0){
                    alert("Uploaded successful");
                    console.log("new image id = " + result);
                    $("#imageName").val(result);
                    console.log($("#imageName").val());
                    $("image-picker").val("");
                    imageArr.push(result);
                } else{
                    console.log(result);
                }
            },
            error : function(xhr){
                var err = eval("(" + xhr.responseText +  ")");
                console.log(err.Message);
            }
        });
    });

    $('#uploadImageAvatar').click(function(){
        var src = $("#avatarImage").attr("src");

        var base64 = "";
        if(src.indexOf("base64")!=-1){
            base64 = src.replace(/data:image\/(jpeg|png|gif|jpg);base64,/, '');
        }
        var data = {
            request : "uploadProductImage",
            data : base64,
            name : "demo.png"
        };
        $.ajax({
            type: "POST",
            url: phpController,
            cache: false,
            data:{ myData : data},
            dataType: "text",
            success: function(result){
                if(result.indexOf("Failed") < 0){
                    alert("Uploaded successful");
                    $("#imageNameAvatar").val(result);
                } else{
                    console.log(result);
                }
            },
            error : function(xhr){
                var err = eval("(" + xhr.responseText +  ")");
                console.log(err.Message);
            }
        });
    });

    // call image picker to use with select images from server
    if($("#image-picker").length){
        $("#image-picker").imagepicker();
    }

    if($("#image-picker-avatar").length){
        $("#image-picker-avatar").imagepicker();
    }

    if($("#image-picker-multi").length){
        $("#image-picker-multi").imagepicker();
    }

    // select ONE image and set the value to the select tag
    $("#select-image").click(function(){
        var imageID = (globalRequest == "insertProduct")? $("#image-picker-avatar").val() : $("#image-picker").val();


        alert("Image set!");
        if(globalRequest == "insertProduct"){
            $('#imageNameAvatar').val(imageID);
            $('#server-images-avatar').modal('hide');
        } else{
            $('#imageName').val(imageID);
            $('#server-images').modal('hide');
        }

        $("#imageName").val("");
        var path = "http://localhost/webprog/Ass2/upload/"+imageID+".png";

        var newImage = "<div class='masonry-gallery'><div class='masonry-thumb'>" +
            "<img id='newsImage' class='grayscale' src='"+path+"' alt='images'>" +
            "</div></div>";
        if(globalRequest == "insertProduct"){
            $("#imageDisplayAvatar").html("");
            $("#imageDisplayAvatar").append(newImage);
        } else{
            $("#imageDisplay").html("");
            $("#imageDisplay").append(newImage);
        }
    });

    // select MULTIPLE image and set the values to the select tag
    $("#select-image-multi").click(function(){
        var imageIDs = $('select#image-picker-multi').val();
        console.log(imageIDs);
        $("#select-image").val(imageIDs);
        alert("Images set!");

        $('#server-images').modal('hide');
        $("#imageDisplay").html("");
        $.each(imageIDs,function(key,value){

            imageArr.push(value);

            var path = "http://localhost/webprog/Ass2/upload/"+value+".png";

            var newImage = "<div class='masonry-gallery'><div class='masonry-thumb'>" +
                "<img id='newsImage' class='grayscale' src='"+path+"' alt='images'>" +
                "</div></div>";

            $("#imageDisplay").append(newImage);
        });
    });

    // LOGIN
    $("#loginBtn").click(function(){
        var username = $("#username").val();
        var pwd = $("#password").val();
        alert("pass = " + pwd);
        $.ajax({
            type: "POST",
            url: phpController,
            data: "request=login&username="+username+"&password="+pwd,
            dataType: "text",
            cache: false,
            success: function(result){
                if(result == "successful"){
                    alert("Login successful");
                    window.href = "index.php";
                } else{
                    alert("Failed : "+ result);
                }
            },
            error: function(xhr){
                var err = eval("(" + xhr.responseText +")");
                console.log(err.Message);
            }
        });
    });

    // USER
    // get user list
    if(pathname == "userPage.php"){
        $.ajax({
            type: "POST",
            data: "request=viewUsers",
            url: phpController,
            cache: false,
            dataType: "json",
            success:function(users){
                $("#users").html("");
                $.each(users,function (key,value) {
                    var status = (value.Status == "1")?"Active":"Disabled";

                    $("#users").append(
                        "<tr id='"+ value.id+"'>" +
                            "<td class='center ID'>" + value.id + "</td>" +
                            "<td class='center FirstName'>" + value.FirstName + "</td>" +
                            "<td class='center LastName'>" + value.LastName + "</td>" +
                            "<td class='center Username'>" + value.UserName + "</td>" +
                            "<td class='center Email'>" + value.Email + "</td>" +
                            "<td class='center Phone'>" + value.Phone + "</td>" +
                            "<td class='center Password'>" + value.Password + "</td>" +
                            "<td class='center AvatarID'>" + value.Avatar_ID + "</td>" +
                            "<td class='center Address'>" + value.Address + "</td>" +
                            "<td class='center RegisterDate'>" + value.RegisterDate + "</td>" +
                            "<td class='center BirthDay'>" + value.BirthDay + "</td>" +
                            "<td class='center Status'>" + status + "</td>" +
                            "<td class='center' colspan='3'><button class='enableUser btn btn-success' style='margin-right: 20px;'>Enable</button><button class='disableUser btn btn-danger'>Disable</button></td>" +
                        "</tr>"
                    );
                });
            },
            error:function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    }

    // disable user
    $('#users').on("click",".disableUser",function () {
        if(confirm("Do you wish to disable this user?")){
            var tr = $(this).closest("tr");
            var id = tr.attr("id");
            $.ajax({
                type: "POST",
                url: phpController,
                data: "request=disableUser&id="+id,
                dataType: "text",
                cache: false,
                success:function(result){
                    if(result == "successful"){
                        tr.find(".Status").html("Disabled");
                        alert("This user has been disabled!");
                    }else{
                        alert("Failed to disable this user: " + result );
                    }
                },
                error: function (xhr) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // enable user
    $("#users").on("click",".enableUser",function () {
       if(confirm("Do you wish to enable this user?")){
           var tr = $(this).closest("tr");
           var id = tr.attr("id");
           $.ajax({
               type: "POST",
               url: phpController,
               data: "request=enableUser&id="+id,
               dataType: "text",
               cache: false,
               success: function(result){
                    if(result == "successful"){
                        tr.find(".Status").html("Active");
                        alert("User enabled!");
                    } else{
                        alert("Enabled user failed: "+result);
                    }
               },
               error: function(xhr){
                   var err = eval("(" + xhr.responseText +")");
                   console.log(err.Message);
               }
           });
       }
    });

    // ADMIN
    // get admin list
    if(pathname == "adminPage.php"){
        $.ajax({
            url: phpController,
            type: "POST",
            data: "request=viewAdmins",
            dataType: "json",
            cache: false,
            success: function(admins){
                $("#users").html("");
                $.each(admins,function(key,value){
                    var status = (value.Status == "1")?"Active":"Disabled";

                    $("#users").append(
                        "<tr id='"+ value.id+"'>" +
                        "<td class='center AdminID'>" + value.Admin_ID + "</td>" +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center FirstName'>" + value.FirstName + "</td>" +
                        "<td class='center LastName'>" + value.LastName + "</td>" +
                        "<td class='center Username'>" + value.UserName + "</td>" +
                        "<td class='center Email'>" + value.Email + "</td>" +
                        "<td class='center Phone'>" + value.Phone + "</td>" +
                        "<td class='center Password'>" + value.Password + "</td>" +
                        "<td class='center AvatarID'>" + value.Avatar_ID + "</td>" +
                        "<td class='center Address'>" + value.Address + "</td>" +
                        "<td class='center RegisterDate'>" + value.RegisterDate + "</td>" +
                        "<td class='center BirthDay'>" + value.BirthDay + "</td>" +
                        "<td class='center Status'>" + status + "</td>" +
                        "<td class='center' colspan='3'><button class='enableUser btn btn-success' style='margin-right: 20px;'>Enable</button><button class='disableUser btn btn-danger'>Disable</button></td>" +
                        "</tr>"
                    );
                });
            }
        });
    }

    // ADDRESS
    // get address list
    if(pathname == "addressPage.php"){
        $.ajax({
            type:"POST",
            url: phpController,
            data: "request=viewAddress",
            dataType: "json",
            cache: false,
            success: function(address){
                $("#address").html("");
                $.each(address,function(key,value){
                    var status = (value.Status == "1")?"Active":"Disabled";
                    $("#address").append(
                        "<tr id='"+ value.id+"'>" +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center HouseNumber' contenteditable='true'>" + value.HouseNumber + "</td>" +
                        "<td class='center Street' contenteditable='true'>" + value.Streets + "</td>" +
                        "<td class='center Ward' contenteditable='true'>" + value.Ward + "</td>" +
                        "<td class='center District' contenteditable='true'>" + value.District + "</td>" +
                        "<td class='center City'>" + value.City + "</td>" +
                        "<td class='center AddressFull'>" + value.AdressFull + "</td>" +
                        "<td class='center Status'>" + status + "</td>" +
                        "<td class='center' colspan='4'>" +
                            "<button class='enableAddress btn btn-success' style='margin-right: 10px;'>Enable</button>" +
                            "<button class='disableAddress btn btn-danger' style='margin-right: 10px;'>Disable</button>" +
                            "<button class='editAddress btn btn-primary'>Save</button></td>" +
                        "</tr>"
                    );
                });
            },
            error: function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    }

    // disable address
    $("#address").on("click",".disableAddress",function () {
        if(confirm("Do you wish to disable this address?")) {
            var tr = $(this).closest("tr");
            var id = tr.attr("id");
            $.ajax({
                type: "POST",
                url: phpController,
                data: "request=disableAddress&id=" + id,
                dataType: "text",
                cache: false,
                success: function (result) {
                    if (result == "successful") {
                        tr.find(".Status").html("Disabled");
                        alert("This address has been disabled!");
                    } else {
                        alert("Failed to disable this address: " + result);
                    }
                },
                error: function (xhr) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // enable address
    $("#address").on("click",".enableAddress",function () {
        if(confirm("Do you wish to enable this address?")) {
            var tr = $(this).closest("tr");
            var id = tr.attr("id");
            $.ajax({
                type: "POST",
                url: phpController,
                data: "request=enableAddress&id=" + id,
                dataType: "text",
                cache: false,
                success: function (result) {
                    if (result == "successful") {
                        tr.find(".Status").html("Active");
                        alert("Address enabled!");
                    } else {
                        alert("Failed to enable this address: " + result);
                    }
                },
                error: function (xhr) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // insert address
    $("#insertAddress").click(function () {
        if(confirm("Do you wish to insert this address?")){
            var HouseNumber = $("#insertHouseNumber").val();
            var Street = $("#insertStreet").val();
            var Ward = $("#insertWard").val();
            var District = $("#insertDistrict").val();

            var data = {
                request : "insertAddress",
                HouseNumber : HouseNumber,
                Street : Street,
                Ward : Ward,
                District : District
            };

            $.ajax({
                url: phpController,
                type: "POST",
                cache: false,
                data: { myData: data},
                dataType: "text",
                success: function(result){
                    if(result == "successful"){
                        alert("Insert Successful");
                        window.location = "addressPage.php";
                    }else{
                        alert("Insert failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // edit address
    $("#address").on("click",".editAddress",function () {
        if(confirm("Do you wish to update this address?")){
            var tr = $(this).closest("tr");
            var id = tr.attr("id");

            var HouseNumber = tr.find(".HouseNumber").html();
            var Street = tr.find(".Street").html();
            var Ward = tr.find(".Ward").html();
            var District = tr.find(".District").html();

            var AddressFull = HouseNumber + " Đường "+Street+", Phường "+Ward+", Quận "+District+", TP.HCM";

            var data = {
                request : "updateAddress",
                id: id,
                HouseNumber : HouseNumber,
                Street : Street,
                Ward : Ward,
                District : District,
                AddressFull : AddressFull
            };

            $.ajax({
                url: phpController,
                type: "POST",
                cache: false,
                data: { myData: data},
                dataType: "text",
                success: function(result){
                    if(result == "successful"){
                        tr.find(".HouseNumber").html(HouseNumber);
                        tr.find(".Street").html(Street);
                        tr.find(".Ward").html(Ward);
                        tr.find(".District").html(District);
                        tr.find(".AddressFull").html(AddressFull);
                        alert("Update Successful");
                    }else{
                        alert("Update failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // VIDEOS
    // get video list
    if(pathname == "videoPage.php"){
        $.ajax({
            url: phpController,
            type: "POST",
            data: "request=viewVideos",
            cache: false,
            dataType: "json",
            success: function(videos){
                $("#videos").html("");
                $.each(videos,function(key,value){
                    $("#videos").append(
                        "<tr id='"+ value.id+"'>" +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center Link' contenteditable='true'>" + value.Link + "</td>" +
                        "<td class='center Name' contenteditable='true'>" + value.Name + "</td>" +
                        "<td class='center ProductID' contenteditable='true'>" + value.Product_ID + "</td>" +
                        "<td class='center ProductName'>" + value.ProductName + "</td>" +
                        "<td class='center'>" +
                            "<button class='editVideo btn btn-primary'>Save</button></td>" +
                        "</tr>"
                    );
                });
            },
            error: function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    }

    // insert video
    $("#insertVideo").click(function(){
        if(confirm("Do you wish to insert this record?")){
            var link = $("#insertLink").val();
            var name = $("#insertName").val();
            var productID = $("#getProductList").val();
            var data = {
                request : "insertVideo",
                link: link,
                name: name,
                productID: productID
            };
            $.ajax({
                type: "POST",
                url: phpController,
                data: {myData: data},
                dataType: "text",
                cache: false,
                success: function(result){
                    if(result == "successful"){
                        alert("Insert successful");
                        window.location = "videoPage.php";
                    } else{
                        alert("Insert failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                }
            });
        }
    });

    // update video
    $("#videos").on("click",".editVideo",function(){
        if(confirm("Do you wish to update this record?")){
            var tr = $(this).closest("tr");
            var id = tr.attr("id");
            var name = tr.find(".Name").html();
            var link = tr.find(".Link").html();
            var productID = tr.find(".ProductID").html();

            var data = {
                request : "updateVideo",
                id : id,
                link: link,
                name: name,
                productID: productID
            };
            $.ajax({
                type: "POST",
                url: phpController,
                dataType : "json",
                data: {myData: data},
                success: function(resultdata){
                    if(resultdata["result"] == "successful"){
                        alert("Update successful");
                        tr.find(".ProductName").html(resultdata["productName"]);
                    } else{
                        alert("Update failed: "+resultdata["productName"]);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // COMMENTS
    // get comment list
    if(pathname == "commentPage.php"){
        $.ajax({
            type: "POST",
            url: phpController,
            data: "request=viewComments",
            dataType: "json",
            cache: false,
            success: function(comments){
                console.log(comments);
                $("#comments").html("");
                $.each(comments,function(key,value){
                    var status = (value.Status == 1)? "Active": "Disabled";
                    $("#comments").append(
                        "<tr id='"+ value.id+"'>" +
                        "<td class='center UserID'>" + value.id + "</td>" +
                        "<td class='center Content'>" + value.Contents + "</td>" +
                        "<td class='center ProductID'>" + value.Products + "</td>" +
                        "<td class='center NewsID'>" + value.News_ID + "</td>" +
                        "<td class='center Name'>" + value.Name + "</td>" +
                        "<td class='center Email'>" + value.Email + "</td>" +
                        "<td class='center Status'>" + status + "</td>" +
                        "<td class='center' colspan='3'><button class='enableComment btn btn-success' style='margin-right: 20px;'>Enable</button><button class='disableComment btn btn-danger'>Disable</button></td>" +
                        "</tr>"
                    );
                });
            },
            error: function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    }

    // enable comment
    $("#comments").on("click",".enableComment",function(){
        if(confirm("Do you wish to enable this comment?")){
            var tr = $(this).closest("tr");
            var id = tr.attr("id");
            $.ajax({
                url: phpController,
                type: "POST",
                data: "request=enableComment&id="+id,
                dataType: "text",
                cache: false,
                success: function(result){
                    if(result == "successful"){
                        alert("This comment has been enabled!");
                        tr.find(".Status").html("Active");
                    }else{
                        alert("Enable failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // disasble comment
    $("#comments").on("click",".disableComment",function(){
        if(confirm("Do you wish to disable this comment?")){
            var tr = $(this).closest("tr");
            var id = tr.attr("id");
            $.ajax({
                url: phpController,
                type: "POST",
                data: "request=disableComment&id="+id,
                dataType: "text",
                cache: false,
                success: function(result){
                    if(result == "successful"){
                        alert("This comment has been disabled!");
                        tr.find(".Status").html("Disabled");
                    }else{
                        alert("Disable failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // RATE
    // get rate list
    if(pathname == "ratePage.php"){
        $.ajax({
            url: phpController,
            type: "POST",
            cache: false,
            data: "request=viewRates",
            dataType: "json",
            success: function(rates){
                $("#rates").html("");
                $.each(rates,function(key,value){
                    $("#rates").append(
                        "<tr id='"+ value.id+"'>" +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center ProductID'>" + value.Product_ID + "</td>" +
                        "<td class='center UserID'>" + value.User_ID + "</td>" +
                        "<td class='center AddressRate'>" + value.Address_Rate + "</td>" +
                        "<td class='center QualityRate'>" + value.Quanlity_Rate + "</td>" +
                        "<td class='center PriceRate'>" + value.Price_Rate + "</td>" +
                        "<td class='center ServiceRate'>" + value.Service_Rate + "</td>" +
                        "<td class='center SpaceRate'>" + value.Space_Rate + "</td>" +
                        "</tr>"
                    );
                });
            },
            error: function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    }

    // RESTAURANT
    // get restaurant list
    if(pathname == "restaurantPage.php"){
        $.ajax({
            type: 'POST',
            url: phpController,
            data: 'request=viewRestaurants',
            cache: false,
            success: function(restaurants){
                $('#restaurants').html("");

                $.each(restaurants,function(key,value){
                    var status = (value.Status == "1")?"Active":"Disabled";
                    $('#restaurants').append('<tr id=' + value.id + '>' +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center Name' contenteditable='true'>" + value.Name + "</td>" +
                        "<td class='center Service' contenteditable='true'>" + value.Service + "</td>" +
                        "<td class='center Status'>" + status + "</td>" +
                        "<td class='center' colspan='2'><button class='updateRestaurant btn btn-success' >Save</button> <button class='enableRestaurant btn btn-primary'>Enable</button> <button class='disableRestaurant btn btn-danger'>Disable</button></td>" +
                        '</tr>');
                });
            },
            error:function(xhr, status, error){
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
    }

    // disable restaurant
    $("#restaurants").on("click",".disableRestaurant",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        if(confirm("Do you wish to disable this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: "request=disableRestaurant&id="+id,
                dataType: "text",
                cache: false,
                error: function(xhr, status, error){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                },
                success: function(result){
                    if(result == "successful"){
                        alert('Record disabled!');
                        tr.find(".Status").html("Disabled");
                    } else {
                        alert("Disabled failed: "+result);
                    }
                }
            });
        }
    });

    // enable restaurant
    $("#restaurants").on("click",".enableRestaurant",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        if(confirm("Do you wish to disable this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: "request=enableRestaurant&id="+id,
                dataType: "text",
                cache: false,
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                },
                success: function(result){
                    if(result == "successful"){
                        alert('Record enabled!');
                        tr.find(".Status").html("Active");
                    } else {
                        alert("Enabled failed: "+result);
                    }
                }
            });
        }
    });

    // update restaurant
    $('#restaurants').on("click",".updateRestaurant",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        // Update values
        var Name = tr.find(".Name").html();
        var Service= tr.find(".Service").html();

        var data = {
            request: "updateRestaurant",
            id: id,
            Name: Name,
            Service: Service
        };

        if(confirm("Do you wish to update this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: {myData: data},
                dataType: "text",
                success: function(result){
                    if(result == "successful"){
                        alert("Update successful");
                        tr.find(".Name").html(Name);
                        tr.find(".Service").html(Service);
                    } else{
                        alert("Failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // insert restaurant
    $('#insertRestaurant').click(function () {
        var Name = $('#insertName').val();
        var Service = $('#insertService').val();

        var data = {
            request: "insertRestaurant",
            Name: Name,
            Service : Service
        };

        $.ajax({
            type: "POST",
            url: phpController,
            dataType: "text",
            data: {myData: data},
            cache: false,
            success:function(result){
                console.log(result);
                if(result == "successful"){
                    alert("Record successfully inserted!");
                    window.location = "restaurantPage.php";
                }else{
                    alert("Failed:" + result);
                }
            },
            error:function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    });

    // insert news
    $('#insertNews').click(function () {
        alert("running");
        var Name = $('#insertName').val();
        var ShortDes = $('#insertShortDes').val();
        var Des = $('#insertDes').val();
        var UserID = $('#insertUserID').val();
        var ImageID = ($("#imageName").val() != "")?$("#imageName").val():$("#image-picker").val();
        console.log(ImageID);
        if(ImageID == ""){
            alert("Please select an image");
            return false;
        }
        var Author = $('#insertAuthor').val();
        var CommentID = $('#insertCommentID').val();
        var MostView = $('#insertMostView').val();

        var data = {
            request: "insertNews",
            Name: Name,
            ShortDes : ShortDes,
            Des : Des,
            UserID : UserID,
            ImageID : ImageID,
            Author : Author,
            CommentID : CommentID,
            MostView : MostView
        };

        console.log(data);

        $.ajax({
            type: "POST",
            url: phpController,
            dataType: "text",
            data: {myData: data},
            cache: false,
            success:function(result){
                console.log(result);
                if(result == "successful"){
                    alert("Record successfully inserted!");
                    window.location = "newsPage.php";
                }else{
                    alert("Failed:" + result);
                }
            },
            error:function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
        
    });

    // insert product
    $('#insertProduct').click(function () {
        var Name = $('#insertName').val();
        var AddressID = $("#getAddressList").val();
        var RestaurantID = $("#getRestaurantList").val();
        var Category = $('#insertCategory').val();
        var ShortDes = $('#insertShortDes').val();
        var Des = $('#insertDes').val();
        var Price = $('#insertPrice').val();
        var Hotline = $('#insertHotline').val();
        var OpenTime = $('#insertOpenTime').val();
        var MostView = $('#insertMostView').val();
        var Avatar = $("#imageNameAvatar").val();

        var data = {
            request: "insertProduct",
            AddressID : AddressID,
            RestaurantID : RestaurantID,
            Category : Category,
            Name: Name,
            ShortDes: ShortDes,
            Des: Des,
            Price: Price,
            Hotline: Hotline,
            OpenTime: OpenTime,
            MostView: MostView,
            Avatar: Avatar,
            Images : imageArr
        };

        console.log(data);

        $.ajax({
            type: "POST",
            url: phpController,
            dataType: "text",
            data: {myData: data},
            cache: false,
            success:function(result){
                console.log(result);
                if(result == "successful"){
                    alert("Record successfully inserted!");
                    window.location = "productPage.php";
                }else{
                    alert("Failed:" + result);
                }
            },
            error:function(xhr,status,error){
                console.log(status);
                console.log(xhr.responseText);
            }
        });
    });
});



