<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Bootstrap Metro Dashboard by Dennis Ji for ARM demo</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<link href="css/style-custom.css" rel="stylesheet">
    <link href="css/image-picker.css" rel="stylesheet">
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
</head>

<body>
	<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.html"><span>Metro</span></a>

				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> Vũ Anh
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
 									<span>Account Settings</span>
								</li>
								<li><a href="#"><i class="halflings-icon user"></i> Profile</a></li>
								<li><a href="login.html"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->

			</div>
		</div>
	</div>
	<!-- start: Header -->
	
	<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li><a href="index.html"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>
						<li><a href="users.php"><i class="icon-align-justify"></i><span class="hidden-tablet"> Users</span></a></li>
						<li><a href="productList.php"><i class="icon-align-justify"></i><span class="hidden-tablet"> Products</span></a></li>
						<li><a href="icon.html"><i class="icon-star"></i><span class="hidden-tablet"> Images</span></a></li>
						<li><a href="login.html"><i class="icon-lock"></i><span class="hidden-tablet"> Login Page</span></a></li>
					</ul>
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">

			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.html">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Tables</a></li>
			</ul>

			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Insert new Record</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form class="form-horizontal">
							<fieldset>
								<?php
									if($_GET['request'] == "insertProduct") {
										?>
										<div class="control-group">
											<label class="control-label" for="insertName">Name</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertName" type="text">
											</div>
										</div>
                                        <!-- Address -->
                                        <div class="control-group">
                                            <label class="control-label" for="getAddressList">Address</label>
                                            <div class="controls">
                                                <select id="getAddressList" data-rel="chosen">
                                                    <?php
                                                        require "admin_model/addressModel.php";
                                                        $addressModel = new addressModel();
                                                        $addressList = $addressModel->getAddress();
                                                        $addressModel->close();
                                                        $temp = json_decode($addressList,true);
                                                        foreach($temp as $address){
                                                            $id = $address["id"];
                                                            $AddressFull = $address["AdressFull"];
                                                            ?>
                                                            <option value='<?php echo $id; ?>'><?php echo $AddressFull; ?></option>
                                                            <?php
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- Restaurant -->
                                        <div class="control-group">
                                            <label class="control-label" for="getRestaurantList">Restaurant</label>
                                            <div class="controls">
                                                <select id="getRestaurantList" data-rel="chosen">
                                                    <?php
                                                    require "admin_model/restaurantModel.php";
                                                    $restaurantModel = new restaurantModel();
                                                    $restaurantList = $restaurantModel->getRestaurants();
                                                    $restaurantModel->close();
                                                    $temp = json_decode($restaurantList,true);
                                                    foreach($temp as $restaurant){
                                                        $id = $restaurant["id"];
                                                        $name = $restaurant["Name"];
                                                        ?>
                                                        <option value='<?php echo $id; ?>'><?php echo $name; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
										<div class="control-group">
											<label class="control-label" for="insertCategory">Category</label>
											<div class="controls">
												<select id="insertCategory" data-rel="chosen">
													<option value="Ăn gì?">Ăn gì</option>
													<option value="Ở đâu?">Ở đâu</option>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertShortDes">Short
												Description</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertShortDes"
													   type="text">
											</div>
										</div>
										<div class="control-group hidden-phone">
											<label class="control-label" for="insertDes">Description</label>
											<div class="controls">
												<textarea class="cleditor" id="insertDes" rows="3"></textarea>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertPrice">Price</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertPrice"
													   type="number">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertHotline">Hotline</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertHotline"
													   type="text">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertOpenTime">Open Time</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertOpenTime"
													   type="text">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertStatus">Status</label>
											<div class="controls">
												<select id="insertStatus" data-rel="chosen">
													<option value="Active">Active</option>
													<option value="Disabled">Disabled</option>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertMostView">Most View</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertMostView"
													   type="number">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Avatar</label>
											<div class="controls">
												<input type="file" id="imageInputAvatar" style="margin-right: 20px;">
												<input type="text" id="imageNameAvatar" style="display: none" />
												<button type="button" id="uploadImageAvatar" class="btn btn-info">Upload Image</button>
												<button type="button" id="getImageAvatar" class="btn btn-info select-images-avatar">Select From Server</button>
											</div>
										</div>
										<div class="control-group" id="imageDisplayAvatar" style="text-align: center">
										</div>
										<div class="control-group">
											<label class="control-label">Images</label>
											<div class="controls">
												<input type="file" id="imageInput" style="margin-right: 20px;">
												<input type="text" id="imageName" style="display: none" />
												<button type="button" id="uploadImage" class="btn btn-info">Upload Image</button>
												<button type="button" id="getImage" class="btn btn-info select-images">Select From Server</button>
											</div>
										</div>
										<div class="control-group" id="imageDisplay" style="text-align: center">
										</div>

										<div class="form-actions">

											<button type="button" id="insertProduct" class="btn btn-primary">Insert Record</button>
											<button type="reset" class="btn btn-success">Cancel</button>
										</div>
										<?php
									}
								?>
								<?php
								    if($_GET['request'] == "insertAddress") {
									?>
										<div class="control-group">
											<label class="control-label" for="insertHouseNumber">HouseNumber</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertHouseNumber" type="text">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertHouseNumber">Street</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertStreet" type="text">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertWard">Ward</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertWard" type="text">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertDistrict">District</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertDistrict" type="text">
											</div>
										</div>
										<div class="form-actions">
											<button type="button" id="insertAddress" class="btn btn-primary">Insert Record</button>
											<button type="reset" class="btn btn-success">Reset</button>
										</div>
									<?php
								}
								?>
                                <?php
                                    if($_GET['request'] == "insertVideo") {
                                    ?>
                                    <div class="control-group">
                                        <label class="control-label" for="insertLink">Link</label>
                                        <div class="controls">
                                            <input class="input-xlarge focused" id="insertLink" type="text">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="insertName">Name</label>
                                        <div class="controls">
                                            <input class="input-xlarge focused" id="insertName" type="text">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="getProductList">Product</label>
                                        <div class="controls">
                                            <select id="getProductList" data-rel="chosen">
                                                <?php
                                                    require "admin_model/productModel.php";
                                                    $productModel = new productModel();
                                                    $productList = $productModel->getProductList();
                                                    $productModel->close();
                                                    $temp = json_decode($productList,true);
                                                    foreach($temp as $product){
                                                        $id = $product["id"];
                                                        $name = $product["Name"];
                                                ?>
                                                        <option value='<?php echo $id; ?>'><?php echo $name; ?></option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="button" id="insertVideo" class="btn btn-primary">Insert</button>
                                        <button type="reset" class="btn btn-success">Reset</button>
                                    </div>
                                    <?php
                                }
                                ?>
								<?php
									if($_GET['request'] == "insertRestaurant"){
									?>
										<div class="control-group">
											<label class="control-label" for="insertName">Name</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertName" type="text">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="insertService">Service</label>
											<div class="controls">
												<input class="input-xlarge focused" id="insertService" type="number">
											</div>
										</div>
										<div class="form-actions">
											<button type="button" id="insertRestaurant" class="btn btn-primary">Insert</button>
											<button type="reset" class="btn btn-success">Reset</button>
										</div>
									<?php
									}
								?>
								<?php
									if($_GET['request'] == "insertNews") {
									?>
									<div class="control-group">
										<label class="control-label" for="insertName">Name</label>
										<div class="controls">
											<input class="input-xlarge focused" id="insertName" type="text">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="insertShortDes">Short
											Description</label>
										<div class="controls">
											<input class="input-xlarge focused" id="insertShortDes"
												   type="text">
										</div>
									</div>
									<div class="control-group hidden-phone">
										<label class="control-label" for="insertDes">Description</label>
										<div class="controls">
											<textarea class="cleditor" id="insertDes" rows="3"></textarea>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="insertUserID">User ID</label>
										<div class="controls">
											<input class="input-xlarge focused" id="insertUserID"
												   type="number">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Images</label>
										<div class="controls">
											<input type="file" id="imageInput" style="margin-right: 20px;">
											<input type="text" id="imageName" style="display: none" />
											<button type="button" id="uploadImage" class="btn btn-info">Upload Image</button>
											<button type="button" id="getImage" class="btn btn-info select-images">Select From Server</button>

										</div>
									</div>
									<div class="control-group" id="imageDisplay" style="text-align: center">
									</div>
									<div class="control-group">
										<label class="control-label" for="insertAuthor">Author</label>
										<div class="controls">
											<input class="input-xlarge focused" id="insertAuthor"
												   type="text">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="insertCommentID">CommentID</label>
										<div class="controls">
											<input class="input-xlarge focused" id="insertCommentID"
												   type="number">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="insertMostView">Most View</label>
										<div class="controls">
											<input class="input-xlarge focused" id="insertMostView"
												   type="number">
										</div>
									</div>
									<div class="form-actions">

										<button type="button" id="insertNews" class="btn btn-primary">Insert</button>
										<button type="reset" class="btn btn-success">Reset</button>
									</div>
									<?php
								}
								?>
							</fieldset>
						</form>
					</div>
				</div><!--/span-->

			</div><!--/row-->

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->

    <div class="modal hide fade" id="server-images">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h3>Settings</h3>
        </div>
        <div class="modal-body">
            <?php if($_GET['request'] == "insertNews"){ ?>
            <select id="image-picker" class="image-picker show-html">
                <option value=""></option>
                <?php
                    require "admin_model/imageModel.php";
                    $imageModel = new imageModel();
                    $images = $imageModel->getImages();
                    $imageModel->close();
                    $temp = json_decode($images,true);

                    $path = "http://localhost/webprog/Ass2/upload/";

                    foreach($temp as $image){
                        $id = $image["id"];
                        $imageName = $image["Name"];
                        $imagePath = $path.$imageName;
                        //<?php echo $imagePath
                        ?>
                        <option data-img-src="<?php echo $imagePath ?>" value="<?php echo $id; ?>"><?php echo $imageName ?></option>
                        <?php
                    }
                    ?>
            </select>
            <?php }else{?>
            <select id="image-picker-multi" multiple="multiple" class="image-picker show-html">
                <option value=""></option>
                <?php
                require "admin_model/imageModel.php";
                $imageModel = new imageModel();
                $images = $imageModel->getImages();
                $imageModel->close();
                $temp = json_decode($images,true);

                $path = "http://localhost/webprog/Ass2/upload/";

                foreach($temp as $image){
                    $id = $image["id"];
                    $imageName = $image["Name"];
                    $imagePath = $path.$imageName;
                    //<?php echo $imagePath
                    ?>
                    <option data-img-src="<?php echo $imagePath ?>" value="<?php echo $id; ?>"><?php echo $imageName ?></option>
                    <?php
                }
                ?>
            </select>
            <?php } ?>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">Close</a>
			<?php if($_GET['request'] == "insertNews"){ ?>
				<a href="#" id="select-image" class="btn btn-primary">Select</a>
			<?php }else{?>
				<a href="#" id="select-image-multi" class="btn btn-primary">Select</a>
			<?php }?>

        </div>
    </div>

	<div class="modal hide fade" id="server-images-avatar">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">

			<select id="image-picker-avatar" multiple="multiple" class="image-picker show-html">
				<option value=""></option>
				<?php
				$imageModel = new imageModel();
				$images = $imageModel->getImages();
				$imageModel->close();
				$temp = json_decode($images,true);

				$path = "http://localhost/webprog/Ass2/upload/";

				foreach($temp as $image){
					$id = $image["id"];
					$imageName = $image["Name"];
					$imagePath = $path.$imageName;
					//<?php echo $imagePath
					?>
					<option data-img-src="<?php echo $imagePath ?>" value="<?php echo $id; ?>"><?php echo $imageName ?></option>
					<?php
				}
				?>
			</select>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
            <a href="#" id="select-image" class="btn btn-primary">Select</a>
		</div>
	</div>

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Select</a>
		</div>
	</div>
    <div class="clearfix"></div>


	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2013 <a href="http://jiji262.github.io/Bootstrap_Metro_Dashboard/" alt="Bootstrap_Metro_Dashboard">Bootstrap Metro Dashboard</a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/custom.js"></script>

		<script type="text/javascript" src="ajax/ajax_handler.js"></script>

        <script src="js/image-picker.js"></script>

        <script src="js/image-picker.min.js"></script>


	<!-- end: JavaScript-->
	
</body>
</html>
