<!DOCTYPE html>
<html lang="en">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<title> Exercise 2 Lab 5 </title>
</head>
<body>
	
<?php
	function Exercise2($input){
		switch ($input%5){
			case 0:
				return "Hello"; break;
			case 1:
				return "How are you"; break;
			case 2:
				return "I'm doing well thank you"; break;
			case 3:
				return "See you later"; break;
			case 4:
				return "Good bye"; break;
			default: return 0; break;
		}
	}
	
	if($_SERVER['REQUEST_METHOD'] == 'POST') {
		$input = $_POST['value'];
		$output = Exercise2($input);
	}
	else $output="";
?>
	<form method="post">
		Your value : <input type="text" name="value">
		<input type="submit" value="Submit">
		<input type="text" value="<?php echo $output ?>">
	</form>
</body>
</html>