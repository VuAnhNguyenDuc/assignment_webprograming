<!DOCTYPE html>
<html lang="en">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<title> Exercise 3 Lab 5 </title>
</head>
<body>
<?php
	function Exercise3(){
		echo "For loop: ";
		for($i = 1; $i<= 100; $i++){
			if($i%2!=0){
				echo " $i";
			}	
		}
		echo "<br><br>";
		
		echo "While loop: ";
		for($j = 1; $j <= 100; $j++){
			if($j%2!=0){
				echo " $j";
			}	
		}
	}
	Exercise3();
?>
</body>
</html>