<!DOCTYPE html>
<html lang="en">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<title> Exercise 5 Lab 5 </title>
</head>
<body>
<?php
// Defining the "calc" class 
class calc { 
    var $n1; 
    var $n2; 
	
	function add($n1,$n2){
		$result = $n1 + $n2;
		return $result;
		exit;
	}
	
	function subtract($n1,$n2){
		$result = $n1 - $n2;
		return $result;
		exit;
	}
	
	function multiply($n1,$n2){
		$result = $n1 * $n2;
		return $result;
		exit;
	}
	
	function divide($n1,$n2){
		$result = $n1 / $n2;
		return $result;
		exit;
	}
	
	function inverse($n1){
		$result = 1/$n1;
		return $result;
		exit;
	}
	
	function exponent($n1,$n2){
		$result = 1;
		for($i = 1; $i <= $n2; $i++){
			$result = $result * $n1; 
		}
		return $result;
		exit;
	}
	
} 
if($_SERVER['REQUEST_METHOD'] != 'POST') {
	$result = 0;
?>
<?php 
	}
	else{
		$calc = new calc(); 
		$n1 = $_POST['number1'];
		$n2 = $_POST['number2'];
		$oper = $_POST['operations'];
		
		if(!$n1){ echo "Please enter number 1"; exit;}
		if(!$n2 and $oper != '1/'){ echo "Please enter number 2"; exit;}
		if($oper == "1/" and $n2){ echo "Only number 1 is needed in inverse operation"; exit;}
		if(!$oper){ echo "Please enter an operation"; exit;}
		
		if(!is_numeric($n1)){ echo "Number 1 is not a number"; exit;}
		if($oper != "1/"){
			if(!is_numeric($n2)){ echo "Number 2 is not a number"; exit;}
		}
				
		if($oper == '+'){
			$result = $calc->add($n1,$n2);
		}
		if($oper == '-'){
			$result = $calc->subtract($n1,$n2);
		}
		if($oper == '*'){
			$result = $calc->multiply($n1,$n2);
		}
		if($oper == '/'){
			$result = $calc->divide($n1,$n2);
		}
		if($oper == '1/'){
			$result = $calc->inverse($n1);
		}
		if($oper == '^'){
			$result = $calc->exponent($n1,$n2);
		}
	}
?>
	<form method="post">
		<table>
			Number 1: <input type="text" size="20" name="number1"><br>
			Number 2: <input type="text" size="20" name="number2"><br>
			Operation: 
			<select name="operations">
				<option value="+">Addition</option>
				<option value="-">Substraction</option>
				<option value="*">Multiplication</option>
				<option value="/">Division</option>
				<option value="1/">Inverse</option>
				<option value="^">Exponent</option>
			</select>
			<input type="submit" value="Calculate" ></input><br>	
			Result: <input type="text" value="<?php echo $result ?>" style="text-align: right;"></input>
	</form>
</body>
</html>