<!DOCTYPE html>
<html lang="en">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<title> Exercise 6 Lab 5 </title>
</head>
<body>
<?php
	if($_SERVER['REQUEST_METHOD'] != 'POST') {
?>
	<form method="post">
		<table>
			<tr>
				<td>First name: </td>
				<td><input type="text" name="firstname"></td>
			</tr>
			<tr>
				<td>Last name: </td>
				<td><input type="text" name="lastname"></td>
			</tr>
			<tr>
				<td>Email: </td>
				<td><input type="email" name="email"></td>
			</tr>
			<tr>
				<td>Password: </td>
				<td><input type="password" name="password"></td>
			</tr>
			<tr>
				<td>Birthday: </td>
				<td><input type="date" name="date"></td>
			</tr>
			<tr>
				<td>Gender: </td>
				<td><input type="radio" value="Man">Man <input type="radio" value="Woman">Woman</td>
			</tr>
			<tr>
				<td>Country: </td>
				<td>
					<select name="countries">
						<option value="vn">Viet Nam</option>
						<option value="au">Australia</option>
						<option value="us">United States</option>
						<option value="id">India</option>
						<option value="other/">Other</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>About: </td>
				<td><textarea name="abouttextarea" rows="10" cols="50"></textarea></td>
			</tr>
			<tr>
				<td><input type = "reset" value = "Reset"></input></td>
				<td><input type = "submit" value = "Submit"></input></td>
			</tr>
		</table>	
	</form>
<?php 
	}
	else{
		$fname = $_POST['firstname'];
		$lname = $_POST['lastname'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$about = $_POST['abouttextarea'];
		
		if(strlen($fname) < 2 or strlen($fname) > 30){
			echo "Firstname must be 2-30 characters long!";
			exit;
		} 
		if(strlen($lname) < 2 or strlen($fname) > 30){
			echo "Lastname must be 2-30 characters long!";
			exit;
		} 
		///^[A-Za-z0-9_\.]{6,32}@([a-zA-Z0-9]{2,12})(\.[a-zA-Z]{2,12})+$/
		//$emailpattern = "/^[A-Za-z0-9_\.]@([a-zA-Z0-9]{2,12})(\.[a-zA-Z]{2,12})+$/";
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			echo "Invalid email format!";
			exit;
		}
		if(strlen($password) < 2 or strlen($fname) > 30){
			echo "Password must be 2-30 characters long!";
			exit;
		} 
		if(!$about){
			echo "Please enter something on about!";
			exit;
		}
		if(strlen($about) > 10000){
			echo "About must be lower than 10000 characters long!";
			exit;
		} 
		
		echo "Complete!";
	}
?>
</body>
</html>