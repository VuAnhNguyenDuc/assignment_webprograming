<!DOCTYPE html>
<html lang="en">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<title> Exercise 4 Lab 5 </title>
	<style>
		#table1{
			width:30%;
			margin: auto;
			text-align: center;
			border: 1px solid black;
			border-collapse: collapse;
			background-color: yellow;
			font-weight: bold;
		}
		#table1 tr,td{
			width: 25px;
			height: 25px;
			border: 1px solid black;
			border-collapse: collapse;
		}
	</style>
</head>
<body>
<?php
	function Exercise4(){
		echo "<table id='table1'>";
		for($i = 1; $i < 8; $i++){	// row
			echo "<tr>";
			for($j = 1; $j < 8; $j++){ // column
				echo "<td>".($i*$j)."</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
	}
	Exercise4();
?>
</body>
</html>