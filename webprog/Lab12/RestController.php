<?php
require_once("MobileRestHandler.php");
require_once("CarRestHandler.php");

		
$method = $_SERVER["REQUEST_METHOD"];

/*
$view = "";
if(isset($_GET["view"]))
	$view = $_GET["view"];
*/
ChromePhp::log($method);	
switch($method){

	case "GET":
		$view = $_GET["view"];
		// to handle REST Url /mobile/list/
		// to handle REST Url /mobile/show/<id>/
		if($view == "all"){
			$carRestHandler = new CarRestHandler(); //MobileRestHandler();
			$carRestHandler->getAllCars();
			break;
		} else if ($view == "single"){
			$carRestHandler = new CarRestHandler(); //MobileRestHandler();
			$carRestHandler->getCar($_GET["id"]);
			break;
		}
		
	case "POST":
		// to handle REST url /car/add/
		$id  = $_POST["id"];
		$name = $_POST["name"];
		$year = $_POST["year"];

		if(gettype(intval($id)) != "integer"){
			echo "ID must be a number";
		} else if(strlen($name) < 5 || strlen($name) > 40){
			echo "Name must be from 5 to 40 characters";
		} else if(intval($year) < 1990 || intval($year) > 2015){
			echo "Year must be between 1990 and 2015";
		} else{
			$carRestHandler = new CarRestHandler();
			$result = $carRestHandler->insertCar($id,$name,$year);
			echo $result;
		}
		break;
	// Ta dung Advanced Rest Controller de goi hai method PUT va DELETE
	// Luu y them vao Raw Header Content-Type: application/x-www-form-urlencoded
	// Them du lieu vao Data form
	case "PUT":
		// to handle REST url car/update/<id>/
		parse_str(file_get_contents("php://input"),$post_vars);
		$id = $post_vars["id"];
		$name = $post_vars["name"];
		$year = $post_vars["year"];

		if(gettype(intval($id)) != "integer"){
			echo "ID must be a number";
		} else if(strlen($name) < 5 || strlen($name) > 40){
			echo "Name must be from 5 to 40 characters";
		} else if(intval($year) < 1990 || intval($year) > 2015){
			echo "Year must be between 1990 and 2015";
		} else{
			$carRestHandler = new CarRestHandler();
			$result = $carRestHandler->updateCar($id,$name,$year);
			echo $result;
		}
		break;
	case "DELETE":
	// to handle REST url car/delete/<id>/
		parse_str(file_get_contents("php://input"),$post_vars);
		$id = $post_vars["id"];
		if(gettype(intval($id)) != "integer"){
			echo "ID must be a number";
		} else{
			$carRestHandler = new CarRestHandler();
			$result = $carRestHandler->deleteCar($id);
			echo $result;
		}
		break; 	
	case "" :
		//404 - not found;
		break;
}

/*
controls the RESTful services
URL mapping

switch($view){

	case "all":
		// to handle REST Url /mobile/list/
		$mobileRestHandler = new CarRestHandler(); //MobileRestHandler();
		$mobileRestHandler->getAllCars();
		break;
		
	case "single":
		// to handle REST Url /mobile/show/<id>/
		$mobileRestHandler = new CarRestHandler(); //MobileRestHandler();
		$mobileRestHandler->getCar($_GET["id"]);
		break;

	case "" :
		//404 - not found;
		break;
}
*/


?>
