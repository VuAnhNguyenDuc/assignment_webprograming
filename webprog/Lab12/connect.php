<?php
    const DATABASE_USER = 'root';
    const DATABASE_PASSWORD = '';
    const DATABASE_HOST = 'localhost';
    const DATABASE_NAME = 'examples';
    global $dbc;

    // Tạo chuỗi két nối và thiết lập hiển thị tiếng việt:
    $dbc = @mysqli_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD,DATABASE_NAME);
    if (!mysqli_set_charset($dbc, "utf8")) {
        echo "Cannot convert to utf8 characters";
    }

// Check the Connection
    if (!$dbc) {
        trigger_error('Can not connect to MySQL: ' . mysqli_connect_error());
    }
?>