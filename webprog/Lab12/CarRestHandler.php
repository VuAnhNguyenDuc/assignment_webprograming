<?php 
	require_once "SimpleRest.php";
	require_once "Car.php";

	class CarRestHandler extends SimpleRest{
		function getAllCars(){
			$car = new Car();
			$rawData = $car->getCars();

			if(empty($rawData)){
				$statusCode = 404;
				$rawData = array("error" => "No mobile found");
			} else{
				$statusCode = 200;
			}

			$requestContentType = 'text/html';
			$this->setHttpHeaders($requestContentType,$statusCode);
			if(strpos($requestContentType,'text/html') !== false){
				$response = $this->encodeHtml($rawData);
				echo $response;
			}
		}

		public function encodeHtml($responseData) {
			$responseData = json_decode($responseData);
			$htmlResponse = "<table border='1'>";
			foreach($responseData as $object) {
	    			$htmlResponse .= "<tr><td>". $object->id. "</td><td>". $object->name. "</td><td>". $object->year. "</td></tr>";
			}
			$htmlResponse .= "</table>";
			$htmlResponse .= "<a href='insertCar.php'>Insert new car</a>";
			return $htmlResponse;		
		}

		public function encodeJson($responseData) {
			$jsonResponse = json_encode($responseData);
			return $jsonResponse;		
		}

		function getCar($id){
			$car = new Car();
			$rawData = $car->getCar($id);
			ChromePhp::log($rawData);

			if(empty($rawData)){
				$statusCode = 404;
				$rawData = array("error" => "No mobile found");
			} else{
				$statusCode = 200;
			}

			$requestContentType = 'text/html';
			$this->setHttpHeaders($requestContentType,$statusCode);
			if(strpos($requestContentType,'text/html') !== false){
				$response = $this->encodeHtml($rawData);
				echo $response;
			}
		}

		public function insertCar($id,$name,$year){
			$car = new Car();
			$result = $car->insertCar($id,$name,$year);

			$result = json_decode($result);
			if($result->result == "success"){
				$htmlResponse = "Car updated successfully";
				return $htmlResponse;
			} else{
				$error = "Failed: ".$result->info;
				$htmlResponse = $error;
				return $htmlResponse;
			}
		}

		public function updateCar($id,$name,$year){
			$car = new Car();
			$result = $car->updateCar($id,$name,$year);

			$result = json_decode($result);
			if($result->result == "success"){
				$htmlResponse = "Car updated successfully";
				return $htmlResponse;
			} else{
				$error = "Failed: ".$result->info;
				$htmlResponse = $error;
				return $htmlResponse;
			}
		}

		public function deleteCar($id){
			$car = new Car();
			$result = $car->deleteCar($id);

			$result = json_decode($result);
			if($result->result == "success"){
				$htmlResponse = "Car deleted successfully";
				return $htmlResponse;
			} else{
				$error = "Failed: ".$result->info;
				$htmlResponse = $error;
				return $htmlResponse;
			}
		}
	}
	//https://www.tutorialspoint.com/restful/restful_methods.htm
	//http://www.restapitutorial.com/lessons/httpmethods.html
	//Content-Type: application/x-www-form-urlencoded
	//https://trinitytuts.com/build-first-web-service-php/
?>