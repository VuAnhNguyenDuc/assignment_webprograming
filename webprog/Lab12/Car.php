<?php 
	class Car{
		public function __construct(){
			require "connect.php";
		}

		public function getCars(){
			global $dbc;
			$query = "SELECT * FROM cars";
			$cars = array();
			if($result = mysqli_query($dbc,$query)){
				while($row  = mysqli_fetch_assoc($result)){
					$cars[] = $row;
				}
			}
			$dbc->close();
			return json_encode($cars);
		}

		public function getCar($id){
			global $dbc;
			$query = "SELECT * FROM cars WHERE id='".$id."'";
			$car = array();
			if($result = mysqli_query($dbc,$query)){
				$row = mysqli_fetch_assoc($result);
				$car[] =$row;
			}
			$dbc->close();
			return json_encode($car);
		}

		public function insertCar($id,$name,$year){
			global $dbc;
			$query = "INSERT INTO cars VALUE('".$id."','".$name."','".$year."')";
			$result = array();			
			
			if(mysqli_query($dbc,$query)){
				$result = array("result" => "success");
			} else{
				$result = array("result" => "failed","info" => mysqli_error($dbc));
			}
			
			$dbc->close();
			return json_encode($result);
		}

		public function updateCar($id,$name,$year){
			global $dbc;
			$result = array();

			$query = "SELECT id FROM cars WHERE id='".$id."'";
			if($result = mysqli_query($dbc,$query)){
				$row = mysqli_fetch_row($result);
				$count = $row[0];
				if($count == 0){
					$result = array("result" => "failed","info" => "This id does not exits!");
				}
				return json_encode($result);
			}

			$query = "UPDATE cars SET name='".$name."',year='".$year."' WHERE id='".$id."'";
			
			if(mysqli_query($dbc,$query)){
				$result = array("result" => "success");
			} else{
				$result = array("result" => "failed","info" => mysqli_error($dbc));
			}

			$dbc->close();
			return json_encode($result);
		}

		public function deleteCar($id){
			global $dbc;
			$result = array();
			
			$query = "SELECT id FROM cars WHERE id='".$id."'";
			if($result = mysqli_query($dbc,$query)){
				$row = mysqli_fetch_row($result);
				$count = $row[0];
				if($count == 0){
					$result = array("result" => "failed","info" => "This id does not exits!");
				}
				return json_encode($result);
			}

			$query = "DELETE FROM cars WHERE id='".$id."'";

			if(mysqli_query($dbc,$query)){
				$result = array("result" => "success");
			} else{
				$result = array("result" => "failed","info" => mysqli_error($dbc));
			}

			$dbc->close();
			return json_encode($result);
		}
	}
?>