$(window).load(function(){
    var phpController = "../temp/admin_controller/Controller.php";
    var pathname = document.location.pathname.match(/[^\/]+$/)[0];
    console.log("path = "+pathname);

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    // PRODUCT
    // get product list
    if(pathname == "productPage.php"){
        $.ajax({
            type: 'POST',
            url: phpController,
            data: 'request=viewProducts',
            cache: false,
            success: function(products){
                $('#products').html("");

                $.each(products,function(key,value){
                    var status = (value.Status == "1")?"Active":"Disabled";
                    $('#products').append('<tr id=' + value.id + '>' +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center Name' contenteditable='true' colspan='3'>" + value.Name + "</td>" +

                        "<td class='center' colspan='3'>" +
                        "<ul>" +
                        "<li>House Number: <span class='HouseNumber' contenteditable='true'>"+ value.HouseNumber+"</span></li>" +
                        "<li>Street: <span class='Street' contenteditable='true'>"+ value.Streets+"</span></li>" +
                        "<li>Ward: <span class='Ward' contenteditable='true'>"+ value.Ward+"</span></li>" +
                        "<li>District: <span class='District' contenteditable='true'>"+ value.District+"</span></li>" +
                        "</ul>" +
                        "</td>" +

                        "<td class='center Category' contenteditable='true'>" + value.Category + "</td>" +
                        "<td class='center ShortDes' contenteditable='true'>" + value.ShortDes + "</td>" +
                        "<td class='center Des' contenteditable='true' colspan='3'>" + value.Descriptions + "</td>" +
                        "<td class='center Price' contenteditable='true'>" + value.Price + "</td>" +
                        "<td class='center Hotline' contenteditable='true'>" + value.Hotline + "</td>" +
                        "<td class='center OpenTime' contenteditable='true'>" + value.OpentTime + "</td>" +
                        "<td class='center PostDate' contenteditable='true'>" + value.PostDate + "</td>" +
                        "<td class='center Status' contenteditable='true'>" + status + "</td>" +
                        "<td class='center MostView' contenteditable='true'>" + value.MostView + "</td>" +
                        "<td class='center Avatar' contenteditable='true'>" + value.avatar + "</td>" +
                        "<td class='center' colspan='2'><button class='updateProduct btn btn-success' style='margin-right: 20px;'>Save</button><button class='deleteProduct btn btn-danger'>Delete</button></td>" +
                        '</tr>');
                });
            },
            error:function(xhr, status, error){
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
    }

    // delete product
    $("#products").on("click",".deleteProduct",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        if(confirm("Do you wish to delete this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: "request=deleteProduct&id="+id,
                dataType: "text",
                cache: false,
                error: function(xhr, status, error){
                    console.log(status);
                    console.log(xhr.responseText);
                },
                success: function(result){
                    if(result == "successful"){
                        alert('Delete Successful');
                        tr.find(".Status").html("Disabled");
                    } else {
                        alert("Delete Failed");
                    }
                }
            });
        }
    });

    // update product
    $('#products').on("click",".updateProduct",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        // Update values
        var Name = tr.find(".Name").html();

        var HouseNumber= tr.find(".HouseNumber").html();
        var Street = tr.find(".Street").html();
        var Ward = tr.find(".Ward").html();
        var District = tr.find(".District").html();

        var Category = tr.find(".Category").html();
        var ShortDes = tr.find(".ShortDes").html();
        var Des = tr.find(".Des").html();
        var Price = tr.find(".Price").html();
        var Hotline = tr.find(".Hotline").html();
        var OpenTime = tr.find(".OpenTime").html();
        var PostDate = tr.find(".PostDate").html();
        var Status = tr.find(".Status").html();
        var MostView = tr.find(".MostView").html();
        var Avatar = tr.find(".Avatar").html();

        var strData = "&Name="+Name+"&HouseNumber="+HouseNumber+"&Street="+Street+"&Ward="+Ward+"&District="+District+"&Category="+Category+"&ShortDes="+ShortDes+"&Des="+Des+"&Price="+Price+"&Hotline="+Hotline+"&OpenTime="+OpenTime+"&PostDate="+PostDate+"&Status="+Status+"&MostView="+MostView+"&Avatar="+Avatar ;

        if(confirm("Do you wish to update this record?")){
            console.log(strData);
            $.ajax({
                type: 'POST',
                url: phpController,
                data: "request=updateProduct&id="+id+strData,
                dataType: "text",
                success: function(result){
                    if(result == "successful"){
                        alert("Update successful");
                        tr.attr("id",nID);
                        tr.find(".Name").html(Name);
                        tr.find(".HouseNumber").html(HouseNumber);
                        tr.find(".Street").html(Street);
                        tr.find(".Ward").html(Ward);
                        tr.find(".District").html(District);
                        tr.find(".Category").html(Category);
                        tr.find(".ShortDes").html(ShortDes);
                        tr.find(".Des").html(Des);
                        tr.find(".Price").html(Price);
                        tr.find(".Hotline").html(Hotline);
                        tr.find(".OpenTime").html(OpenTime);
                        tr.find(".PostDate").html(PostDate);
                        tr.find(".Author").html(Author);
                        tr.find(".Status").html(Status);
                        tr.find(".MostView").html(MostView);
                        tr.find(".Avatar").html(Avatar);
                    } else{
                        alert("Failed: "+result);
                    }
                }
            });
        }
    });

    // insert product
    $('#insertProduct').click(function () {
        var Name = $('#insertName').val();
        var AddressID = $("#getAddressList").val();
        var RestaurantID = $("#getRestaurantList").val();
        var Category = $('#insertCategory').val();
        var ShortDes = $('#insertShortDes').val();
        var Des = $('#insertDes').val();
        var Price = $('#insertPrice').val();
        var Hotline = $('#insertHotline').val();
        var OpenTime = $('#insertOpenTime').val();
        var Status = $('#insertStatus').val();
        var MostView = $('#insertMostView').val();
        var Avatar = $('#insertAvatar').val();

        var data = {
            request: "insertProduct",
            AddressID : AddressID,
            RestaurantID : RestaurantID,
            Category : Category,
            Name: Name,
            ShortDes: ShortDes,
            Des: Des,
            Price: Price,
            Hotline: Hotline,
            OpenTime: OpenTime,
            Status: Status,
            MostView: MostView,
            Avatar: Avatar
        };

        $.ajax({
            type: "POST",
            url: phpController,
            dataType: "text",
            data: {myData: data},
            cache: false,
            success:function(result){
                console.log(result);
                if(result == "successful"){
                    alert("Record successfully inserted!");
                    window.location = "productPage.php";
                }else{
                    alert("Failed:" + result);
                }
            },
            error:function(xhr,status,error){
                console.log(status);
                console.log(xhr.responseText);
            }
        });
    });

    // save product


    // USER
    // get user list
    if(pathname == "userPage.php"){
        $.ajax({
            type: "POST",
            data: "request=viewUsers",
            url: phpController,
            cache: false,
            dataType: "json",
            success:function(users){
                $("#users").html("");
                $.each(users,function (key,value) {
                    var status = (value.Status == "1")?"Active":"Disabled";

                    $("#users").append(
                        "<tr id='"+ value.id+"'>" +
                            "<td class='center ID'>" + value.id + "</td>" +
                            "<td class='center FirstName'>" + value.FirstName + "</td>" +
                            "<td class='center LastName'>" + value.LastName + "</td>" +
                            "<td class='center Username'>" + value.UserName + "</td>" +
                            "<td class='center Email'>" + value.Email + "</td>" +
                            "<td class='center Phone'>" + value.Phone + "</td>" +
                            "<td class='center Password'>" + value.Password + "</td>" +
                            "<td class='center AvatarID'>" + value.Avatar_ID + "</td>" +
                            "<td class='center Address'>" + value.Address + "</td>" +
                            "<td class='center RegisterDate'>" + value.RegisterDate + "</td>" +
                            "<td class='center BirthDay'>" + value.BirthDay + "</td>" +
                            "<td class='center Status'>" + status + "</td>" +
                            "<td class='center' colspan='3'><button class='enableUser btn btn-success' style='margin-right: 20px;'>Enable</button><button class='disableUser btn btn-danger'>Disable</button></td>" +
                        "</tr>"
                    );
                });
            },
            error:function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    }

    // disable user
    $('#users').on("click",".disableUser",function () {
        if(confirm("Do you wish to disable this user?")){
            var tr = $(this).closest("tr");
            var id = tr.attr("id");
            $.ajax({
                type: "POST",
                url: phpController,
                data: "request=disableUser&id="+id,
                dataType: "text",
                cache: false,
                success:function(result){
                    if(result == "successful"){
                        tr.find(".Status").html("Disabled");
                        alert("This user has been disabled!");
                    }else{
                        alert("Failed to disable this user: " + result );
                    }
                },
                error: function (xhr) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // enable user
    $("#users").on("click",".enableUser",function () {
       if(confirm("Do you wish to enable this user?")){
           var tr = $(this).closest("tr");
           var id = tr.attr("id");
           $.ajax({
               type: "POST",
               url: phpController,
               data: "request=enableUser&id="+id,
               dataType: "text",
               cache: false,
               success: function(result){
                    if(result == "successful"){
                        tr.find(".Status").html("Active");
                        alert("User enabled!");
                    } else{
                        alert("Enabled user failed: "+result);
                    }
               },
               error: function(xhr){
                   var err = eval("(" + xhr.responseText +")");
                   console.log(err.Message);
               }
           });
       }
    });

    // ADMIN
    // get admin list
    if(pathname == "adminPage.php"){
        $.ajax({
            url: phpController,
            type: "POST",
            data: "request=viewAdmins",
            dataType: "json",
            cache: false,
            success: function(admins){
                $("#users").html("");
                $.each(admins,function(key,value){
                    var status = (value.Status == "1")?"Active":"Disabled";

                    $("#users").append(
                        "<tr id='"+ value.id+"'>" +
                        "<td class='center AdminID'>" + value.Admin_ID + "</td>" +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center FirstName'>" + value.FirstName + "</td>" +
                        "<td class='center LastName'>" + value.LastName + "</td>" +
                        "<td class='center Username'>" + value.UserName + "</td>" +
                        "<td class='center Email'>" + value.Email + "</td>" +
                        "<td class='center Phone'>" + value.Phone + "</td>" +
                        "<td class='center Password'>" + value.Password + "</td>" +
                        "<td class='center AvatarID'>" + value.Avatar_ID + "</td>" +
                        "<td class='center Address'>" + value.Address + "</td>" +
                        "<td class='center RegisterDate'>" + value.RegisterDate + "</td>" +
                        "<td class='center BirthDay'>" + value.BirthDay + "</td>" +
                        "<td class='center Status'>" + status + "</td>" +
                        "<td class='center' colspan='3'><button class='enableUser btn btn-success' style='margin-right: 20px;'>Enable</button><button class='disableUser btn btn-danger'>Disable</button></td>" +
                        "</tr>"
                    );
                });
            }
        });
    }

    // ADDRESS
    // get address list
    if(pathname == "addressPage.php"){
        $.ajax({
            type:"POST",
            url: phpController,
            data: "request=viewAddress",
            dataType: "json",
            cache: false,
            success: function(address){
                $("#address").html("");
                $.each(address,function(key,value){
                    var status = (value.Status == "1")?"Active":"Disabled";
                    $("#address").append(
                        "<tr id='"+ value.id+"'>" +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center HouseNumber' contenteditable='true'>" + value.HouseNumber + "</td>" +
                        "<td class='center Street' contenteditable='true'>" + value.Streets + "</td>" +
                        "<td class='center Ward' contenteditable='true'>" + value.Ward + "</td>" +
                        "<td class='center District' contenteditable='true'>" + value.District + "</td>" +
                        "<td class='center City'>" + value.City + "</td>" +
                        "<td class='center AddressFull'>" + value.AdressFull + "</td>" +
                        "<td class='center Status'>" + status + "</td>" +
                        "<td class='center' colspan='4'>" +
                            "<button class='enableAddress btn btn-success' style='margin-right: 10px;'>Enable</button>" +
                            "<button class='disableAddress btn btn-danger' style='margin-right: 10px;'>Disable</button>" +
                            "<button class='editAddress btn btn-primary'>Save</button></td>" +
                        "</tr>"
                    );
                });
            },
            error: function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    }

    // disable address
    $("#address").on("click",".disableAddress",function () {
        if(confirm("Do you wish to disable this address?")) {
            var tr = $(this).closest("tr");
            var id = tr.attr("id");
            $.ajax({
                type: "POST",
                url: phpController,
                data: "request=disableAddress&id=" + id,
                dataType: "text",
                cache: false,
                success: function (result) {
                    if (result == "successful") {
                        tr.find(".Status").html("Disabled");
                        alert("This address has been disabled!");
                    } else {
                        alert("Failed to disable this address: " + result);
                    }
                },
                error: function (xhr) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // enable address
    $("#address").on("click",".enableAddress",function () {
        if(confirm("Do you wish to enable this address?")) {
            var tr = $(this).closest("tr");
            var id = tr.attr("id");
            $.ajax({
                type: "POST",
                url: phpController,
                data: "request=enableAddress&id=" + id,
                dataType: "text",
                cache: false,
                success: function (result) {
                    if (result == "successful") {
                        tr.find(".Status").html("Active");
                        alert("Address enabled!");
                    } else {
                        alert("Failed to enable this address: " + result);
                    }
                },
                error: function (xhr) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // insert address
    $("#insertAddress").click(function () {
        if(confirm("Do you wish to insert this address?")){
            var HouseNumber = $("#insertHouseNumber").val();
            var Street = $("#insertStreet").val();
            var Ward = $("#insertWard").val();
            var District = $("#insertDistrict").val();

            var data = {
                request : "insertAddress",
                HouseNumber : HouseNumber,
                Street : Street,
                Ward : Ward,
                District : District
            };

            $.ajax({
                url: phpController,
                type: "POST",
                cache: false,
                data: { myData: data},
                dataType: "text",
                success: function(result){
                    if(result == "successful"){
                        alert("Insert Successful");
                        window.location = "addressPage.php";
                    }else{
                        alert("Insert failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // edit address
    $("#address").on("click",".editAddress",function () {
        if(confirm("Do you wish to update this address?")){
            var tr = $(this).closest("tr");
            var id = tr.attr("id");

            var HouseNumber = tr.find(".HouseNumber").html();
            var Street = tr.find(".Street").html();
            var Ward = tr.find(".Ward").html();
            var District = tr.find(".District").html();

            var AddressFull = HouseNumber + " Đường "+Street+", Phường "+Ward+", Quận "+District+", TP.HCM";

            var data = {
                request : "updateAddress",
                id: id,
                HouseNumber : HouseNumber,
                Street : Street,
                Ward : Ward,
                District : District,
                AddressFull : AddressFull
            };

            $.ajax({
                url: phpController,
                type: "POST",
                cache: false,
                data: { myData: data},
                dataType: "text",
                success: function(result){
                    if(result == "successful"){
                        tr.find(".HouseNumber").html(HouseNumber);
                        tr.find(".Street").html(Street);
                        tr.find(".Ward").html(Ward);
                        tr.find(".District").html(District);
                        tr.find(".AddressFull").html(AddressFull);
                        alert("Update Successful");
                    }else{
                        alert("Update failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // VIDEOS
    // get video list
    if(pathname == "videoPage.php"){
        $.ajax({
            url: phpController,
            type: "POST",
            data: "request=viewVideos",
            cache: false,
            dataType: "json",
            success: function(videos){
                $("#videos").html("");
                $.each(videos,function(key,value){
                    $("#videos").append(
                        "<tr id='"+ value.id+"'>" +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center Link' contenteditable='true'>" + value.Link + "</td>" +
                        "<td class='center Name' contenteditable='true'>" + value.Name + "</td>" +
                        "<td class='center ProductID' contenteditable='true'>" + value.Product_ID + "</td>" +
                        "<td class='center ProductName'>" + value.ProductName + "</td>" +
                        "<td class='center'>" +
                            "<button class='editVideo btn btn-primary'>Save</button></td>" +
                        "</tr>"
                    );
                });
            },
            error: function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    }

    // insert video
    $("#insertVideo").click(function(){
        if(confirm("Do you wish to insert this record?")){
            var link = $("#insertLink").val();
            var name = $("#insertName").val();
            var productID = $("#getProductList").val();
            var data = {
                request : "insertVideo",
                link: link,
                name: name,
                productID: productID
            };
            $.ajax({
                type: "POST",
                url: phpController,
                data: {myData: data},
                dataType: "text",
                cache: false,
                success: function(result){
                    if(result == "successful"){
                        alert("Insert successful");
                        window.location = "videoPage.php";
                    } else{
                        alert("Insert failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                }
            });
        }
    });

    // update video
    $("#videos").on("click",".editVideo",function(){
        if(confirm("Do you wish to update this record?")){
            var tr = $(this).closest("tr");
            var id = tr.attr("id");
            var name = tr.find(".Name").html();
            var link = tr.find(".Link").html();
            var productID = tr.find(".ProductID").html();

            var data = {
                request : "updateVideo",
                id : id,
                link: link,
                name: name,
                productID: productID
            };
            $.ajax({
                type: "POST",
                url: phpController,
                dataType : "json",
                data: {myData: data},
                success: function(resultdata){
                    if(resultdata["result"] == "successful"){
                        alert("Update successful");
                        tr.find(".ProductName").html(resultdata["productName"]);
                    } else{
                        alert("Update failed: "+resultdata["productName"]);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // COMMENTS
    // get comment list
    if(pathname == "commentPage.php"){
        $.ajax({
            type: "POST",
            url: phpController,
            data: "request=viewComments",
            dataType: "json",
            cache: false,
            success: function(comments){
                console.log(comments);
                $("#comments").html("");
                $.each(comments,function(key,value){
                    var status = (value.Status == 1)? "Active": "Disabled";
                    $("#comments").append(
                        "<tr id='"+ value.id+"'>" +
                        "<td class='center Content'>" + value.Contents + "</td>" +
                        "<td class='center UserID'>" + value.User_ID + "</td>" +
                        "<td class='center ProductID'>" + value.Products + "</td>" +
                        "<td class='center NewsID'>" + value.News_ID + "</td>" +
                        "<td class='center Name'>" + value.Name + "</td>" +
                        "<td class='center Email'>" + value.Email + "</td>" +
                        "<td class='center Status'>" + status + "</td>" +
                        "<td class='center' colspan='3'><button class='enableComment btn btn-success' style='margin-right: 20px;'>Enable</button><button class='disableComment btn btn-danger'>Disable</button></td>" +
                        "</tr>"
                    );
                });
            },
            error: function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    }

    // enable comment
    $("#comments").on("click",".enableComment",function(){
        if(confirm("Do you wish to enable this comment?")){
            var tr = $(this).closest("tr");
            var id = tr.attr("id");
            $.ajax({
                url: phpController,
                type: "POST",
                data: "request=enableComment&id="+id,
                dataType: "text",
                cache: false,
                success: function(result){
                    if(result == "successful"){
                        alert("This comment has been enabled!");
                        tr.find(".Status").html("Active");
                    }else{
                        alert("Enable failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // disasble comment
    $("#comments").on("click",".disableComment",function(){
        if(confirm("Do you wish to disable this comment?")){
            var tr = $(this).closest("tr");
            var id = tr.attr("id");
            $.ajax({
                url: phpController,
                type: "POST",
                data: "request=disableComment&id="+id,
                dataType: "text",
                cache: false,
                success: function(result){
                    if(result == "successful"){
                        alert("This comment has been disabled!");
                        tr.find(".Status").html("Disabled");
                    }else{
                        alert("Disable failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // RATE
    // get rate list
    if(pathname == "ratePage.php"){
        $.ajax({
            url: phpController,
            type: "POST",
            cache: false,
            data: "request=viewRates",
            dataType: "json",
            success: function(rates){
                $("#rates").html("");
                $.each(rates,function(key,value){
                    $("#rates").append(
                        "<tr id='"+ value.id+"'>" +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center ProductID'>" + value.Product_ID + "</td>" +
                        "<td class='center UserID'>" + value.User_ID + "</td>" +
                        "<td class='center AddressRate'>" + value.Address_Rate + "</td>" +
                        "<td class='center QualityRate'>" + value.Quanlity_Rate + "</td>" +
                        "<td class='center PriceRate'>" + value.Price_Rate + "</td>" +
                        "<td class='center ServiceRate'>" + value.Service_Rate + "</td>" +
                        "<td class='center SpaceRate'>" + value.Space_Rate + "</td>" +
                        "</tr>"
                    );
                });
            },
            error: function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    }

    // RESTAURANT
    // get restaurant list
    if(pathname == "restaurantPage.php"){
        $.ajax({
            type: 'POST',
            url: phpController,
            data: 'request=viewRestaurants',
            cache: false,
            success: function(restaurants){
                $('#restaurants').html("");

                $.each(restaurants,function(key,value){
                    var status = (value.Status == "1")?"Active":"Disabled";
                    $('#restaurants').append('<tr id=' + value.id + '>' +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center Name' contenteditable='true'>" + value.Name + "</td>" +
                        "<td class='center Service' contenteditable='true'>" + value.Service + "</td>" +
                        "<td class='center Status'>" + status + "</td>" +
                        "<td class='center' colspan='2'><button class='updateRestaurant btn btn-success' >Save</button> <button class='enableRestaurant btn btn-primary'>Enable</button> <button class='disableRestaurant btn btn-danger'>Disable</button></td>" +
                        '</tr>');
                });
            },
            error:function(xhr, status, error){
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
    }

    // disable restaurant
    $("#restaurants").on("click",".disableRestaurant",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        if(confirm("Do you wish to disable this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: "request=disableRestaurant&id="+id,
                dataType: "text",
                cache: false,
                error: function(xhr, status, error){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                },
                success: function(result){
                    if(result == "successful"){
                        alert('Record disabled!');
                        tr.find(".Status").html("Disabled");
                    } else {
                        alert("Disabled failed: "+result);
                    }
                }
            });
        }
    });

    // enable restaurant
    $("#restaurants").on("click",".enableRestaurant",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        if(confirm("Do you wish to disable this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: "request=enableRestaurant&id="+id,
                dataType: "text",
                cache: false,
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                },
                success: function(result){
                    if(result == "successful"){
                        alert('Record enabled!');
                        tr.find(".Status").html("Active");
                    } else {
                        alert("Enabled failed: "+result);
                    }
                }
            });
        }
    });

    // update restaurant
    $('#restaurants').on("click",".updateRestaurant",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        // Update values
        var Name = tr.find(".Name").html();
        var Service= tr.find(".Service").html();

        var data = {
            request: "updateRestaurant",
            id: id,
            Name: Name,
            Service: Service
        };

        if(confirm("Do you wish to update this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: {myData: data},
                dataType: "text",
                success: function(result){
                    if(result == "successful"){
                        alert("Update successful");
                        tr.find(".Name").html(Name);
                        tr.find(".Service").html(Service);
                    } else{
                        alert("Failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // insert restaurant
    $('#insertRestaurant').click(function () {
        var Name = $('#insertName').val();
        var Service = $('#insertService').val();

        var data = {
            request: "insertRestaurant",
            Name: Name,
            Service : Service
        };

        $.ajax({
            type: "POST",
            url: phpController,
            dataType: "text",
            data: {myData: data},
            cache: false,
            success:function(result){
                console.log(result);
                if(result == "successful"){
                    alert("Record successfully inserted!");
                    window.location = "restaurantPage.php";
                }else{
                    alert("Failed:" + result);
                }
            },
            error:function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    });

    // NEWS
    // get news list
    if(pathname == "newsPage.php"){
        $.ajax({
            type: 'POST',
            url: phpController,
            data: 'request=viewNews',
            cache: false,
            success: function(news){
                $('#news').html("");

                $.each(news,function(key,value){
                    var status = (value.Status == "1")?"Active":"Disabled";
                    $('#news').append('<tr id=' + value.id + '>' +
                        "<td class='center ID'>" + value.id + "</td>" +
                        "<td class='center Name' contenteditable='true'>" + value.Name + "</td>" +
                        "<td class='center ShortDes' contenteditable='true'>" + value.ShortDescriptions + "</td>" +
                        "<td class='center Des' contenteditable='true'>" + value.Descriptions + "</td>" +
                        "<td class='center UserID' contenteditable='true'>" + value.User_ID + "</td>" +
                        "<td class='center ImageID' contenteditable='true'>" + value.ImageID + "</td>" +
                        "<td class='center Postdate' contenteditable='true'>" + value.Postdate + "</td>" +
                        "<td class='center Author' contenteditable='true'>" + value.Author + "</td>" +
                        "<td class='center CommentID' contenteditable='true'>" + value.CommentID + "</td>" +
                        "<td class='center MostView' contenteditable='true'>" + value.MostView + "</td>" +
                        "<td class='center Status'>" + status + "</td>" +
                        "<td class='center' colspan='2'><button class='updateNews btn btn-success' style='margin-right: 20px;'>Save</button><button class='enableNews btn btn-primary'>Enable</button> <button class='disableNews btn btn-danger'>Disable</button></td>" +
                        '</tr>');
                });
            },
            error:function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    }

    // disable news
    $("#news").on("click",".disableNews",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        if(confirm("Do you wish to disable this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: "request=disableNews&id="+id,
                dataType: "text",
                cache: false,
                error: function(xhr, status, error){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                },
                success: function(result){
                    if(result == "successful"){
                        alert('Record disabled!');
                        tr.find(".Status").html("Disabled");
                    } else {
                        alert("Disabled failed: "+result);
                    }
                }
            });
        }
    });

    // enable news
    $("#news").on("click",".enableNews",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        if(confirm("Do you wish to disable this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: "request=enableNews&id="+id,
                dataType: "text",
                cache: false,
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                },
                success: function(result){
                    if(result == "successful"){
                        alert('Record enabled!');
                        tr.find(".Status").html("Active");
                    } else {
                        alert("Enabled failed: "+result);
                    }
                }
            });
        }
    });

    // update news
    $('#news').on("click",".updateNews",function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("id");

        // Update values
        var Name = tr.find(".Name").html();
        var ShortDes = tr.find(".ShortDes").html();
        var Des = tr.find(".Des").html();
        var UserID = tr.find(".UserID").html();
        var ImageID = tr.find(".ImageID").html();
        var Postdate = tr.find(".Postdate").html();
        var Author = tr.find(".Author").html();
        var CommentID = tr.find(".CommentID").html();
        var MostView = tr.find(".MostView").html();

        var data = {
            request: "updateNews",
            id: id,
            Name: Name,
            ShortDes : ShortDes,
            Des : Des,
            UserID : UserID,
            ImageID : ImageID,
            Postdate : Postdate,
            Author : Author,
            CommentID : CommentID,
            MostView : MostView
        };

        if(confirm("Do you wish to update this record?")){
            $.ajax({
                type: 'POST',
                url: phpController,
                data: {myData: data},
                dataType: "text",
                success: function(result){
                    if(result == "successful"){
                        alert("Update successful");
                        tr.find(".Name").html(Name);
                        tr.find(".Service").html(Service);
                    } else{
                        alert("Failed: "+result);
                    }
                },
                error: function(xhr){
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    // insert news
    $('#insertNews').click(function () {
        var Name = $('#insertName').val();
        var ShortDes = $('#insertShortDes').val();
        var Des = $('#insertDes').val();
        var UserID = $('#insertUserID').val();
        var ImageID = $('#insertImageID').val();
        var Postdate = $('#insertPostdate').val();
        var Author = $('#insertAuthor').val();
        var CommentID = $('#insertCommentID').val();
        var MostView = $('#insertMostView').val();

        var data = {
            request: "insertNews",
            Name: Name,
            ShortDes : ShortDes,
            Des : Des,
            UserID : UserID,
            ImageID : ImageID,
            Postdate : Postdate,
            Author : Author,
            CommentID : CommentID,
            MostView : MostView
        };

        $.ajax({
            type: "POST",
            url: phpController,
            dataType: "text",
            data: {myData: data},
            cache: false,
            success:function(result){
                console.log(result);
                if(result == "successful"){
                    alert("Record successfully inserted!");
                    window.location = "newsPage.php";
                }else{
                    alert("Failed:" + result);
                }
            },
            error:function(xhr){
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
            }
        });
    });
});



