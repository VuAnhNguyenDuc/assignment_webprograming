<?php
    class commentModel{
        public function __construct(){
            require "connect.php";
        }
        public function getComments(){
            global $dbc;
            $query = "SELECT * FROM comment";
            $comments = array();
            if($result = mysqli_query($dbc,$query)){
                while($row = mysqli_fetch_assoc($result)){
                    $comments[] = $row;
                }
                return json_encode($comments);
            } else{
                return null;
            }
        }
        public function enableComment($id){
            global $dbc;
            $dbc->begin_transaction();
            try {
                $query = "UPDATE comment SET Status='1' WHERE id='" . $id . "'";
                if($result = mysqli_query($dbc,$query)){
                    $dbc->commit();
                    echo "successful";
                } else{
                    $error = str_replace("'","",mysqli_error($dbc));
                    throw new Exception($error);
                }
            } catch (Exception $e) {
                $dbc->rollback();
                echo $e;
            }
        }
        public function disableComment($id){
            global $dbc;
            $dbc->begin_transaction();
            try {
                $query = "UPDATE comment SET Status='-1' WHERE id='" . $id . "'";
                if($result = mysqli_query($dbc,$query)){
                    $dbc->commit();
                    echo "successful";
                } else{
                    $error = str_replace("'","",mysqli_error($dbc));
                    throw new Exception($error);
                }
            } catch (Exception $e) {
                $dbc->rollback();
                echo $e;
            }
        }
        public function close(){
            global $dbc;
            $dbc->close();
        }
    }
?>