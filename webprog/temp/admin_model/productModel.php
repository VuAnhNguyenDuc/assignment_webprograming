<?php
    //require_once '../admin_controller/ChromePhp.php';

    class productModel{
        function __construct(){
            require_once 'connect.php';
        }

        public function getProducts(){
            global $dbc;
            $query = "SELECT * FROM products";
            $products = mysqli_query($dbc,$query);
            return $products;
        }

        public function getAddress($id){
            global $dbc;
            $query = "SELECT Address_ID FROM address_product WHERE Product_ID='".$id."'";

            $Address_ID = "";

            if($result = mysqli_query($dbc,$query)){
                while($row = mysqli_fetch_row($result)){
                    $Address_ID = $row[0];
                }
            }

            $query = "SELECT AdressFull,Streets,Ward,District FROM adress WHERE id='".$Address_ID."'";

            if($result = mysqli_query($dbc,$query)){
                $row = mysqli_fetch_row($result);
                $array = array($row[0],$row[1],$row[2],$row[3]);
                return $array;
            } else{
                return "";
            }
        }

        public function getCategory($id){
            global $dbc;
            $query = "SELECT Category_ID FROM product_cate WHERE Product_ID='".$id."'";

            $Category_ID = "";
            $Category = "";

            if($result = mysqli_query($dbc,$query)){
                while($row = mysqli_fetch_row($result)){
                    $Category_ID = $row[0];
                }
            }

            $query = "SELECT Name FROM category WHERE id='".$Category_ID."'";

            if($result = mysqli_query($dbc,$query)){
                while($row = mysqli_fetch_row($result)){
                    $Category = $row[0];
                }
                return $Category;
            }
        }

        public function getProductList(){
            global $dbc;
            $query = "SELECT id,Name FROM products";
            if($result = mysqli_query($dbc,$query)){
                $productList = array();
                while($row = mysqli_fetch_assoc($result)){
                    $productList[] = $row;
                }
                return json_encode($productList);
            }
            return str_replace("'","",mysqli_error($dbc));
        }

        public function insertProduct($Name,$HouseNumber,$Street,$Ward,$District,$Category,$ShortDes,$Des,$Price,$Hotline,$OpenTime,$Status,$MostView,$Avatar){
            global $dbc;
            $dbc->begin_transaction();
            try{
                // insert into product table
                $query = "INSERT INTO products(Name, ShortDes, Descriptions, Price, Hotline, OpentTime, PostDate,Status, MostView, avatar) VALUE ('".$Name."','".$ShortDes."','".$Des."','".$Price."','".$Hotline."','".$OpenTime."',CURRENT_TIMESTAMP,'".$Status."','".$MostView."','".$Avatar."')";
                if(!$result = mysqli_query($dbc,$query)){
                    $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                    throw new Exception($error);
                } else{
                    // return the new product id
                    $nID = $dbc->insert_id;
                }

                // Check address
                $AddressFull = $HouseNumber . " Đường " . $Street . ", Phường " . $Ward . ", Quận " .$District. " TP.HCM" ;
                $query = "SELECT * FROM adress WHERE AdressFull = '".$AddressFull."'";
                $result = mysqli_query($dbc,$query);
                if($result->num_rows == 0){
                    // if the address is new
                    $query = "INSERT INTO adress(Streets, Ward, District, City, AdressFull, Status) VALUES ('".$Street."','".$Ward."','".$District."','TP.HCM','".$AddressFull."','1')";
                    if($result = mysqli_query($dbc,$query)){
                        // get id
                        $query = "SELECT id FROM adress WHERE AdressFull = '".$AddressFull."'";
                        $result = mysqli_query($dbc,$query);
                        $Address_ID = "";
                        while($row = mysqli_fetch_row($result)){
                            $Address_ID = $row[0];
                        }
                        $query = "INSERT INTO address_product VALUE ('".$Address_ID."','".$nID."')";
                        if(!$result = mysqli_query($dbc,$query)){
                            $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                            throw new Exception($error);
                        }
                    } else{
                        $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                        throw new Exception($error);
                    }
                }else{
                    // if the address exist
                    $query = "SELECT id FROM adress WHERE AdressFull = '".$AddressFull."'";
                    $Address_ID = "";
                    if($result = mysqli_query($dbc,$query)){
                        while($row = mysqli_fetch_row($result)){
                            $Address_ID = $row[0];
                        }
                        $query = "INSERT INTO address_product VALUE ('".$Address_ID."','".$nID."')";
                        if(!$result = mysqli_query($dbc,$query)){
                            $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                            throw new Exception($error);
                        }
                    } else{
                        $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                        throw new Exception($error);
                    }
                }

                // Check category
                $catid = ($Category == "Ăn gì?")?3:4;
                $query = "INSERT INTO product_cate(Category_ID, Product_ID) VALUES('".$catid."','".$nID."')";
                if(!$result = mysqli_query($dbc,$query)){
                    $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                    throw new Exception($error);
                }

                $dbc->commit();
                echo "successful";
            } catch (Exception $ex){
                $dbc->rollback();
                echo $ex;
            }
        }

        public function editProduct($id,$Name,$HouseNumber,$Street,$Ward,$District,$Category,$ShortDes,$Des,$Price,$Hotline,$OpenTime,$PostDate,$Status,$MostView,$Avatar){
            global $dbc;
            $dbc->begin_transaction();
            try{
                $AddressFull = $HouseNumber . " Đường " . $Street . ", Phường " . $Ward . ", Quận " .$District. " TP.HCM" ;

                // Check if address is in database
                $query = "SELECT * FROM adress WHERE AdressFull = '".$AddressFull."'";
                    $result = mysqli_query($dbc,$query);
                    if($result->num_rows == 0){
                        // if the address is new
                        $query = "INSERT INTO adress(Streets, Ward, District, City, AdressFull, Status) VALUES ('".$Street."','".$Ward."','".$District."','TP.HCM','".$AddressFull."','1')";
                        if($result = mysqli_query($dbc,$query)){
                            // get new address id
                            $query = "SELECT id FROM adress WHERE AdressFull = '".$AddressFull."'";
                            $result = mysqli_query($dbc,$query);
                            $Address_ID = "";
                            while($row = mysqli_fetch_row($result)){
                                $Address_ID = $row[0];
                            }
                            $query = "UPDATE address_product SET Address_ID='".$Address_ID."' WHERE Product_ID='".$id."'";
                            if(!$result = mysqli_query($dbc,$query)){
                                $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                                throw new Exception($error);
                            }
                        } else{
                            $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                            throw new Exception($error);
                        }
                    } else{
                        // if the address exist
                        $query = "SELECT id FROM adress WHERE AdressFull = '".$AddressFull."'";
                        $Address_ID = "";
                        if($result = mysqli_query($dbc,$query)){
                            while($row = mysqli_fetch_row($result)){
                                $Address_ID = $row[0];
                            }
                            $query = "UPDATE address_product SET Address_ID='".$Address_ID."' WHERE Product_ID='".$id."'";
                            if(!$result = mysqli_query($dbc,$query)){
                                $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                                throw new Exception($error);
                            }
                        } else{
                            $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                            throw new Exception($error);
                        }
                    }

                    // Check category
                    $catid = ($Category == "Ăn gì?")?3:4;
                    $query = "UPDATE product_cate SET Category_ID = '$catid' WHERE Product_ID = '".$id."'";
                    if(!$result = mysqli_query($dbc,$query)){
                        $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                        throw new Exception($error);
                    }

                    // Update the rest
                    $query = "UPDATE products SET Name='".$Name."',ShortDes ='".$ShortDes."',Descriptions='".$Des."',Price='".$Price."',Hotline='".$Hotline."',OpentTime='".$OpenTime."',PostDate='".$PostDate."',Status='".$Status."',MostView='".$MostView."',avatar='".$Avatar."' WHERE id='".$id."'";

                    if(!$result = mysqli_query($dbc,$query)){
                        $error = str_replace("'"," ","SQL Error: ".mysqli_error($dbc));
                        throw new Exception($error);
                    }
                echo "successful";
                $dbc->commit();
            } catch (Exception $ex){
                $dbc->rollback();
                echo $ex;
                //->getMessage()
            }
        }

        public function deleteProduct($id){
            global $dbc;
            $query = "UPDATE products SET Status= '-1' WHERE id='".$id."'" ;

            $dbc->begin_transaction();
            if($result = mysqli_query($dbc,$query)){
                $dbc->commit();
                return "successful";
            } else{
                $dbc->rollback();
                return "failed";
            }
        }

        public function close(){
            global $dbc;
            mysqli_close($dbc);
        }
    }
?>