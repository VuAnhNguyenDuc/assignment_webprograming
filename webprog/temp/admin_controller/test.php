<?php
    header('Content-type: application/json');
    include 'ChromePhp.php';
    require "../admin_model/productModel.php";

    $request = (isset($_POST['request']) && gettype($_POST['request']) == "string")? $_POST['request']:(isset($_POST['myData']))?json_decode(json_encode($_POST['myData']))->request:"null";

    ChromePhp::log($request);

    $productModel = new productModel();
    $products = $productModel->getProducts();
    $array = array();
    while($row = mysqli_fetch_assoc($products)){
        $id = $row['id'];
        $address = $productModel->getAddress($id);
        $row['Price'] = number_format($row['Price'])." vnđ";

        $HouseNumber = strchr($address['AdressFull'],"Đường",true);
        $row['HouseNumber'] = $HouseNumber;
        $row['Streets'] = $address['Streets'];
        $row['Ward'] = $address['Ward'];
        $row['District'] = $address['District'];

        $category = $productModel->getCategory($id);
        $row['Category'] = $category;

        $array[] = $row;
    }
    $productModel->close();

    echo json_encode($array);
?>