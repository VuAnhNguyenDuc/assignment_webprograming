<!DOCTYPE html>
<html lang="en">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<link href="./css/style.css" rel="stylesheet" />
	<link href="./css/bootstrap.min.css" rel="stylesheet" />
	<title> Lab 7 </title>
</head>
<body>
	<div class="col-md-6">
		<h1><span class="label label-primary">Please fill in the following information</span></h1>
		<form id="insertForm" method="post">
			<div class="form-group">
				<label for="id">ID:</label>
				<input type="text" class="form-control" id="id" name="id" required>
			</div>
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" class="form-control" id="name" name="name" required>
			</div>
			<div class="form-group">
				<label for="year">Year:</label>
				<input type="text" class="form-control" id="year" name="year" required>
			</div>
			<button type="button" id="test" value="Add" class="btn btn-default" onclick="validateForm();">Add</button>
			<button type="button" class="btn btn-default" onclick="Return();">Return</button>
			<button type="submit" id="submit" hidden="hidden"></button>
			<button type="reset" hidden="hidden" id="reset"></button>
		</form>
	</div>
<script>
	function validateForm(){
		var id = document.forms["insertForm"]["id"].value;
		var name = document.forms["insertForm"]["name"].value;
		var year = document.forms["insertForm"]["year"].value;
		
		if(id == ""){ 
			alert("id must be filled out"); document.getElementById("reset").click(); return false;
		}
		if(name == ""){ 
			alert("name must be filled out"); document.getElementById("reset").click(); return false;
		}
		if(year == ""){ 
			alert("year must be filled out"); document.getElementById("reset").click(); return false;
		}
			
		if( isNaN(id)){
			alert("id must be a number");
			document.getElementById("reset").click();
			return false;
		}
		if( name.length < 5 || name.length > 40){
			alert("name must be within 5 to 40 characters");
			document.getElementById("reset").click();
			return false;
		}
		if( isNaN(year)){
			alert("year and must be a number");
			document.getElementById("reset").click();
			return false;
		}
		if( year < 1930 || year > 2015){
			alert("year must be within 1930 to 2015");
			document.getElementById("reset").click();
			return false;
		}
		document.getElementById("submit").click();
		return true;
	}
	function Return(){
		window.location = "menu.php";
	}
</script>
<?php
	if($_SERVER['REQUEST_METHOD'] == 'POST') {
		$username = "root";
		$password = "";
		$hostname = "localhost";
				
		// connect to the database
		$dbhandle = mysqli_connect($hostname,$username,$password) or die("Unable to connect to MySQL");
				
		$selected = mysqli_select_db($dbhandle, "examples") or die("Could not select examples");
			
		$query = "INSERT INTO cars(id,name,year) VALUES ('$_POST[id]','$_POST[name]','$_POST[year]')";
							
		if (!mysqli_query($dbhandle,$query)){
			$error = str_replace("'"," ","SQL Error: ".mysqli_error($dbhandle));
			
			echo "<script>";
			echo "alert(' $error ');";
			echo "</script>";
			sleep(2);
			//echo("SQL Error: " . mysqli_error($dbhandle));
		}else{
			echo "<script>";
			echo "alert(' Data was added successfully ');";
			echo "</script>";
		}
		// fetch data from the database		
		mysqli_close($dbhandle);	
	}
?>
</body>
</html>