<!DOCTYPE html>
<html lang="en">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<link href="./css/style.css" rel="stylesheet" />
	<link href="./css/bootstrap.min.css" rel="stylesheet" />
	<title> Lab 7 </title>
</head>
<body>
	<h1><button type="button" id="insertBtt" class="btn btn-lg btn-primary">Add cars</button></h1>
	<div class="row">	
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>ID</th>
					<th>Name</th>
					<th>Year</th>
					<th>Actions</th>
				</thead>
				<tbody>
					<?php include 'select.php'; ?>
				</tbody>
			</table>
		</div>
	</div>
<script type="text/javascript">
	document.getElementById("insertBtt").onclick = function(){
		location.href = "insert.php";
	}
</script>
</body>
</html>