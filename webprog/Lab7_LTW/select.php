<?php
	$username = "root";
	$password = "";
	$hostname = "localhost";
		
	// connect to the database
	$dbhandle = mysqli_connect($hostname,$username,$password) or die("Unable to connect to MySQL");
	
	$selected = mysqli_select_db($dbhandle, "examples") or die("Could not select examples");
		
	//$result = mysqli_query($dbhandle, "SELECT * FROM cars");
	
	if (!($result = mysqli_query($dbhandle,"SELECT * FROM cars"))){
		echo("SQL Error: " . mysqli_error($dbhandle));
		$error = str_replace("'"," ","SQL Error: ".mysqli_error($dbhandle));
		echo "<script>";
		echo "alert(' $error ');";
		echo "</script>";
	}
				
	// fetch data from the database
	$i = 1;
	while ($row = mysqli_fetch_array($result)) {
	    $temp = "<tr>
					<td>".$i."</td>
					<td>".$row{'id'}."</td>
					<td>".$row{'name'}."</td>
					<td>".$row{'year'}."</td>
					<td>
						<a href='edit.php?id=".$row{'id'}."' class='btn btn-primary linkbutton'>Edit</button>
						<a href='delete.php?id=".$row{'id'}."' class='btn btn-danger linkbutton'>Delete</button>
					</td>
			    </tr>";
		echo $temp;
	    $i++;
	}
	mysqli_close($dbhandle);		
?>
