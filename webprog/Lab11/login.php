<html>
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<title> Lab 11 - Login page </title>
	<?php
		if($_SERVER["REQUEST_METHOD"] == "POST"){
			$username = $_POST["username"];
			if($username != "" && $username != null){
				setcookie("username",$username,time()+3600,"/");
				header('Location: index.php');
			}
		}	
	?>
</head>
<body>
	<?php 
		if(isset($_COOKIE["username"])){
			header('Location: index.php');
		} else{	
	?>
	<h1>Login page</h1>
	<form method = "post">
		<table>
			<tr>
				<td><label for='username'>Username: </label></td>
				<td colspan='3'><input type='text' name='username' style='width: 100%'></input></td>
			</tr>
			<tr>
				<td><label for='password'>Password: </label></td>
				<td colspan='3'><input type='password' name='password' style='width: 100%'></input></td>
			</tr>
			<tr>
				<td><button type='submit'>Login</button></td>
			</tr>
		</table>
	</form>
	<?php 
		}
	?>
</body>
</html>