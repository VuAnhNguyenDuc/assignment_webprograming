COOKIE
- Ưu điểm:
+ Tính thuận tiện : cookie giúp ta ghi nhớ các website đã truy cập, đồng thời nó cũng ghi nhớ các dữ liệu ta đã điền vào form. Bằng cách này ta có thể dễ dàng điền lại những thông tin mà ta đã điền nhiều lần (ví dụ đĩa chỉ đơn hàng...). Ngày nay nhiều cửa hàng online lưu thông tin địa chỉ và email của người dùng bằng cookie.
+ Tính cá nhân (personalization) : cookie giúp các trang web có thể biết được nhu cầu riêng của mỗi người dùng. Ví dụ Amazon dùng cookie để giới thiệu người dùng những sản phẩm liên quan đến nhưng thứ họ xem chi tiết hoặc cho vào giỏ hàng.
+ Quảng cáo hiệu quả : những công ty marketing online thu thập dữ liệu từ cookie để lập ra những chiến dịch marketing nhằm vào một phân khúc (sản phầm, khách hàng) nhất định.
+ Dễ quản lý: người dùng có thể dễ dàng quản lý cookie của mình thông qua trình duyệt, giúp người dùng có thể chọn lựa nên giữ cookie nào và xóa cookie nào (nếu họ biết cách dùng chức năng này)

- Nhược điểm:
+ Độ riêng tư : Mối lo lớn nhất của người dùng về cookie là tính riêng tư, những browser mặc định cho phép trang web lưu cookie vào máy sẽ dễ dàng giúp cho các bên thứ ba có thể truy cập những thông tin đã được lưu trong cookie. Bên thứ ba có thể là công ty quảng cáo, người dùng khác, hoặc ngay cả chính phủ trong một số trường hợp.
+ Tính bảo mật : tính bảo mật của cookie là một vấn đề lớn. Mối lo ngại ở đây đó là nhiều lỗ hổng bảo mật đã được tìm thấy trong nhiều trình duyệt khác nhau. Một số các lỗ hổng này nguy hiểm đến nỗi nó cho phép những trang web chứa mã độc (malicious) có thể truy cập và lấy được thông tin người dùng ( địa chỉ, email, sđt và ngay cả thẻ tín dụng)
+ Tính bí mật (secrecy) : mặc dù cookie từ bên thứ ba có thể bị block bởi trình duyệt. Nhưng hầu hết các người dùng không có kỹ năng cần thiết để làm việc này. Phần lớn trình duyệt sẽ làm người dùng cảm thấy khó khăn để biết cách dùng chức năng này. Bởi vì không có cookie nghĩa là không có thông tin, và cũng đồng nghĩa với việc giảm doanh thu của các trình duyệt.

source : http://www.internetmarketinginc.com/blog/the-pros-and-cons-of-cookies-a-google-story/

SESSION
- Ưu điểm:
+ Nếu developer có những giá trị cần dùng trong nhiều trang web khác nhau, họ chỉ cần dùng session để chứa các giá trị đó, thay vì phải lưu vào database và sau đó đọc trong database lại.
+ Session được quản lý bởi phía server , và trạng thái của nó phụ thuộc hoàn toàn vào server. Có nghĩa là mặc dù người dùng sử dụng các trình duyệt khác nhau, các developer luôn có thể đảm bảo một trạng thái nhất định cho trang web của mình.
- Nhược điểm:
+ Session và cookie đồng bộ với nhau, có nghĩa là khi người dùng thiết lập chế độ từ chối cookie, thì ngay cả session cũng sẽ không hoạt động.
+ Vì session có thể được khởi tạo thoải mái và sữ dụng bất cứ đâu trong trang web. Nếu developer sử dụng quá nhiều biến session có thể tạo ra một chương trình rất khó đọc và bảo trì.
+ Session có tuổi thọ ngắn và được tạo ra để quản lý duy nhất một người dùng độc nhất truy cập đến website. 
source : http://www.4guysfromrolla.com/webtech/092098-2.shtml
http://stackoverflow.com/questions/10430432/what-are-pros-and-cons-of-cookies-versus-sessions

Ví dụ các trang web sử dụng cookie và session: 
Amazon , Tiki, Thegioididong, Lazada, những trang web này lưu lại những sản phẩm mà một khách hàng hay quan tâm để có thể gợi ý cho khách hàng những sản phẩm liên quan đến nó nhằm thu lại lợi nhuận cao hơn. 


