function getCars(){
	$.ajax({
		type: 'GET',
		url: 'getCars.php',
		data: 'json',
		cache: false,
		success: function(car){
			console.log(car);
			$.each(car,function(key,value){
				// key : 0,1,2,3
				var newCar = "<tr id='"+ value.id +"'>" + 
				"<td class='id' contenteditable='true'>"+ value.id +"</td>" +
				"<td class='name' contenteditable='true'>"+ value.name +"</td>" +
				"<td class='year' contenteditable='true'>"+ value.year +"</td>" +
				"<td><button style='margin-right: 20px;' class='deleteRow btn btn-danger'>DELETE</button>" +
				"<button class='saveRow btn btn-success'>SAVE</button></td>" +
				+"</tr>";
				$(newCar).appendTo('#cars');
			});
		}
	});
}

$(document).ready(function(){
	// insert
	$('#submit').click(function(){
		var id = parseInt($('#id').val());
		var name = $("#name").val();
		var year = parseInt($('#year').val());

		var object = new Object();
		object.id= id;
		object.name = name;
		object.year = year;

		/*var newCar = JSON.stringify(object);
		console.log(newCar);*/
		var newCar = object;
		var dataStr = "id="+id+"&name="+name+"&year="+year;

		// validate
		if(isNaN(id)){
			alert("Id must be a number");
		} else if(name == ''||year==''){
			alert("Please fill in all of the required informations");
		} else if(isNaN(year)){
			alert("Year must be a number");
		} else if(name.length < 5 || name.length > 40){
			alert("Name must be from 5 to 40 character");
		} else if(year < 1990 || year > 2015){
			alert("Year must be from 1990 to 2015");
		} else{
			$.ajax({
				type: 'POST',
				url: 'insertCar.php',
				data: dataStr,
				cache: false,
				success: function(){
					console.log(newCar);
					alert('success');
					var newCar = "<tr id='"+id+"'>" + 
									"<td contenteditable='true'>"+ id +"</td>" +
									"<td contenteditable='true'>"+ name +"</td>" +
									"<td contenteditable='true'>"+ year +"</td>" +
									"<td><button class='btn btn-danger'>Delete</button></td>" +
									+"</tr>";
					$(newCar).appendTo('#cars');
					$('#id').val('');
					$('#name').val('');
					$('#year').val('');
				}
			});
		}
		return false;
	});
	//edit
	$("#cars").on("click",".saveRow",function(){
		var tr = $(this).closest('tr');
		var id = tr.attr('id');
		
		
		if(confirm('Do you wish to update this record?') == true){
			var newid = tr.find(".id").html();
			var newname = tr.find(".name").html();
			var newyear = tr.find(".year").html();
			console.log(newid);
			console.log(newname);
			console.log(newyear);
			$.ajax({
				type: 'POST',
				url: 'editCar.php',
				cache: false,
				data: 'newid='+newid+'&newname='+newname+'&newyear='+newyear,
				error: function(xhr,error){
					var err = eval("(" + xhr.responseText + ")");
					console.log(err.Message);
				},
				success:function(){
					alert('update success');
					tr.find(".id").html(newid);
					tr.find(".name").html(newname);
					tr.find(".year").html(newyear);
				}
			});
		}
	});
	//delete
	$("#cars").on("click",".deleteRow",function(){
		var tr = $(this).closest('tr');
     	var id = tr.attr('id');

     	if(confirm('Do you wish to delete this record?') == true){
     		$.ajax({
				type: 'POST',
				url: 'deleteCar.php',
				data: "id="+id,
				cache: false,
				error: function(xhr, error){
				    console.debug(xhr); console.debug(error);
				},
				success: function(){
					alert('success');			
					tr.fadeOut(500,function(){
						tr.remove();
					});
				}
			});
     	}
	});
});