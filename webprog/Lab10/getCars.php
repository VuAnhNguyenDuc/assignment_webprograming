<?php
	header('Content-Type: application/json'); 

	require_once 'connect.php';

	$query = "SELECT * FROM cars";
	if(!$cars = mysqli_query($dbhandle,$query)){
		echo("SQL Error: " . mysqli_error($dbhandle));
		$error = str_replace("'"," ","SQL Error: ".mysqli_error($dbhandle));
		echo "<script>";
		echo "alert(' $error ');";
		echo "</script>";
	}
	$array = array();
	while($row = mysqli_fetch_assoc($cars)){
		$array[] = $row;
	}
	mysqli_close($dbhandle);
	echo json_encode($array);
?>