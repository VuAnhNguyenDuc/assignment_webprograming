<!DOCTYPE html>
<html>
<head>
	<title>Lab 10</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<script language="javascript" type="text/javascript" src="script/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="script/ajax.js"></script>
	<script>
		window.onload = function(){
			getCars();
		}
	</script>
</head>
<body>
	<div class="container">
		<form>
			<div class="form-group">
				<label for="id">ID: </label>
				<input class="form-control" type="text" id="id" name="id" placeholder="Enter car id">
			</div>
			<div class="form-group">
				<label for="name">Name: </label>
				<input class="form-control" type="text" id="name" name="name" placeholder="Enter car name">
			</div>
			<div class="form-group">
				<label for="year">Year: </label>
				<input class="form-control" type="text" id="year" name="year" placeholder="Enter car year">
			</div>
			<button id="submit" class="btn btn-primary" type="button">Insert new cars</button>
		</form>
		<table class="table" id="cars">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Year</th>
				<th>Action</th>
			</thead>

		</table>
	</div>
</body>
</html>

